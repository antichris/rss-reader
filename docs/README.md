# Development notes

## Background

Originally started as an implementation of two very similar feed reader application specifications, one for an [Atom feed](spec-atom.pdf) (erroneously referred to as "RSS" in the specification), other for an [RSS feed](spec-rss.pdf), since then this project has become more of a proving grounds for various novel design concepts, techniques and technologies, some of which may have been superficially familiar before, some are completely new.

### Docker

One of those superficially familiar tools, that have been encountered before, but never truly mastered. Here the focus has been mostly on

- containerizing applications with their dependencies and configurations,
- using multi-stage builds,
- setting up volumes,
- reduced image layer counts.

### Docker Compose

A somewhat basic cluster management, yet sufficiently powerful for its purposes, should probably be replaced by Kubernetes at some point. In addition to service definitions, some thought has also been put into configuration of networks and volumes here.

### `Makefile` (GNU Make)

The primary motivation was a need for a solution that would simplify common tasks, reduce the burden of typing very long, verbose and complex invocations, preferably offering some degree of auto-completion support, and `make` seemed to be a good fit.

The main `Makefile` may really have grown a bit too convoluted and complex, but it does precisely what it was intended to. Some effort to clean it up, make more readable and maintainable would not hurt, but is by no means a priority at this point.

### Unix sockets

Sockets as a tool for high-bandwidth inter-process communication are currently used to link the NGINX front-end with PHP back-end containers. Doing direct memory I/O bypasses the TCP/IP stack and should give a significant performance boost.

Using sockets also for communication with the database is worth considering, but it may introduce undesirable complexity just to retain the ability to easily switch between a cluster-managed database and an external one, available over a TCP/IP connection.

### Dotfile and environment variables

A dynamic method of specifying various application component configuration parameters, used by many, including PHP frameworks, such as Symfony, Laravel and the upcoming Yii3, but somehow missed out on by Yii2.

### Specific to PHP and/or Yii2

The above tools are useful regardless of the specific programming language environment, but there are also some neat new PHP-specific things.

#### Static analysis with PHPStan

Prevents a class of bugs by ensuring more consistent typing. Could be replaced (or supplemented?) by Vimeo's Psalm, although the benefit of doing so is unclear so far.

#### Extensive unit test coverage

Although at times clumsy and overly complex (which is often an indicator of design deficiencies), the near-complete (at this point only controllers and views are not covered) unit test coverage gives the freedom to *refactor mercilessly*. And that is a Very Good Thing™.

#### Dependency injection

Since this project was started around Yii2, which relies on service locator extensively, it took some effort to break the habit and only use `Yii::createObject` where there is no other way to inject objects (e.g. static context, Yii2 does that, notably in `ActiveRecord`).

#### Non-default project layout

Many frameworks impose their conventions on the layout of your project, which means, you often can't just lay out the project in a way that makes sense to your business, and later just go and plop in a framework of your choice, change your mind and switch to another framework, or mix and match components from multiple ones. Yii2 is a wonderful exception to this, as, although it has a default project structure predefined, all aspects of it can be freely reconfigured at whim (or as business requirements dictate).

The layout of this project is modeled to loosely resemble the Filesystem Hierarchy Standard of Linux systems.

#### Favicon and web application manifest assets for Yii

The idiomatic Yii2 approach for publishing static assets.


## To do

There are many things that could (and some really should) be improved in the future. Most of them are marked with a `TODO`/`FIXME` comments (and `@todo` annotations) in the code. Here are some that didn't find their place in comments.

### RESTful API

A robust boundary between the necessarily server-side business logic and the presentation (which could just as well be rendered entirely client-side). It would probably mean getting rid of server-side view templates and rendering.

Some of this is already in the works and will be introduced in an upcoming release.

### Go

[PHP kind of sucks, and Everyone™ agrees on that][php-vs-go]. Even a crude Go implementation is guaranteed to yield a vastly superior performance, in addition to improved re-usability (think, interfaces).

### Queue(s)

An asynchronous queuing solution, such as RabbitMQ, should be used for mailing and maybe other applications (maybe fetching the feed).

### Kubernetes

Docker Compose does its job and is fine for now, but the industry standard tool for could service management is Kubernetes, and eventually this project should migrate to that.

If and when this comes to implementation, Traefik could be given a look.

[php-vs-go]: https://valueinbrief.com/posts/php-vs-go/

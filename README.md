# RSS/Atom Reader

Displays a list of items from an RSS or Atom feed to authenticated users. Become a user by registering.


## Installation

You can take any of several approaches to installation, depending on how deep you wish to get involved with configuration and prerequisites.

No matter which method you pick, you have to take the following "step zero" and adjust the [application parameters](#Configuration):

- Either set the relevant values in the OS environment or create a local `.env` file (copy and edit `.env.example`). If you have GNU Make, running `make .env` [will do that for you](#Make-init).

If you are going to use Docker, either [stand-alone](#Docker) or via [Docker Compose](#Docker-Compose), you also have to copy `devops/Dockerfile.dist` to `devops/Dockerfile`. Running `make devops/Dockerfile` [can do this for you](#Make-init). The primary motivation for this is so that you could enable the [Composer cache reuse](#Reusing-Composer-Cache-in-Docker-Development-Image) for PHP-FPM Docker image builds in your local `Dockerfile`.


### Docker Compose

As of this writing, the quickest and easiest way to get up and running is by using `docker-compose`.

In case you have GNU Make installed, there is a `Makefile` included with a [bunch of recipes for the most common tasks](#Makefile).

1. Build and start a production environment Compose deployment

	```sh
	docker-compose \
		--project-directory . \
		-p rss-reader \
		-f devops/docker-compose.yml \
		-f devops/docker-compose.prod.yml \
		up -d
	```

	If that looks a bit verbose to you, consider the fact that starting a production deployment with `make` is as simple as running

	```sh
	make prod/up
	```

	If this has piqued your interest, install `make` and read up [the section on Makefile](#Makefile).

2. Apply the database migrations:

	```sh
	docker-compose \
		--project-directory . \
		-p rss-reader \
		-f devops/docker-compose.yml \
		-f devops/docker-compose.prod.yml \
		exec \
			-u www-data:www-data \
			php bin/app migrate --interactive 0
	```

	Running the `make` recipe for that would be:

	```sh
	make prod/migrate
	```

And that is it. By now you should have an up-and-running Docker Compose deployment, consisting of a MySQL 8.0 database server and a PHP FPM instance fronted by an NGINX container listening at host port `80`.


### Docker without Compose

Described here is the bare minimum to get an application instance up and running, since this method is too flexible to list all the possible configuration options here. If you wish to take this approach, it is highly recommended that you first familiarize yourself with Docker and how to do all the things you can and want to do with it.

Also, this assumes you are using a proper, POSIX compliant shell.

1. Build the production image:

	```sh
	image=rss-reader_php; \
	docker build \
		-f devops/Dockerfile \
		--target prod \
		-t "$image" .
	```

2. Create and start a container from the production image:

	```sh
	image=rss-reader_php; \
	name=rss-reader_php_1; \
	docker run -d \
		--env-file .env \
		-v rss-reader_assets:/var/www/srv/assets:rw \
		-v rss-reader_webroot:/var/www/srv:ro \
		-v rss-reader_var-data:/var/www/var:rw \
		-v rss-reader_fpm-socket:/var/run/php-fpm.sock.d:rw \
		--name "$name" \
		"$image"
	```

3. Apply the database migrations.

	Assuming you have a properly set up database server, accessible from the PHP FPM container, run

	```sh
	name=rss-reader_php_1; \
	docker exec \
		-u www-data:www-data \
		"$name" \
		php bin/app migrate --interactive 0
	```

4. Build the NGINX image:

	```sh
	image=rss-reader_nginx; \
	docker build -t "$image" devops/nginx
	```

5. Start the NGINX container:

	```sh
	port=8080; \
	image=rss-reader_nginx; \
	name=rss-reader_nginx_1; \
	docker run -d \
		-v rss-reader_assets:/var/www/srv/assets:ro \
		-v rss-reader_webroot:/var/www/srv:ro \
		-v rss-reader_fpm-socket:/var/run/php.sock.d:ro \
		-p $port:80 \
		--name "$name" \
		"$image"
	```

At this point you should have it up and running, with NGINX listening at your host port `8080`.


### Standalone

1. Install the Composer dependencies.

	For a production installation run

	```sh
	composer install --prefer-dist --no-dev --optimize-autoloader
	```

	Or drop the `--no-dev` to get all the development dependencies, and `--optimize-autoloader` to prevent issues with class auto-loading in the development environment.

2. Apply the database migrations by running

	```sh
	bin/app migrate
	```

3. Ensure the directories `srv/assets` and `var/yii` are writable by your server process.

	That could be as simple as, for example, setting the group ownership to `www-data`:

	```sh
	chown :www-data srv/assets var/yii
	```

	and adding write to group permissions:

	```sh
	chmod g+w srv/assets var/yii
	```

	or it could be a more complicated process, involving setting up the setgid bit, appropriate umasks and/or ACLs.

	The most quick and dirty approach would be simply setting the directories in question to be world-writable:

	```sh
	chmod o+rwx srv/assets var/yii
	```

	but this is not recommended due to security considerations — the permission system is there for a reason, after all.

4. Serve `srv` as the document root, with `srv/index.php` as the entry point.

	For demonstration and testing purposes an instance on the PHP built-in web server can be started by running

	```sh
	bin/app serve
	```

	Run `bin/app serve --help` to find out more about parameters and options.


## Configuration

Essential features can be configured by either setting the OS environment variables, or creating a `.env` file based on the values in [`.env.example`](.env.example).

The Docker application images support secrets through these `*_FILE` environment variables:

- `COOKIE_VALIDATION_KEY_FILE`
- `DB_PASS_FILE`
- `SMTP_PASS_FILE`
- `MAILER_DSN_FILE`

Note that they are mutually exclusive with their respective "non-`_FILE`" forms, which means that you must unset, for example, `DB_PASS` if you set `DB_PASS_FILE`. Otherwise the PHP FPM container will refuse to start.

> **IMPORTANT!**
>
> If the `MAILER_ENABLED` parameter is not set to a non-empty value, file transport will be used, saving the files to `var/yii/mail/`. See Yii2 documentation on how to adjust that if you need to.

When setting the SMTP parameters and/or `MAILER_DSN`, note that if the username, password or host contain any character considered special in a URI as defined by [RFC 3986](https://datatracker.ietf.org/doc/html/rfc3986#section-2.2) (such as `+`, `@`, `$`, `#`, `/`, `:`, `*`, `!`), you must encode them. See the [Symfony Mailer documentation](https://symfony.com/doc/current/mailer.html#transport-setup) for further details on configuring the mailer transport.

The rest of the application configuration is stored in `etc/`:

```text
bootstrap.php   Defines application environment constants, loads Composer autoloader and `\Yii` class.
common.php      Configuration shared among all Application (that's the Yii class) instances.
web.php         The web application instance configuration.
container.php   The Yii dependency injection container configuration for the web application instance.
console.php     The console application instance configuration.
params.php      Miscellaneous application parameters config.
test-web.php    The web application test instance configuration.
```


## Dev stuff

### PHPUnit

Tests are incomplete. Currently have unit coverage on most of the services, but lack integration. Controllers have nil coverage.

```sh
vendor/bin/phpunit
```

### PHPStan

Some messages have been filtered. See `phpstan.neon.dist` for details.

```sh
vendor/bin/phpstan analyze -lmax etc src tests
```

### PHP Coding Standards Fixer

Some rules have been explicitly disabled, some have not been enabled although considered at a point. See `.php-cs-fixer.dist.php`.

```sh
vendor/bin/php-cs-fixer fix
```

### Reusing Composer Cache in Docker Development Image

To reduce network traffic and gain a significant speed boost during Composer dependency installations on subsequent image builds by re-using the Composer cache from build to build, there is a `COPY` directive in the `devops/Dockerfile` that you can uncomment after completing an initial development image build.

If you notice that the size of the Composer cache in your development image has grown quite a bit over an extended period of updating existing and adding new Composer dependencies, you can shrink it back down to just the current packages by doing a single dev build with the cache reuse disabled (the above-mentioned `COPY` directive commented out).

### phpMyAdmin

The development environment also includes a phpMyAdmin service, by default accessible at <http://pma.localhost:81/>. The Docker Compose dev deployment builds NGINX with PMA support and starts the PMA service automatically when bringing the services up; you would have to disable it manually if you wish to prevent that.

### Development notes

See [docs/README.md](docs/README.md) for some behind-the-scenes on the development.


## Makefile

To save a lot (and I do mean *a lot*) of typing, there is a `Makefile` included, which has been extensively drive-tested with GNU Make 4.1. Also, if your shell supports auto-complete for `make` invocations, you may find useful the fact that [all recipe names are aliased](#Aliases) for keyword order not to matter.

If you have not used `make` before, note that you can execute more than one recipe in a single invocation. For example, to build all development images, you can execute

```sh
make dev/nginx/image dev/php/image dev/pma/image
```

You can preview recipes without executing them by running `make` with the `-n` option, for example:

```sh
$ make -n prod/php/compose
docker-compose --project-directory . -p rss-reader -f devops/docker-compose.yml -f devops/docker-compose.prod.yml build php
docker-compose --project-directory . -p rss-reader -f devops/docker-compose.yml -f devops/docker-compose.prod.yml stop php
docker-compose --project-directory . -p rss-reader -f devops/docker-compose.yml -f devops/docker-compose.prod.yml rm -f php
docker-compose --project-directory . -p rss-reader -f devops/docker-compose.yml -f devops/docker-compose.prod.yml up -d php
```

### Make Init

The `init` recipe initializes the project. It depends on `.env` and `devops/Dockerfile` recipes and `make` will execute either or both of them, as it deems it necessary. Since `init` is the first recipe in the `Makefile`, it is also the default recipe to execute when `make` is invoked without any arguments.

If the `.env` file does not exist, `make .env` creates it as a copy of the `.env.example`, with the `COOKIE_VALIDATION_KEY` variable set to a random (from `/dev/urandom`) 256-bit number as a 64 hexadecimal digit string.

If `devops/Dockerfile` does not exist, `make devops/Dockerfile` creates it as a copy of `devops/Dockerfile.dist`. If `make` detects that `Dockerfile.dist` is newer than `Dockerfile`, it will prompt you with a diff and ask for confirmation before overwriting `Dockerfile`.

### Recipes

In addition to the recipes described in the following sections, also included are two recipes that print the environment-specific Docker Compose command prefix: `dev` and `prod`. These can save you a great deal of typing when you need to run a command for which there is no predefined recipe.

For example, I have not had the need for the `images` command, but if you do, instead of typing

```sh
docker-compose --project-directory . -p rss-reader -f devops/docker-compose.yml -f devops/docker-compose.prod.yml images
```

all by hand, you could run it like this:

```sh
$(make prod) images
```

As a matter of fact, you can safely ignore the rest of the recipes and control Docker Compose this way, using all the `docker-compose` commands you are well familiar with, although then you would be giving up the convenience of shell auto-completion.

#### Building Docker Images

These can also be used if you only have Make and Docker installed, since they do not depend on Docker Compose — unlike the `build` recipes (also yielding Docker images) described in the following sections.

- `prod/php/image` — PHP FPM production image.
- `prod/nginx/image` — NGINX front-end production image.
- `dev/php/image` — PHP FPM development image.
- `dev/nginx/image` — NGINX front-end development image.
- `dev/pma/image` — phpMyAdmin image (for the development NGINX).

#### Controlling the Compose Deployment

To control the Docker Compose deployment, you have to indicate environment by adding the environment-specific prefix (or suffix, [it does not matter](#Aliases)). The following list is prefixed with `prod` for the production environment. To apply them to the development environment, just replace that with `dev` (e.g. `dev/logs`).

Without any further ado, here they are:

- `prod/build` — (Re)build services.
- `prod/up` — Build, (re)create and start services in detached mode.
- `prod/pause` — Pause services.
- `prod/unpause` — Unpause services.
- `prod/restart` — Restart running services.
- `prod/stop` — Stop the running services without removing them.
- `prod/down` — Stop and remove services and networks.
- `prod/logs` — Follow the container logs.
- `prod/unvolume` — Prune dangling volumes of the environment.
- `prod/recreate` — Stop, remove, recreate and start services back up again.
- `prod/compose` — (Re)build and recreate the services.
- `prod/reset` — Bring services down, remove volumes and bring services back up, effectively resetting the deployment.
- `prod/migrate` — Apply the database migrations from the PHP service.

#### Controlling Services in the Deployment

Of the above operations, you can perform `build`, `up`, `pause`, `unpause`, `restart`, `stop`, `down`, `logs`, `recreate` and `compose` on specific services (`php` in the following example) with a recipe like this:

```sh
make prod/php/up
```

In addition to these recipes, there are

- `rm` (e.g. `prod/php/rm`), that removes a stopped container, and
- `shell` (e.g. `prod/php/shell`), that executes and attaches to a root shell session in a running container (more on this in [the next subsection](#Shell-Access-to-Services-in-the-Deployment)).

You can also perform database maintenance operations by starting a MySQL client with the recipe `prod/mysql/client`. This will connect with the same credentials as used by the application.

#### Shell Access to Services in the Deployment

The recipe `<env>/<service>/shell` (e.g. `dev/mysql/shell`) starts a root shell process in the respective container and attaches your session to it. While this is perfectly adequate for most maintenance tasks you may want to perform, file operations could change the permissions and ownership of the local files and directories bound to the container.

Therefore execute the recipe `dev/php/userShell` to attach to a shell session in the development container as your current user and group (the one that the `id` command resolves to). This enables you to work with the project sources in the Docker environment without messing up the permissions of your local files.

As you are likely to invoke the `dev/php/userShell` recipe a lot, it has a special shortcut alias. It is simply `shell`:

```sh
make shell
```


### Aliases

All recipe names are aliased for keyword order not to matter. For example, the canonical recipe to build the NGINX service for the development environment is `dev/nginx/build`, but you can run it also as

- `dev/build/nginx`
- `nginx/dev/build`
- `nginx/build/dev`
- `build/dev/nginx`
- `build/nginx/dev`

whichever comes easier to you on the spur of the moment.

In addition to the aliases that free you from obeying any specific keyword order, `<env>/shell` is an alias for `<env>/php/shell` (e.g. `dev/shell` == `dev/php/shell`). And that too is aliased as `shell/<env>`, so you can start a root shell session in the `php` container by doing, for example, `shell/dev` for the development environment.

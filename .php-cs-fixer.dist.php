<?php

$header = <<<'EOT'
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
EOT;

use PhpCsFixer\Finder;
use PhpCsFixer\Config;

$finder = Finder::create()
    ->name('*.php.stub')
    ->in(__DIR__ . '/etc')
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/tests');

$config = (new Config())
    ->setRules([
        '@PhpCsFixer' => true,
        '@PSR12' => true,
        'align_multiline_comment' => ['comment_type' => 'all_multiline'],
        'array_syntax' => ['syntax' => 'short'],
        'concat_space' => ['spacing' => 'one'],
        // Risky
        'declare_strict_types' => true,
        'echo_tag_syntax' => ['format' => 'short'],
        // TODO: Meditate on this.
        'final_class' => false,
        'header_comment' => ['header' => $header],
        'list_syntax' => ['syntax' => 'short'],
        'multiline_whitespace_before_semicolons' => ['strategy' => 'no_multi_line'],
        // Risky
        //'non_printable_character' => ['use_escape_sequences_in_strings' => true],
        'nullable_type_declaration_for_default_null_value' => true,
        'ordered_imports' => [
            'imports_order' => ['class', 'const', 'function'],
            'sort_algorithm' => 'alpha',
        ],
        'phpdoc_align' => [
            // The default value excludes read-/write-only property annotations.
            // TODO It still does not align the comments of those properties.
            'tags' => [
                'method', 'param', 'property', 'property-read', 'property-write',
                'return', 'throws', 'type', 'var',
            ],
        ],
        'phpdoc_no_alias_tag' => [
            // The default value strips read-/write-only property annotations.
            'replacements' => ['type' => 'var', 'link' => 'see'],
        ],
        // Breaks PHPDoc type hinting in view templates.
        'phpdoc_to_comment' => false,
        // TODO: Meditate on this.
        //'protected_to_private' => false,
        // Breaks PHPDoc type hinting in view templates.
        'single_line_comment_style' => false,
        'trailing_comma_in_multiline' => ['elements' => ['arrays', 'arguments', 'parameters']],
        'visibility_required' => ['elements' => ['property', 'method', 'const']],
        'yoda_style' => ['identical' => null],
    ])
    ->setCacheFile(__DIR__ . '/var/php-cs-fixer.cache')
    ->setFinder($finder);

return $config;

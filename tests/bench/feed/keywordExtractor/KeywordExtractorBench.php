<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\bench\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\KeywordExtractor;
use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use antichris\rssReader\tests\_support\feed\keywordExtractor\BenchDataProvider;
use Closure;
use Generator;
use function antichris\rssReader\internalCall;

/**
 * @internal
 */
final class KeywordExtractorBench
{
    use BenchDataProvider;

    /**
     * @var Closure&callable(string[]):string[]
     */
    private Closure $filter;

    /**
     * @BeforeMethods("setUpFilter")
     * @ParamProviders({"provideWords","provideBlacklist"})
     *
     * @param array{string[]} $params
     */
    public function benchFilter(array $params): void
    {
        ($this->filter)(...$params);
    }

    public function setUpFilter(): void
    {
        $this->filter = internalCall($this->createInstance(), 'filter');
    }

    /**
     * @return Generator<string,array{string[]},mixed,void>
     */
    public function provideBlacklist(): Generator
    {
        yield 'default' => [$this->createInstance()->blacklist];
    }

    private function createInstance(): KeywordExtractor
    {
        return new KeywordExtractor(new WordExtractor());
    }
}

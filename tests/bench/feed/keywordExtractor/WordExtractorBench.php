<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\bench\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use antichris\rssReader\tests\_support\feed\keywordExtractor\BenchDataProvider;
use Closure;
use function antichris\rssReader\internalCall;

/**
 * @internal
 */
final class WordExtractorBench
{
    use BenchDataProvider;

    private WordExtractor $instance;
    /**
     * @var Closure&callable(string):string[]
     */
    private Closure $extractWords;
    /**
     * @var Closure&callable(string[]):string[]
     */
    private Closure $filterNonWords;

    /**
     * @BeforeMethods("setUpInstance")
     * @ParamProviders("provideStrings")
     *
     * @param array{string} $params
     */
    public function benchExtractWords(array $params): void
    {
        ($this->extractWords)(...$params);
    }

    /**
     * @BeforeMethods("setUpInstance")
     * @ParamProviders("provideWords")
     *
     * @param array{string[]} $params
     */
    public function benchFilterNonWords(array $params): void
    {
        ($this->filterNonWords)(...$params);
    }

    public function setUpInstance(): void
    {
        $this->instance = new WordExtractor();
        $this->extractWords = $this->instanceCall('extractWords');
        $this->filterNonWords = $this->instanceCall('filterNonWords');
    }

    private function instanceCall(string $method): Closure
    {
        return internalCall($this->instance, $method);
    }
}

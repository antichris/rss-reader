<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration;

use antichris\rssReader\feed\parser\FormatDetector;
use antichris\rssReader\user\authentication\AuthenticationService;
use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\registration\RegistrationMailer;
use antichris\rssReader\user\registration\RegistrationService;
use antichris\rssReader\web\controllers\RegistrationController;
use antichris\rssReader\web\controllers\UserController;
use antichris\rssReader\web\View;
use antichris\rssReader\web\widgets\Alert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Yii;
use yii\base\Application;
use yii\base\Request;
use yii\base\Response;
use yii\base\Security;
use yii\di\Container;
use yii\i18n\Formatter;
use yii\i18n\I18N;
use yii\mail\MailerInterface;
use yii\web\Session;
use yii\web\UrlManager;
use yii\web\User;
use yii\web\View as WebView;

/**
 * @internal
 * @coversNothing
 */
final class ContainerConfigTest extends TestCase
{
    /**
     * @var array<string,mixed[]>
     */
    private static $config;

    /**
     * @var MockObject&Application
     */
    private MockObject $app;

    public static function setUpBeforeClass(): void
    {
        require_once YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php';
        self::$config = require YII_APP_BASE_PATH . '/etc/container.php';
    }

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->app = $this->createMock(Application::class);

        Yii::$app = $this->app;
        Yii::$container = new Container();
        Yii::configure(Yii::$container, self::$config);
    }

    public function testGetAlert(): void
    {
        $session = $this->mockSession();
        $this->appGetConsecutiveComponents(compact('session'));

        $this->assertGotInstanceOf(Alert::class);
    }

    public function testGetAuthenticationService(): void
    {
        $user = $this->mockUser();
        $this->appGetConsecutiveComponents(compact('user'));
        $this->setMockDependency(PasswordService::class);

        $this->assertGotInstanceOf(AuthenticationService::class);
    }

    public function testGetFormatDetector(): void
    {
        $this->assertGotInstanceOf(FormatDetector::class);
    }

    public function testGetPasswordService(): void
    {
        $security = $this->createMock(Security::class);
        $this->appGetConsecutiveComponents(compact('security'));

        $this->assertGotInstanceOf(PasswordService::class);
    }

    public function testGetRegistrationController(): void
    {
        $user = $this->mockUser();
        $session = $this->mockSession();
        $this->appGetConsecutiveComponents(compact('user', 'session'));
        $this->setMockDependency(RegistrationService::class);

        $this->assertGotControllerInstance(RegistrationController::class);
    }

    public function testGetRegistrationMailer(): void
    {
        $senderEmail = '<sender-email>';
        $senderName = '<sender-name>';
        $urlManager = $this->createMock(UrlManager::class);
        $mailer = $this->createMock(MailerInterface::class);

        $this->app->params = compact('senderEmail', 'senderName');
        $this->appGetConsecutiveComponents(compact('mailer', 'urlManager'));
        $class = RegistrationMailer::class;

        $instance = Yii::$container->get($class);

        $this->assertInstanceOf($class, $instance);
    }

    public function testGetUserController(): void
    {
        $user = $this->mockUser();
        $this->appGetConsecutiveComponents(compact('user'));
        $this->setMockDependency(AuthenticationService::class);

        $this->assertGotControllerInstance(UserController::class);
    }

    public function testGetView(): void
    {
        $this->mockViewDependencies();
        $this->assertGotInstanceOf(View::class);
    }

    public function testGetWebView(): void
    {
        $this->mockViewDependencies();
        $this->assertGotBoundInstance(View::class, WebView::class);
    }

    /**
     * @param array<string,object> $components
     */
    private function appGetConsecutiveComponents(array $components): void
    {
        $this->app->method('get')
            ->withConsecutive(...array_map(
                fn ($component) => [$component],
                array_keys($components),
            ))
            ->willReturnOnConsecutiveCalls(...array_values($components));
    }

    /**
     * @phpstan-param class-string $expected
     *
     * @param mixed[]             $params
     * @param array<string,mixed> $config
     */
    private function assertGotInstanceOf(
        string $expected,
        array $params = [],
        array $config = [],
    ): void {
        $this->assertGotBoundInstance($expected, $expected, $params, $config);
    }

    /**
     * @phpstan-param class-string $expected
     */
    private function assertGotControllerInstance(
        string $expected,
    ): void {
        // See yii\base\Module::createController()
        $params = ['<controller-id>', $this->app];

        $this->assertGotInstanceOf($expected, $params, [
            'request' => $this->createMock(Request::class),
            'response' => $this->createMock(Response::class),
        ]);
    }

    /**
     * @phpstan-param class-string $expected
     * @phpstan-param class-string $binding
     *
     * @param mixed[]             $params
     * @param array<string,mixed> $config
     */
    private function assertGotBoundInstance(
        string $expected,
        string $binding,
        array $params = [],
        array $config = [],
    ): void {
        $instance = Yii::$container->get($binding, $params, $config);
        $this->assertInstanceOf($expected, $instance);
    }

    /**
     * @param class-string $class
     */
    private function setMockDependency(string $class): void
    {
        Yii::$container->set($class, $this->createMock($class));
    }

    private function mockViewDependencies(): void
    {
        $formatter = $this->createMock(Formatter::class);
        $i18n = $this->createMock(I18N::class);
        $user = $this->mockUser();
        $this->appGetConsecutiveComponents(compact(
            'formatter',
            'i18n',
            'user',
        ));
    }

    /**
     * @return MockObject&Session
     */
    private function mockSession()
    {
        return $this->createMock(Session::class);
    }

    /**
     * @return MockObject&User
     */
    private function mockUser()
    {
        return $this->createMock(User::class);
    }
}

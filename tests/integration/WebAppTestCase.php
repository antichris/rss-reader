<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration;

use PHPUnit\Framework\TestCase;
use Yii;
use yii\web\Application;
use const YII_APP_BASE_PATH;

/**
 * @internal
 * @coversNothing
 */
class WebAppTestCase extends TestCase
{
    private ?Application $app;

    protected function getApp(): Application
    {
        if (empty($this->app)) {
            $this->app = $this->createWebApplication();
        } else {
            Yii::$app = $this->app;
        }

        return $this->app;
    }

    /**
     * Creates a Yii web application instance, so that Yii::$app is an object and stuff that depends on it can be tested.
     *
     * @todo Figure out a more robust solution.
     */
    private function createWebApplication(): Application
    {
        require_once YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php';
        $config = require YII_APP_BASE_PATH . '/etc/test-web.php';

        return new Application($config);
    }
}

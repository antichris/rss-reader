<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration\user\registration;

use antichris\rssReader\tests\_support\CaseNamerTrait;
use antichris\rssReader\user\registration\RegistrationFormMapper;
use antichris\rssReader\user\registration\RegistrationMailer;
use antichris\rssReader\user\registration\RegistrationService;
use antichris\rssReader\user\UserService;
use antichris\rssReader\user\UserStatusService;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use yii\base\Security;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\registration\RegistrationService
 */
final class RegistrationServiceTest extends TestCase
{
    use CaseNamerTrait;

    /**
     * @var RegistrationService
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $userService = $this->createMock(UserService::class);
        $formMapper = $this->createMock(RegistrationFormMapper::class);
        $statusService = $this->createMock(UserStatusService::class);
        $mailer = $this->createMock(RegistrationMailer::class);

        $this->instance = new RegistrationService(
            $userService,
            $formMapper,
            $statusService,
            $mailer,
            new Security(),
        );
    }

    /**
     * @dataProvider provideGenerateEmailToken
     * @covers ::generateEmailToken
     * @covers ::generateRandomString
     */
    public function testGenerateEmailToken(int $expectedLength, int $length): void
    {
        $instance = $this->instance;

        $token = $instance->generateEmailToken($length);

        $timeBase36 = substr($token, -6);
        $this->assertEquals($expectedLength, strlen($token));
        $this->assertEqualsWithDelta(time(), base_convert($timeBase36, 36, 10), 1.0);
    }

    /**
     * @return array<string,array{int,int}>
     */
    public function provideGenerateEmailToken(): array
    {
        $lengths = [128, 64, 32, 16, 6, 3, 0, -1];

        $expectedLengths = array_map(function ($length) {
            return max($length, 6);
        }, $lengths);

        $caseParams = array_map(null, $expectedLengths, $lengths);
        $caseNames = $this->nameCases('length %d', $lengths);
        $cases = array_combine($caseNames, $caseParams);
        if (!$cases) {
            throw new RuntimeException('Test case data generation failed');
        }

        return $cases;
    }
}

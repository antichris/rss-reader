<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration\user;

use antichris\rssReader\tests\_support\CaseNamerTrait;
use antichris\rssReader\user\UserStatusService;
use BadMethodCallException;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use RuntimeException;
use Throwable;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\UserStatusService
 * @phpstan-type tDescribeThrowable array{class-string<Throwable>,string}
 */
final class UserStatusServiceTest extends TestCase
{
    use CaseNamerTrait;

    private ?UserStatusService $instance;
    /**
     * @var string[]
     */
    private $statusNames = ['DELETED', 'ACTIVE', 'INACTIVE', 'DEFAULT'];

    /**
     * @before
     */
    public function setUp(): void
    {
        $this->instance = new UserStatusService();
    }

    /**
     * @dataProvider provideHas
     * @covers ::has
     */
    public function testHas(bool $expected, string $statusName): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        $actual = $instance->has($statusName);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,string}>
     */
    public function provideHas(): array
    {
        $names = $this->statusNames;

        $caseParams = array_map(null, $this->getTrues($names), $names);
        $cases = array_combine($names, $caseParams);
        if (!$cases) {
            throw new RuntimeException('Test case data generation failed');
        }

        return $cases + [
            'default (not)' => [false, 'default'],
            'AWESOME (not)' => [false, 'AWESOME'],
        ];
    }

    /**
     * @dataProvider provideGet
     * @covers ::get
     *
     * @param null|tDescribeThrowable $exception
     */
    public function testGet(int $expected, string $statusName, ?array $exception = null): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        if ($exception) {
            [$type, $message] = $exception;
            $this->expectException($type);
            $this->expectExceptionMessage($message);
        }

        $actual = $instance->get($statusName);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{int,string,}>
     */
    public function provideGet(): array
    {
        $names = $this->statusNames;
        $values = $this->getStatusValues($names);

        $caseParams = array_map(null, $values, $names);
        $cases = array_combine($names, $caseParams);
        if (!$cases) {
            throw new RuntimeException('Test case data generation failed');
        }

        return $cases + [
            'AWESOME (not)' => [0, 'AWESOME', [InvalidArgumentException::class, "Status 'AWESOME' is not defined"]],
        ];
    }

    /**
     * @dataProvider provideIs
     * @covers ::is
     *
     * @param null|tDescribeThrowable $exception
     */
    public function testIs(bool $expected, string $statusName, int $value, ?array $exception = null): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        if ($exception) {
            [$type, $message] = $exception;
            $this->expectException($type);
            $this->expectExceptionMessage($message);
        }

        $actual = $instance->is($statusName, $value);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,string,int,3?:tDescribeThrowable}>
     */
    public function provideIs(): array
    {
        $names = $this->statusNames;
        $values = $this->getStatusValues($names);

        $caseNames = $this->nameCases('%s, %d', $names, $values);
        $caseParams = array_map(null, $this->getTrues($names), $names, $values);
        $cases = array_combine($caseNames, $caseParams);
        if (!$cases) {
            throw new RuntimeException('Test case data generation failed');
        }

        return $cases + [
            'ACTIVE, 13 (not)' => [false, 'ACTIVE', 13],
            'AWESOME, 0 (not)' => [false, 'AWESOME', 0, [InvalidArgumentException::class, "Status 'AWESOME' is not defined"]],
        ];
    }

    /**
     * @covers ::getAll
     * @covers ::getReflection
     */
    public function testGetAll(): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;
        $expected = $this->getStatusValues($this->statusNames);

        $actual = $instance->getAll();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideIsValidValue
     * @covers ::isValidValue
     */
    public function testIsValidValue(bool $expected, int $status): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        $actual = $instance->isValidValue($status);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,int}>
     */
    public function provideIsValidValue(): array
    {
        $names = $this->statusNames;
        $values = $this->getStatusValues($this->statusNames);

        $caseParams = array_map(null, $this->getTrues($values), $values);
        $caseNames = $this->nameCases('%d (%s)', $values, $names);
        $cases = array_combine($caseNames, $caseParams);
        if (!$cases) {
            throw new RuntimeException('Test case data generation failed');
        }

        return $cases + [
            '13 (not)' => [false, 13],
        ];
    }

    /**
     * @covers ::__call
     */
    public function testGetDefault(): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;
        $expected = $instance::DEFAULT;

        $actual = $instance->getDefault();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideIsDefault
     * @covers ::__call
     */
    public function testIsDefault(bool $expected, int $status): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        $actual = $instance->isDefault($status);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,int}>
     */
    public function provideIsDefault(): array
    {
        $default = $this->getInstance()::DEFAULT;
        $other = 13;

        return [
            "{$default} (is)" => [true, $default],
            "{$other} (not)" => [false, $other],
        ];
    }

    /**
     * @covers ::__call
     */
    public function testIsDefaultThrowsWithoutArguments(): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        $this->expectException(BadMethodCallException::class);
        $this->expectExceptionMessage('Missing required value argument');

        $instance->isDefault();
    }

    /**
     * @covers ::__call
     */
    public function testIsUnknownStatusThrows(): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Status 'UNKNOWNSTATUS' is not defined");

        $instance->isUnknownStatus(13);
    }

    /**
     * @covers ::__call
     */
    public function testGetUnknownStatusThrows(): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Status 'UNKNOWNSTATUS' is not defined");

        $instance->getUnknownStatus();
    }

    /**
     * @covers ::__call
     */
    public function testCallingAnythingElseThrows(): void
    {
        /** @var UserStatusService $instance */
        $instance = $this->instance;
        $class = get_class($instance);
        $name = 'anythingElse';

        $this->expectException(BadMethodCallException::class);
        $this->expectExceptionMessage("Call to undefined method {$class}::{$name}()");

        $instance->{$name}();
    }

    private function getInstance(): UserStatusService
    {
        if (empty($this->instance)) {
            $this->instance = new UserStatusService();
        }

        return $this->instance;
    }

    /**
     * Returns an associative array of status values for the given status names, keyed by names.
     *
     * @param string[] $statusNames
     *
     * @return array<string,int>
     */
    private function getStatusValues(array $statusNames): array
    {
        $reflection = new ReflectionClass($this->getInstance());
        $values = array_map([$reflection, 'getConstant'], $statusNames);
        $statusValues = array_combine($statusNames, $values);
        if (!$statusValues) {
            throw new RuntimeException('Status value acquisition and preparation failed');
        }

        return $statusValues;
    }

    /**
     * Returns an array of boolean `true` values with the same count as elements in the given array.
     *
     * @param mixed[] $array
     *
     * @return bool[]
     */
    private function getTrues(array $array): array
    {
        return array_fill(0, count($array), true);
    }
}

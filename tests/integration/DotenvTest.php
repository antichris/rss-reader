<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration;

use antichris\rssReader\tests\_support\vfsHelper;
use Dotenv\Dotenv;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use function antichris\rssReader\base64md5;
use function antichris\rssReader\callerName;
use function antichris\rssReader\getlenv;

/**
 * This integration test confirms that the current solutions for loading .env
 * files and getting values from environment variables are in fact compatible,
 * cooperative and fit for the purpose.
 *
 * @internal
 * @coversNothing
 */
class DotenvTest extends TestCase
{
    private const DEFAULT = '<default>';

    /**
     * @dataProvider provideDotenvImmutable
     */
    public function testDotenvImmutable(
        string $expected,
        string $varname,
        string $path,
    ): void {
        Dotenv::createImmutable($path)->safeLoad();

        $actual = getlenv($varname, self::DEFAULT);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string,string,string}>
     */
    public function provideDotenvImmutable(): array
    {
        // Avoid name collision in an immutable environment.
        return $this->vfsDotenvCases('FOO_', 'BAR_');
    }

    /**
     * @dataProvider provideDotenvMutable
     */
    public function testDotenvMutable(
        string $expected,
        string $varname,
        string $path,
    ): void {
        Dotenv::createMutable($path)->safeLoad();

        $actual = getlenv($varname, self::DEFAULT);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string,string,string}>
     */
    public function provideDotenvMutable(): array
    {
        return $this->vfsDotenvCases('FOO_');
    }

    /**
     * @return array<string,array{string,string,string}>
     */
    private function vfsDotenvCases(
        string $normalPrefix,
        ?string $emptyPrefix = null,
    ): array {
        $value = microtime();

        $normalName = $this->md5suffix($normalPrefix, $value);
        $emptyName = $this->md5suffix($emptyPrefix ?? $normalPrefix, $value);

        $cases = [
            'unset' => [self::DEFAULT, $normalName, ''],
            'normal' => [$value, $normalName, "{$normalName}='{$value}'"],
            'empty' => ['', $emptyName, "{$emptyName}=''"],
        ];

        $vfs = vfsHelper::create([], nameSeed: callerName());
        foreach ($cases as $name => &$case) {
            $content = $case[2];

            $vfsDir = vfsStream::newDirectory($name)->at($vfs);
            vfsHelper::addNewFile($vfsDir, '.env')->withContent($content);

            $case[2] = $vfsDir->url();
        }

        return $cases;
    }

    private function md5suffix(
        string $name,
        string $suffixData,
    ): string {
        return $name . str_replace('-', '.', base64md5($suffixData));
    }
}

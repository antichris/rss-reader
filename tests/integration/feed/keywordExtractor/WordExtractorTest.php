<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\keywordExtractor\WordExtractor
 */
final class WordExtractorTest extends TestCase
{
    /**
     * @var WordExtractor
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = new WordExtractor();
    }

    /**
     * @dataProvider provideExtract
     * @covers ::extract
     * @covers ::extractWords
     * @covers ::filterNonWords
     *
     * @param string[] $expected
     */
    public function testExtract(array $expected, string $string): void
    {
        $instance = $this->instance;

        $actual = $instance->extract($string);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string[],string}>
     */
    public function provideExtract(): array
    {
        return [
            'words' => [['Word', 'dashed-word', "apostroph'd", 'l33t'], "Word: dashed-word; apostroph'd. l33t"],
            'unicode words' => [['ģļāžšķūņū', 'ŗūķīšī'], 'ģļāžšķūņū ŗūķīšī'],
            'number-words' => [["cloud9's", 'Java11', 'haxx0r'], "cloud9's Java11 haxx0r"],
            'numbers' => [[], '123 1-2 12.34 1,234.56'],
            'symbols' => [[], '` - = [ ] \ ; \' , . / ~ ! @ # $ % ^ & * ( ) _ + { } | : " < > ?'],
            'symbol strings' => [[], "-_- -'- _-'-_  -"],
            'empty string' => [[], ''],
        ];
    }
}

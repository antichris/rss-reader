<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\KeywordExtractor;
use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\keywordExtractor\KeywordExtractor
 *
 * @method KeywordExtractor createInstance() Constructs the instance under test.
 *
 * @extends TestCase<KeywordExtractor>
 */
final class KeywordExtractorTest extends TestCase
{
    use MockHelperTrait;
    /**
     * @var KeywordExtractor
     */
    private $instance;
    /**
     * @var WordExtractor
     */
    private $extractor;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->extractor = new WordExtractor();
        $this->instance = $this->createInstance();
    }

    /**
     * @dataProvider provideExtract
     * @covers ::extract
     *
     * @param array<string,int> $expected
     */
    public function testExtract(array $expected, string $string): void
    {
        $instance = $this->instance;

        $actual = $instance->extract($string);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{array<string,int>,string}>
     */
    public function provideExtract(): array
    {
        $string = $this->provideString();

        return [
            'string' => [
                [
                    "there's" => 4, 'lady' => 1, 'sure' => 2, 'glitters' => 1, 'gold' => 1, 'line' => 1, "that's" => 1,
                    'replaced' => 1, 'here' => 1, 'gets' => 1, 'knows' => 1, 'stores' => 1, 'closed' => 1, 'word' => 1,
                    'came' => 1, 'sign' => 1, 'wall' => 1, 'wants' => 1, "'cause" => 1, 'sometimes' => 2, 'words' => 1,
                    'meanings' => 1, 'tree' => 1, 'brook' => 1, 'songbird' => 1, 'sings' => 1, 'thoughts' => 1,
                    'misgiven' => 1,
                ],
                $string,
            ],
            'empty string' => [[], ''],
        ];
    }

    /**
     * @dataProvider provideExtractSorted
     * @covers ::extractTop
     *
     * @param array<string,int> $expected
     */
    public function testExtractSorted(array $expected, string $string, int $order): void
    {
        $instance = $this->instance;

        $actual = $instance->extractSorted($string, $order);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{array<string,int>,string,KeywordExtractor::ORDER_*}>
     */
    public function provideExtractSorted(): array
    {
        $string = $this->provideString();
        $orderAsc = KeywordExtractor::ORDER_ASC;
        $orderDesc = KeywordExtractor::ORDER_DESC;

        return [
            'ascending' => [
                [
                    'lady' => 1, 'glitters' => 1, 'gold' => 1, 'line' => 1, "that's" => 1, 'replaced' => 1, 'here' => 1,
                    'gets' => 1, 'knows' => 1, 'stores' => 1, 'closed' => 1, 'word' => 1, 'came' => 1, 'sign' => 1,
                    'wall' => 1, 'wants' => 1, "'cause" => 1, 'words' => 1, 'meanings' => 1, 'tree' => 1, 'brook' => 1,
                    'songbird' => 1, 'sings' => 1, 'thoughts' => 1, 'misgiven' => 1, 'sure' => 2, 'sometimes' => 2,
                    "there's" => 4,
                ],
                $string, $orderAsc,
            ],
            'descending' => [
                [
                    "there's" => 4, 'sure' => 2, 'sometimes' => 2, 'lady' => 1, 'glitters' => 1, 'gold' => 1,
                    'line' => 1, "that's" => 1, 'replaced' => 1, 'here' => 1, 'gets' => 1, 'knows' => 1, 'stores' => 1,
                    'closed' => 1, 'word' => 1, 'came' => 1, 'sign' => 1, 'wall' => 1, 'wants' => 1, "'cause" => 1,
                    'words' => 1, 'meanings' => 1, 'tree' => 1, 'brook' => 1, 'songbird' => 1, 'sings' => 1,
                    'thoughts' => 1, 'misgiven' => 1,
                ],
                $string, $orderDesc,
            ],
            'empty string' => [[], '', $orderDesc],
        ];
    }

    /**
     * @dataProvider provideExtractTop
     * @covers ::extractTop
     *
     * @param array<string,int> $expected
     */
    public function testExtractTop(array $expected, int $count, string $string): void
    {
        $instance = $this->instance;

        $actual = $instance->extractTop($count, $string);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{array<string,int>,int,string}>
     */
    public function provideExtractTop(): array
    {
        $string = $this->provideString();

        return [
            'top 15' => [
                [
                    "there's" => 4, 'sure' => 2, 'sometimes' => 2, 'lady' => 1, 'glitters' => 1, 'gold' => 1,
                    'line' => 1, "that's" => 1, 'replaced' => 1, 'here' => 1, 'gets' => 1, 'knows' => 1, 'stores' => 1,
                    'closed' => 1, 'word' => 1,
                ],
                15, $string,
            ],
            'top 5' => [
                ["there's" => 4, 'sure' => 2, 'sometimes' => 2, 'lady' => 1, 'glitters' => 1], 5, $string,
            ],
            'empty string' => [[], 10, ''],
        ];
    }

    protected function instanceClass(): string
    {
        return KeywordExtractor::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->extractor];
    }

    private function provideString(): string
    {
        return <<<'EOD'
There's a lady who is sure all that glitters is gold
And there's a line that's replaced here
When she gets there she knows, if the stores are all closed
With a word she can get what she came for.

There's a sign on the wall but she wants to be sure
'Cause you know sometimes words have two meanings.
In a tree by the brook, there's a songbird who sings,
Sometimes all of our thoughts are misgiven.
EOD;
    }
}

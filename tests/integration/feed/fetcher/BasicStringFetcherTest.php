<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\integration\feed\fetcher;

use antichris\rssReader\feed\fetcher\BasicStringFetcher;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\fetcher\BasicStringFetcher
 */
final class BasicStringFetcherTest extends TestCase
{
    /**
     * @var BasicStringFetcher
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = new BasicStringFetcher();
    }

    /**
     * @covers ::getContents
     */
    public function testFetch(): void
    {
        /** @var BasicStringFetcher $instance */
        $instance = $this->instance;

        $haystack = $instance->fetch('http://www.example.com/');

        $this->assertStringContainsString('Example Domain', $haystack);
    }
}

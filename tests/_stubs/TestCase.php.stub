<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace PHPUnit\Framework;

use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 */
class TestCase
{
    /**
     * @param mixed[]    $data
     * @param int|string $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $originalClassName
     *
     * @param mixed $originalClassName
     *
     * @return MockObject&T
     */
    protected function createStub($originalClassName)
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $originalClassName
     *
     * @param mixed $originalClassName
     *
     * @return MockObject&T
     */
    protected function createMock($originalClassName)
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $className
     *
     * @return MockBuilder<T>
     */
    protected function getMockBuilder(string $className)
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $originalClassName
     *
     * @param mixed $originalClassName
     *
     * @return MockObject&T
     */
    protected function createConfiguredMock($originalClassName)
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $originalClassName
     * @phpstan-param string[] $methods
     *
     * @param mixed $originalClassName
     *
     * @return MockObject&T
     */
    protected function createPartialMock($originalClassName, array $methods)
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $originalClassName
     *
     * @param mixed $originalClassName
     *
     * @return MockObject&T
     */
    protected function createTestProxy($originalClassName)
    {
    }

    /**
     * @template T
     * @phpstan-param class-string<T> $originalClassName
     * @phpstan-param mixed[] $arguments
     * @phpstan-param string $mockClassName
     * @phpstan-param bool $callOriginalConstructor
     * @phpstan-param bool $callOriginalClone
     * @phpstan-param bool $callAutoload
     * @phpstan-param string[] $mockedMethods
     * @phpstan-param bool $cloneArguments
     *
     * @param mixed $originalClassName
     * @param mixed $mockClassName
     * @param mixed $callOriginalConstructor
     * @param mixed $callOriginalClone
     * @param mixed $callAutoload
     * @param mixed $mockedMethods
     * @param mixed $cloneArguments
     *
     * @return MockObject&T
     */
    protected function getMockForAbstractClass($originalClassName, array $arguments = [], $mockClassName = '', $callOriginalConstructor = true, $callOriginalClone = true, $callAutoload = true, $mockedMethods = [], $cloneArguments = false)
    {
    }

    /**
     * @template T
     * @phpstan-param string $wsdlFile
     * @phpstan-param class-string<T> $originalClassName
     * @phpstan-param string $mockClassName
     * @phpstan-param string[] $methods
     * @phpstan-param bool $callOriginalConstructor
     * @phpstan-param mixed[] $options
     *
     * @param mixed $wsdlFile
     * @param mixed $originalClassName
     * @param mixed $mockClassName
     * @param mixed $callOriginalConstructor
     *
     * @return MockObject&T
     */
    protected function getMockFromWsdl($wsdlFile, $originalClassName = '', $mockClassName = '', array $methods = [], $callOriginalConstructor = true, array $options = [])
    {
    }
}

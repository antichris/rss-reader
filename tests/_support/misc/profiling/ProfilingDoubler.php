<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\misc\profiling;

use antichris\rssReader\misc\profiling\concrete\Sample;
use antichris\rssReader\misc\profiling\ProfilerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;

/**
 * ProfilingDoubler provides test doubles to stand in for profiling components.
 */
class ProfilingDoubler
{
    public function __construct(
        private TestCase $test,
    ) {
    }

    /**
     * Returns stub for Sample.
     *
     * @return Stub&Sample
     */
    public function stubSample(): Stub
    {
        $sample = $this->createStub(Sample::class);
        $sample->method('begin')->willReturnSelf();

        return $sample;
    }

    /**
     * @return Stub&ProfilerInterface
     */
    public function stubProfiler(): Stub
    {
        $profiler = $this->createStub(ProfilerInterface::class);
        $profiler->method('start')->willReturn($this->stubSample());
        $profiler->method('wrap')->willReturnCallback(fn ($fn) => $fn());

        return $profiler;
    }

    /**
     * Makes configurable stub for the specified class.
     *
     * @template RealInstanceType of object
     *
     * @param class-string<RealInstanceType> $originalClassName
     *
     * @return Stub&RealInstanceType
     */
    private function createStub(string $originalClassName): Stub
    {
        return $this->createMockObject($originalClassName);
    }

    /**
     * @template RealInstanceType of object
     *
     * @param class-string<RealInstanceType> $originalClassName
     *
     * @return MockObject&RealInstanceType
     */
    private function createMockObject(string $originalClassName): MockObject
    {
        return $this->test->getMockBuilder($originalClassName)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();
    }
}

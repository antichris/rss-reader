<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\misc;

use antichris\rssReader\misc\AbstractProvider;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use yii\di\Container;

/**
 * @internal
 * @coversNothing
 *
 * @template TUnderTest of AbstractProvider
 * @extends TestCase<TUnderTest>
 */
abstract class ProviderAbstractTest extends TestCase
{
    /**
     * @var AbstractProvider&TUnderTest
     */
    private $instance;
    /**
     * @var Container&MockObject
     */
    private $container;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->container = $this->createMock(Container::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @return array{string}[]
     */
    abstract public function provideGetClassName(): array;

    /**
     * @dataProvider provideGetClassName
     * @covers ::getClassName
     */
    public function testGetClassName(string $expected): void
    {
        $instance = $this->instance;
        $instance__getClassName = $this->internalCall($instance, 'getClassName');

        $actual = $instance__getClassName();

        $this->assertEquals($expected, $actual);
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->container];
    }
}

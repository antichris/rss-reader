<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

use Yii;
use yii\di\Container;

/**
 * Injects methods to store a reference to the Yii::$container before each single test (method call) and restore after.
 */
trait YiiContainerPreserverTrait
{
    /**
     * @var Container
     */
    private $__preservedYiiContainer;

    /**
     * @before
     */
    public function __storeYiiContainer(): void
    {
        if (!class_exists(Yii::class)) {
            require_once YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php';
        }
        $this->__preservedYiiContainer = Yii::$container;
    }

    /**
     * @after
     */
    public function __restoreYiiContainer(): void
    {
        Yii::$container = $this->__preservedYiiContainer;
    }
}

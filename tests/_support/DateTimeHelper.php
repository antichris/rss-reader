<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

use DateTime;
use DateTimeImmutable;
use Exception;

/**
 * Creates a DateTime or DateTimeImmutable instance from the given (or current if none given) UNIX timestamp without microseconds.
 */
class DateTimeHelper
{
    /**
     * Creates DateTime instance from the given (or current if none given) UNIX timestamp without microseconds.
     */
    public function create(?int $unixTime = null): DateTime
    {
        return $this->createFromFormat(DateTime::class, $unixTime);
    }

    /**
     * Creates DateTimeImmutable instance from the given (or current if none given) UNIX timestamp without microseconds.
     */
    public function createImmutable(?int $unixTime = null): DateTimeImmutable
    {
        return $this->createFromFormat(DateTimeImmutable::class, $unixTime);
    }

    /**
     * @template T&(DateTime|DateTimeImmutable)
     *
     * @param class-string<T> $dateTimeClass
     *
     * @return DateTime|DateTimeImmutable
     * @phpstan-return T&(DateTime|DateTimeImmutable)
     */
    protected function createFromFormat(string $dateTimeClass, ?int $unixTime = null)
    {
        if (null === $unixTime) {
            $unixTime = time();
        }

        $dateTime = $dateTimeClass::createFromFormat('U', (string) $unixTime);
        if (!$dateTime) {
            throw new Exception("Could not create {$dateTimeClass} from {$unixTime}");
        }

        return $dateTime;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

use Closure;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase as PHPUnitTestCase;
use function antichris\rssReader\internalCall;

/**
 * Common base class for tests.
 *
 * @template TUnderTest
 */
abstract class TestCase extends PHPUnitTestCase
{
    /**
     * Returns the class name for the instance under test.
     *
     * @return class-string<TUnderTest>
     */
    abstract protected function instanceClass(): string;

    /**
     * Returns the constructor arguments for the instance under test.
     *
     * @return mixed[]
     */
    protected function instanceCtorArgs(): array
    {
        return [];
    }

    /**
     * Constructs the instance under test.
     *
     * @return TUnderTest
     */
    protected function createInstance()
    {
        $instanceClass = $this->instanceClass();

        return new $instanceClass(...$this->instanceCtorArgs());
    }

    /**
     * Creates a partial mock of the instance under test.
     *
     * @param string[] $mockedMethods
     *
     * @return MockObject&TUnderTest
     */
    protected function mockInstance(array $mockedMethods = []): MockObject
    {
        return $this->buildInstanceMock($mockedMethods)->getMock();
    }

    /**
     * Returns an instance mock builder with constructor arguments (and, optionally,
     * mocked methods) already set.
     *
     * @param string[] $mockedMethods
     *
     * @return MockBuilder<TUnderTest>
     */
    protected function buildInstanceMock(array $mockedMethods = []): MockBuilder
    {
        return $this->getMockBuilder($this->instanceClass())
            ->setConstructorArgs($this->instanceCtorArgs())
            ->onlyMethods($mockedMethods);
    }

    /**
     * Returns a closure for calling a protected or private method of an instance.
     *
     * When called, the closure will pass all of its arguments to the method and
     * pass back the return value.
     *
     * @return Closure&callable(...mixed):mixed
     */
    protected function internalCall(object $instance, string $method): Closure
    {
        return internalCall($instance, $method);
    }

    /**
     * Returns a closure that sets a protected or private field of an instance.
     *
     * @return Closure&callable(mixed)
     */
    protected function internalSetter(object $instance, string $field): Closure
    {
        return (fn ($value) => $this->{$field} = $value)
            ->bindTo($instance, $instance);
    }

    /**
     * Returns a closure that returns the value of a protected or private field
     * of an instance.
     *
     * @return Closure&callable():mixed
     */
    protected function internalGetter(object $instance, string $field): Closure
    {
        return (fn () => $this->{$field})->bindTo($instance, $instance);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

/**
 * Injects lazy-loading getter for MockHelper.
 */
trait MockHelperTrait
{
    private ?MockHelper $mockHelper;

    public function helpMock(): MockHelper
    {
        if (empty($this->mockHelper)) {
            $this->mockHelper = new MockHelper($this);
        }

        return $this->mockHelper;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

/**
 * @todo Come up with a better way to keep books on invocations.
 */
class FakeArClass
{
    /**
     * @var callable
     */
    public static $callback;

    /**
     * @param mixed $condition
     *
     * @return mixed
     */
    public static function findOne($condition)
    {
        return self::callBack($condition);
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return self::callBack();
    }

    /**
     * @param mixed $args
     *
     * @return mixed
     */
    private static function callBack(...$args)
    {
        $callback = self::$callback;

        return $callback(...$args);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MockHelper
{
    public function __construct(
        private TestCase $testCase,
    ) {
    }

    /**
     * Prepares a partial mock builder with constructor arguments.
     *
     * @template TMockedClass
     *
     * @param class-string<TMockedClass> $mockedClass
     * @param mixed[]                    $constructorArgs
     * @param string[]                   $mockedMethods
     *
     * @return MockBuilder<TMockedClass>
     */
    public function build(string $mockedClass, array $constructorArgs, array $mockedMethods): MockBuilder
    {
        return $this->testCase
            ->getMockBuilder($mockedClass)
            ->setConstructorArgs($constructorArgs)
            ->onlyMethods($mockedMethods);
    }

    /**
     * Mocks a method to execute once and return nothing.
     *
     * @template TMockedClass
     *
     * @param TMockedClass&MockObject $mockObject
     * @param mixed                   $arguments
     *
     * @return InvocationMocker<TMockedClass>
     */
    public function methodVoidOnce(MockObject $mockObject, string $method, ...$arguments): InvocationMocker
    {
        return $mockObject
            ->expects($this->testCase->once())
            ->method($method)
            ->with(...$arguments);
    }

    /**
     * Mocks a method to execute once and return the given result.
     *
     * @template TMockedClass
     *
     * @param TMockedClass&MockObject $mockObject
     * @param mixed                   $returnValue
     * @param mixed                   $arguments
     *
     * @return InvocationMocker<TMockedClass&MockObject>
     */
    public function methodOnce(MockObject $mockObject, string $method, $returnValue, ...$arguments): InvocationMocker
    {
        return $this->methodVoidOnce($mockObject, $method, ...$arguments)
            ->willReturn($returnValue);
    }
}

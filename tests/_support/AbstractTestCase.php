<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

use PHPUnit\Framework\MockObject\MockObject;

/**
 * Common base class for tests of abstract classes.
 *
 * @template TUnderTest
 * @extends TestCase<TUnderTest>
 */
abstract class AbstractTestCase extends TestCase
{
    /**
     * Creates a mock instance of the abstract class under test.
     */
    protected function createInstance(): MockObject
    {
        return $this->buildInstanceMock()->getMockForAbstractClass();
    }

    /**
     * Creates a partial mock instance of the abstract class under test.
     *
     * @param string[] $mockedMethods
     */
    protected function mockInstance(array $mockedMethods = []): MockObject
    {
        return $this->buildInstanceMock($mockedMethods)->getMockForAbstractClass();
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

/**
 * Wraps nameCases() method around lazily injected CaseNamer sprintf().
 */
trait CaseNamerTrait
{
    private ?CaseNamer $caseNamer;

    /**
     * Generates case names mapping sprintf with the format string over args arrays.
     *
     * @param mixed[] $args
     *
     * @return string[]
     */
    public function nameCases(string $format, array ...$args): array
    {
        if (empty($this->caseNamer)) {
            $this->caseNamer = new CaseNamer();
        }

        return $this->caseNamer->sprintf($format, ...$args);
    }
}

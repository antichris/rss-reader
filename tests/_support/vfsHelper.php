<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamContainer;
use org\bovigo\vfs\vfsStreamContent;
use org\bovigo\vfs\vfsStreamDirectory;
use org\bovigo\vfs\vfsStreamFile;
use RuntimeException;
use function antichris\rssReader\base64md5;
use function antichris\rssReader\callerName;

/**
 * Keeps track of whether vfsStream has been set up and provides a handy API
 * around that.
 *
 * This aims to solve the problem that vfsStream objects are commonly prepared
 * during test data provider execution, before any tests run, but providers are
 * supposed not to communicate nor depend on a shared state. This encapsulates
 * that state initialization responsibility so that data providers can focus on
 * their specific tasks instead of synchronizing vfsStream setup.
 *
 * @todo Consider a more testable non-static implementation. Or removal.
 */
class vfsHelper
{
    private static bool $setUp = false;

    /**
     * Create vfsStream directory structure from an array and add it to the
     * given directory under a sub directory whose name is generated from the
     * given seed.
     *
     * @see vfsStream::create()
     *
     * @param array<string,mixed> $structure will be created
     * @param vfsStreamDirectory  $baseDir   for the new directory and structure
     * @param string              $nameSeed  is hashed to generate directory
     *                                       name. Defaults to the caller method
     *
     * @return vfsStreamDirectory holds the newly created structure
     */
    public static function create(
        array $structure,
        ?vfsStreamDirectory $baseDir = null,
        string $nameSeed = '',
    ): vfsStreamDirectory {
        self::softSetup();

        $hashable = $nameSeed ?: callerName();
        $name = base64md5($hashable);

        $result = vfsStream::create([$name => $structure], $baseDir)
            ->getChild($name);

        if (!$result instanceof vfsStreamDirectory) {
            // This should never happen.
            throw new RuntimeException('Could not get the created directory');
        }

        return $result;
    }

    /**
     * Create and add a new (empty) vfsStreamFile instance to given container.
     *
     * @see vfsStream::newFile()
     * @see vfsStreamContainer::add()
     *
     * @param vfsStreamContainer<vfsStreamContent> $container,
     */
    public static function addNewFile(
        vfsStreamContainer $container,
        string $name,
        ?int $permissions = null,
    ): vfsStreamFile {
        return vfsStream::newFile($name, $permissions)->at($container);
    }

    /**
     * Call vfsStream::setup() and mark as set up only if that has not been done
     * yet.
     */
    private static function softSetup(): void
    {
        if (self::$setUp) {
            return;
        }
        vfsStream::setup();
        self::$setUp = true;
    }
}

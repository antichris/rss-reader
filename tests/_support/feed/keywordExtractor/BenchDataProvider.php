<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use Generator;
use function antichris\rssReader\internalCall;

/**
 * Provides benchmarks with data.
 */
trait BenchDataProvider
{
    /**
     * @return Generator<string,array{string},mixed,void>
     */
    public function provideStrings(): Generator
    {
        yield 'words' => ["Word: another; dash-word, apostroph'd, l33t"];
        yield 'words and numbers' => [<<<'EOD'
            Word: another; 123 dash-word, 1-2 apostroph'd, 12.34 l33t 1,234.56
            EOD];
        yield 'passage' => [<<<'EOD'
            There are certain queer times and occasions in this strange mixed
            affair we call life when a man takes this whole universe for a vast
            practical joke, though the wit thereof he but dimly discerns, and
            more than suspects that the joke is at nobody’s expense but his own.
            However, nothing dispirits, and nothing seems worth while disputing.
            He bolts down all events, all creeds, and beliefs, and persuasions,
            all hard things visible and invisible, never mind how knobby; as an
            ostrich of potent digestion gobbles down bullets and gun flints. And
            as for small difficulties and worryings, prospects of sudden
            disaster, peril of life and limb; all these, and death itself, seem
            to him only sly, good-natured hits, and jolly punches in the side
            bestowed by the unseen and unaccountable old joker. That odd sort of
            wayward mood I am speaking of, comes over a man only in some time of
            extreme tribulation; it comes in the very midst of his earnestness,
            so that what just before might have seemed to him a thing most
            momentous, now seems but a part of the general joke. There is
            nothing like the perils of whaling to breed this free and easy sort
            of genial, desperado philosophy; and with it I now regarded this
            whole voyage of the Pequod, and the great White Whale its object.
            EOD];
    }

    /**
     * @return Generator<string,array{string[]},mixed,void>
     */
    public function provideWords(): Generator
    {
        /**
         * @var callable(string):string[]
         */
        $extractWords = internalCall(new WordExtractor(), 'extractWords');

        foreach ($this->provideStrings() as $name => $params) {
            yield $name => [$extractWords($params[0])];
        }
    }
}

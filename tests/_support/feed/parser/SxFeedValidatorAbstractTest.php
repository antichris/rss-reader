<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxFeedValidator;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;

/**
 * Abstract base class for concrete AbstractSxFeedValidator implementation tests.
 *
 * @internal
 * @coversNothing
 *
 * @method AbstractSxFeedValidator&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @template TUnderTest of AbstractSxFeedValidator
 * @extends TestCase<TUnderTest>
 */
abstract class SxFeedValidatorAbstractTest extends TestCase
{
    private const METHOD_GET_FEED = 'getFeed';
    private const METHOD_VALIDATE_FEED = 'validateFeed';
    private const METHOD_VALIDATE_ROOT = 'validateRoot';

    /**
     * Provides case data for testing the validateRoot() method.
     *
     * @return array<string,array{bool,string}>
     */
    abstract public function provideValidateRoot(): array;

    /**
     * Provides case data for testing the getFeed() method.
     *
     * @return array<string,array{bool,string}>
     */
    abstract public function provideGetFeed(): array;

    /**
     * Provides case data for testing the validateFeed() method.
     *
     * @return array<string,array{bool,string}>
     */
    abstract public function provideValidateFeed(): array;

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideValidateRoot
     * @covers ::validateRoot
     */
    public function testValidateRoot(bool $expected, string $xml): void
    {
        $root = new SimpleXMLElement($xml);

        $instance = $this->mockInstance([self::METHOD_GET_FEED, self::METHOD_VALIDATE_FEED]);
        if ($expected) {
            $expectGetFeed = $this->once();
            $expectValidateFeed = $this->once();
        } else {
            $expectGetFeed = $this->never();
            $expectValidateFeed = $this->never();
        }
        $instance->expects($expectGetFeed)->method(self::METHOD_GET_FEED)->with($root)->willReturn($root);
        $instance->expects($expectValidateFeed)->method(self::METHOD_VALIDATE_FEED)->with($root)->willReturn($expected);

        $actual = $instance->validate($root);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideGetFeed
     * @covers ::getFeed
     */
    public function testGetFeed(bool $expected, string $xml): void
    {
        $root = new SimpleXMLElement($xml);

        $instance = $this->mockInstance([self::METHOD_VALIDATE_ROOT, self::METHOD_VALIDATE_FEED]);
        $this->mockInstanceValidateRoot($instance, $root, true);
        $instance->expects($this->once())->method(self::METHOD_VALIDATE_FEED)->withAnyParameters()->willReturn($expected);

        $actual = $instance->validate($root);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideValidateFeed
     * @covers ::validateFeed
     */
    public function testValidateFeed(bool $expected, string $xml): void
    {
        $root = new SimpleXMLElement('<xml/>');
        $feed = new SimpleXMLElement($xml);

        $instance = $this->mockInstance([self::METHOD_VALIDATE_ROOT, self::METHOD_GET_FEED]);
        $this->mockInstanceValidateRoot($instance, $root, true);
        $instance->expects($this->once())->method(self::METHOD_GET_FEED)->with($root)->willReturn($feed);

        $actual = $instance->validate($root);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Mocks the AbstractSxFeedValidator::validateRoot() method.
     *
     * @return InvocationMocker<TUnderTest>
     */
    private function mockInstanceValidateRoot(MockObject $instance, SimpleXMLElement $root, bool $result): InvocationMocker
    {
        return $instance->expects($this->once())
            ->method(self::METHOD_VALIDATE_ROOT)
            ->with($root)
            ->willReturn($result);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\feed\parser;

use antichris\rssReader\feed\parser\AbstractDateTimeParser;
use antichris\rssReader\tests\_support\TestCase;

/**
 * Abstract base class for testing concrete AbstractDateTimeParser implementations.
 *
 * @internal
 * @coversNothing
 *
 * @method AbstractDateTimeParser createInstance() Constructs the instance under test.
 *
 * @template TUnderTest of AbstractDateTimeParser
 * @extends TestCase<TUnderTest>
 */
abstract class DateTimeParserAbstractTest extends TestCase
{
    /**
     * @var AbstractDateTimeParser
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = $this->createInstance();
    }

    /**
     * Provides case data for testing the getDateFormat() method.
     *
     * @return array{string}[]
     */
    abstract public function provideGetDateFormat(): array;

    /**
     * @dataProvider provideGetDateFormat
     * @covers ::getDateFormat
     */
    public function testGetDateFormat(string $expected): void
    {
        $instance = $this->instance;
        $instance__getDateFormat = $this->internalCall($instance, 'getDateFormat');

        $actual = $instance__getDateFormat();

        $this->assertSame($expected, $actual);
    }
}

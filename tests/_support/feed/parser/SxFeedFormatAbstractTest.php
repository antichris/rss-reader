<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxFeedFormat;
use antichris\rssReader\tests\_support\TestCase;

/**
 * Abstract base class for concrete AbstractSxFeedFormat implementation tests.
 *
 * @internal
 * @coversNothing
 *
 * @method AbstractSxFeedFormat createInstance() Constructs the instance under test.
 *
 * @template TUnderTest of AbstractSxFeedFormat
 * @extends TestCase<TUnderTest>
 */
abstract class SxFeedFormatAbstractTest extends TestCase
{
    /**
     * @return array{string}[]
     */
    abstract public function provideGetName(): array;

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideGetName
     * @covers ::getName
     */
    public function testGetName(string $expected): void
    {
        $instance = $this->createInstance();

        $actual = $instance->getName();

        $this->assertEquals($expected, $actual);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxFeedMapper;
use antichris\rssReader\feed\parser\SxItemMapperInterface;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;

/**
 * Abstract base class for concrete AbstractSxFeedMapper implementation tests.
 *
 * @internal
 * @coversNothing
 *
 * @method AbstractSxFeedMapper            createInstance()                           Constructs the instance under test.
 * @method AbstractSxFeedMapper&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @template TUnderTest of AbstractSxFeedMapper
 * @extends TestCase<TUnderTest>
 */
abstract class SxFeedMapperAbstractTest extends TestCase
{
    private const METHOD_GET_FEED = 'getFeed';
    private const METHOD_GET_ITEM_MAPPER = 'getItemMapper';
    private const METHOD_MAP_FEED_ITEMS = 'mapFeedItems';

    /**
     * Provides case data for testing the getItemTag() method.
     *
     * @return array{string}[]
     */
    abstract public function provideGetItemTag(): array;

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideGetItemTag
     * @covers ::getItemTag
     */
    public function testGetItemTag(string $itemTag): void
    {
        $root = new SimpleXMLElement('<xml/>');
        $feed = $root;
        $limit = 13;
        $expected = ['<expected>'];

        $instance = $this->mockInstance([self::METHOD_GET_FEED, self::METHOD_GET_ITEM_MAPPER, self::METHOD_MAP_FEED_ITEMS]);
        $mapper = $this->createMock(SxItemMapperInterface::class);
        $instance->expects($this->once())->method(self::METHOD_GET_FEED)->with($root)->willReturn($feed);
        $instance->expects($this->once())->method(self::METHOD_GET_ITEM_MAPPER)->willReturn($mapper);
        $instance->expects($this->once())
            ->method(self::METHOD_MAP_FEED_ITEMS)
            ->with($mapper, $itemTag, $feed, $limit)
            ->willReturn($expected);

        $actual = $instance->map($root, $limit);

        $this->assertEquals($expected, $actual);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\_support\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxItemMapper;
use antichris\rssReader\tests\_support\DateTimeHelper;
use antichris\rssReader\tests\_support\TestCase;
use DateTimeImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;
use SimpleXMLElement;

/**
 * Abstract base class for concrete AbstractSxItemMapper implementation tests.
 *
 * @internal
 * @coversNothing
 *
 * @method AbstractSxItemMapper            createInstance()                           Constructs the instance under test.
 * @method AbstractSxItemMapper&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @template TUnderTest of AbstractSxItemMapper
 * @extends TestCase<TUnderTest>
 */
abstract class SxItemMapperAbstractTest extends TestCase
{
    private const METHOD_PARSE_DATE_TIME = 'parseDateTime';

    /**
     * @var AbstractSxItemMapper
     */
    private $instance;
    /**
     * @var DateTimeHelper
     */
    private $dateTimeHelper;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        // TODO Consider dependency injection here.
        $this->dateTimeHelper = new DateTimeHelper();
    }

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = $this->createInstance();
    }

    /**
     * Provides case data for testing the getPubDate() method.
     *
     * @return array<string,array{?DateTimeImmutable,string}>
     */
    abstract public function provideGetPubDate(): array;

    /**
     * Provides case data for testing the getUpdated() method.
     *
     * @return array<string,array{?DateTimeImmutable,string}>
     */
    abstract public function provideGetUpdated(): array;

    /**
     * Provides case data for testing the getImageURL() method.
     *
     * @return array<string,array{?string,string}>
     */
    abstract public function provideGetImageUrl(): array;

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideGetPubDate
     * @covers ::getPubDate
     */
    public function testGetPubDate(?DateTimeImmutable $expected, string $xml): void
    {
        $item = $this->newSimpleXMLItem($xml);
        $instance = $this->mockInstanceParseDateTime($this->expectParseDateTimeInGetPubDate(), $expected);

        $actual = $instance->getPubDate($item);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideGetUpdated
     * @covers ::getUpdated
     */
    public function testGetUpdated(?DateTimeImmutable $expected, string $xml): void
    {
        $item = $this->newSimpleXMLItem($xml);
        $instance = $this->mockInstanceParseDateTime($this->expectParseDateTimeInGetUpdated(), $expected);

        $actual = $instance->getUpdated($item);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideGetImageUrl
     * @covers ::getImageURL
     */
    public function testGetImageUrl(?string $expected, string $xml): void
    {
        $item = $this->newSimpleXMLItem($xml);
        $instance = $this->instance;

        $actual = $instance->getImageURL($item);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Returns matcher for invocations of parseDateTime() during the test of getPubDate().
     *
     * In other words, return $this->once() to indicate that the getPubDate() implementation calls parseDateTime() once
     * to produce the return value, return $this->never() to indicate that the getPubDate() implementation never calls
     * parseDateTime().
     */
    abstract protected function expectParseDateTimeInGetPubDate(): InvocationOrder;

    /**
     * Returns matcher for invocations of parseDateTime() during the test of getPubDate().
     *
     * In other words, return $this->once() to indicate that the getPubDate() implementation calls parseDateTime() once
     * to produce the return value, return $this->never() to indicate that the getPubDate() implementation never calls
     * parseDateTime().
     */
    abstract protected function expectParseDateTimeInGetUpdated(): InvocationOrder;

    /**
     * Creates DateTimeImmutable instance from the given (or current if none given) UNIX timestamp without microseconds.
     */
    protected function createDateTimeImmutable(?int $unixTime = null): DateTimeImmutable
    {
        return $this->dateTimeHelper->createImmutable($unixTime);
    }

    /**
     * Returns the instance under test.
     */
    protected function getInstance(): AbstractSxItemMapper
    {
        return $this->instance;
    }

    /**
     * Creates a new SimpleXMLElement instance for representing the item element in tests.
     */
    protected function newSimpleXMLItem(string $innerXml): SimpleXMLElement
    {
        return new SimpleXMLElement("<xml>{$innerXml}</xml>");
    }

    /**
     * @return AbstractSxItemMapper&MockObject
     */
    private function mockInstanceParseDateTime(InvocationOrder $invocationRule, ?DateTimeImmutable $result): MockObject
    {
        $instance = $this->mockInstance([self::METHOD_PARSE_DATE_TIME]);
        $instance->expects($invocationRule)
            ->method(self::METHOD_PARSE_DATE_TIME)
            ->willReturn($result);

        return $instance;
    }
}

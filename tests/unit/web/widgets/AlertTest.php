<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\web\widgets;

use antichris\rssReader\web\widgets\Alert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Yii;
use yii\web\Application;
use yii\web\Session;
use yii\web\View;

/**
 * @internal
 * @coversDefaultClass \antichris\rssReader\web\widgets\Alert
 *
 * @phpstan-import-type tCloseButtonOptions from Alert
 * @phpstan-type tAlertTypes array<string,string>
 * @phpstan-type tConfig array{alertTypes?:tAlertTypes,closeButton?:tCloseButtonOptions,id?:?string}
 * @phpstan-type tRxParams array{id?:string,class?:string,closeButton?:string,message?:string}
 */
final class AlertTest extends TestCase
{
    private const DEFAULT_MESSAGE = 'message';

    /** @var MockObject&Application */
    private MockObject $app;
    /** @var MockObject&Session */
    private MockObject $session;
    /** @var MockObject&View */
    private MockObject $view;

    public static function setUpBeforeClass(): void
    {
        require_once YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php';
    }

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->app = $this->createMock(Application::class);
        $this->session = $this->createMock(Session::class);
        $this->view = $this->createMock(View::class);

        Yii::$container->setSingleton(Session::class, fn () => $this->session);
        Yii::$app = $this->app;

        $this->app->method('getView')->willReturn($this->view);
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $id = '<id>';
        $expected = $id;
        $instance = Alert::begin(compact('id'));

        $actual = $instance->id;
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideClassOption
     * @covers ::run
     */
    public function testClassOption(
        string $expected,
        ?string $class = null,
    ): void {
        $this->setInfoFlashes();
        $config = ['options' => ['class' => $class]];

        $rx = $this->alertRegex(class: "\\S+ {$expected} ?alert( [^\"]+)?");
        $this->assertRenderedWidgetMatchesRegex($rx, $config);
    }

    /**
     * @return array<string,array{string,1?:string}>
     */
    public function provideClassOption(): array
    {
        $class = 'my-class';

        return [
            'no class' => [''],
            'empty string' => ['', ''],
            'custom' => [$class, $class],
        ];
    }

    /**
     * @dataProvider provideSkipUnknownTypeFlashes
     * @covers ::run
     *
     * @param tAlertTypes $alertTypes
     */
    public function testSkipUnknownTypeFlashes(
        bool $expected,
        string $type,
        ?array $alertTypes = null,
    ): void {
        $this->setTypedFlashes($type);
        $config = $this->configAlertTypes($alertTypes);

        $actual = Alert::widget($config);

        if ($expected) {
            $this->assertNotEmpty($actual);
        } else {
            $this->assertEmpty($actual);
        }
    }

    /**
     * @return array<string,array{bool,string,2?:tAlertTypes}>
     */
    public function provideSkipUnknownTypeFlashes(): array
    {
        $type = 'my-type';
        $class = 'my-class';

        $alertTypes = [$type => $class];

        return [
            'unknown' => [false, $type],
            'custom alertTypes' => [false, 'info', $alertTypes],
            'known' => [true, 'info'],
            'known custom' => [true, $type, $alertTypes],
        ];
    }

    /**
     * @dataProvider provideCloseButton
     * @covers ::run
     *
     * @param tCloseButtonOptions $closeButton
     */
    public function testCloseButton(
        string $expected,
        mixed $closeButton = null,
    ): void {
        $this->setInfoFlashes();
        $config = $closeButton !== null
            ? ['closeButton' => $closeButton]
            : [];

        $pattern = $this->alertRegex(closeButton: $expected);
        $this->assertRenderedWidgetMatchesRegex($pattern, $config);
    }

    /**
     * @return array<string,array{string,2?:tCloseButtonOptions}>
     */
    public function provideCloseButton(): array
    {
        return [
            'default' => [
                '<button type="button" class="btn-close"[^>]*></button>',
            ],
            'disabled' => ['', false],
            '<a> tag' => ['<a[^>]+></a>', ['tag' => 'a']],
            'custom label' => ['<[^>]+>foo</[^>]+>', ['label' => 'foo']],
            'extra attribute' => [
                '<[^>]+ bar="qux"[^>]*></[^>]+>',
                ['bar' => 'qux'],
            ],
        ];
    }

    /**
     * @dataProvider provideId
     * @covers ::run
     */
    public function testId(
        string $expected,
        string $type,
        ?string $id = null,
    ): void {
        $this->setTypedFlashes($type);
        $config = ['id' => $id];

        $pattern = $this->alertRegex(id: $expected);
        $this->assertRenderedWidgetMatchesRegex($pattern, $config);
    }

    /**
     * @return array<string,array{string,1?:?string}>
     */
    public function provideId(): array
    {
        $id = 'my-id';
        $type = 'error';
        $default = "w\\d+-{$type}-0";

        return [
            'unspecified' => [$default, $type],
            'null' => [$default, $type, null],
            'custom' => ["{$id}-{$type}-0", $type, $id],
            '"0"' => ["0-{$type}-0", $type, '0'],
            'empty string' => ["-{$type}-0", $type, ''],
        ];
    }

    /**
     * @dataProvider provideTypeCssClass
     * @covers ::run
     *
     * @param tAlertTypes $alertTypes
     */
    public function testTypeCssClass(
        string $expected,
        string $type,
        ?array $alertTypes = null,
    ): void {
        $this->setTypedFlashes($type);
        $config = $this->configAlertTypes($alertTypes);

        $pattern = $this->alertRegex(class: "{$expected}( [^\"]+)?");
        $this->assertRenderedWidgetMatchesRegex($pattern, $config);
    }

    /**
     * @return array<string,array{string,string,2?:tAlertTypes}>
     */
    public function provideTypeCssClass(): array
    {
        $type = 'my-type';
        $class = 'my-class';

        $types = ['danger', 'success', 'info', 'warning'];

        return array_combine($types, array_map(
            fn ($type) => ["alert-{$type}", $type],
            $types,
        )) + [
            'error' => ['alert-danger', 'error'],
            'custom type' => [$class, $type, [$type => $class]],
        ];
    }

    /**
     * @covers ::run
     */
    public function testMultipleMessages(): void
    {
        $messages = ['foo', 'bar', 'qux'];
        $this->setFlashes(['info' => $messages]);

        $pattern = implode('', array_map(
            fn ($ms) => $this->alertRegex(message: $ms, partial: true),
            $messages,
        ));
        $this->assertRenderedWidgetMatchesRegex("(^{$pattern}$)", []);
    }

    /**
     * @internal
     * @dataProvider provideAlertRegex
     * @covers \antichris\rssReader\tests\unit\web\widgets\AlertTest::alertRegex
     *
     * @param tRxParams $rxParams
     * @param tConfig   $config
     */
    public function testAlertRegex(
        array $rxParams,
        array $config = [],
        string $message = self::DEFAULT_MESSAGE,
    ): void {
        $this->setInfoFlashes([$message]);

        $pattern = $this->alertRegex(...$rxParams);

        $this->assertRenderedWidgetMatchesRegex($pattern, $config);
    }

    /**
     * @return array<string,array{tRxParams,1?:tConfig,2?:string}>
     */
    public function provideAlertRegex(): array
    {
        $id = 'custom';
        $class = 'foo';
        $closeButton = 'very special close button';
        $message = 'hello world';

        return [
            'everything default' => [[]],
            'custom ID' => [
                // The widget uses the config value as prefix for the actual id.
                ['id' => $id . '-info-0'],
                ['id' => $id],
            ],
            'custom class' => [
                ['class' => "[^\"]+ {$class} [^\"]+"],
                ['options' => ['class' => $class]],
            ],
            'no close button' => [
                ['closeButton' => ''],
                ['closeButton' => false],
            ],
            'special close button' => [
                ['closeButton' => ".*{$closeButton}.*"],
                ['closeButton' => ['label' => $closeButton]],
            ],
            'custom message' => [['message' => $message], [], $message],
        ];
    }

    /**
     * @param string[] $flashes
     */
    private function setInfoFlashes(
        array $flashes = [self::DEFAULT_MESSAGE],
    ): void {
        $this->setTypedFlashes('info', $flashes);
    }

    /**
     * The single source of truth.
     *
     * The string parameters are all concatenated with(in) the larger regular
     * expression, so you cannnot use anchors (^ an $) to delimit these
     * sub-patterns.
     *
     * You are free to use the forward slash character (/) in sub-patterns
     * unescaped, since by default the result is delimited by parentheses.
     *
     * @param bool $partial when true, the result doesn't include the start and
     *                      end anchors (^ and $) and regex delimiters, so it
     *                      can be concatenated with other strings to form a
     *                      larger pattern
     */
    private function alertRegex(
        string $id = '[^"]+',
        string $class = '[^"]+',
        string $closeButton = '<[^\n]+>',
        string $message = self::DEFAULT_MESSAGE,
        bool $partial = false,
    ): string {
        $pattern = <<<EOD
            <div id="{$id}" class="{$class}" role="alert">
            \n{$message}
            {$closeButton}\n
            </div>
            EOD;

        return $partial
            ? $pattern
            : "(^{$pattern}$)";
    }

    /**
     * @param tConfig $config
     */
    private function assertRenderedWidgetMatchesRegex(
        string $pattern,
        array $config,
    ): void {
        $actual = Alert::widget($config);
        $this->assertMatchesRegularExpression($pattern, $actual);
    }

    /**
     * @param string[] $flashes
     */
    private function setTypedFlashes(
        string $type,
        array $flashes = [self::DEFAULT_MESSAGE],
    ): void {
        $this->setFlashes([$type => $flashes]);
    }

    /**
     * @param ?tAlertTypes $alertTypes
     * @param tConfig      $config
     *
     * @return tConfig
     */
    private function configAlertTypes(
        ?array $alertTypes,
        array $config = [],
    ): array {
        if ($alertTypes !== null) {
            $config['alertTypes'] = $alertTypes;
        }

        return $config;
    }

    /**
     * @param array<string,string[]> $flashes
     */
    private function setFlashes(
        array $flashes,
        ?MockObject $session = null,
    ): void {
        $session ??= $this->session;
        $session->method('getAllFlashes')->willReturn($flashes);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\web;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\web\View;
use PHPUnit\Framework\MockObject\MockObject;
use yii\base\Application;
use yii\i18n\Formatter;
use yii\i18n\I18N;
use yii\web\Application as WebApplication;
use yii\web\User as UserComponent;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\web\View
 *
 * @method View createInstance()
 *
 * @extends TestCase<View>
 */
class ViewTest extends TestCase
{
    /**
     * @var MockObject&Application
     */
    private $app;
    /**
     * @var MockObject&Formatter
     */
    private $formatter;
    /**
     * @var MockObject&I18N
     */
    private $i18n;
    /**
     * @var MockObject&UserComponent
     */
    private $userComponent;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->app = $this->mockWebApp();
        $this->formatter = $this->createMock(Formatter::class);
        $this->i18n = $this->createMock(I18N::class);
        $this->userComponent = $this->createMock(UserComponent::class);
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $expectedApp = $this->mockBaseApp();
        $this->app = $expectedApp;

        $instance = $this->createInstance();

        $actualApp = $this->internalGetter($instance, 'app')();
        $this->assertSame($expectedApp, $actualApp);
    }

    /**
     * @covers ::getAppName
     */
    public function testGetAppName(): void
    {
        $this->testAppProperyGetter(
            expected: '<appName>',
            property: 'name',
            viewProp: 'appName',
        );
    }

    /**
     * @covers ::getCharset
     */
    public function testGetCharset(): void
    {
        $this->testAppProperyGetter(
            expected: '<charset>',
            property: 'charset',
        );
    }

    /**
     * @covers ::getLanguage
     */
    public function testGetLanguage(): void
    {
        $this->testAppProperyGetter(
            expected: '<lang>',
            property: 'language',
        );
    }

    /**
     * @covers ::getFormatter
     */
    public function testGetFormatter(): void
    {
        $this->assertInstanceGetterAndPropertyEqual(
            expected: $this->formatter,
            property: 'formatter',
        );
    }

    /**
     * @covers ::getI18n
     */
    public function testGetI18n(): void
    {
        $this->assertInstanceGetterAndPropertyEqual(
            expected: $this->i18n,
            property: 'i18n',
        );
    }

    /**
     * @covers ::getUserComponent
     */
    public function testGetUserComponent(): void
    {
        $this->assertInstanceGetterAndPropertyEqual(
            expected: $this->userComponent,
            property: 'userComponent',
        );
    }

    /**
     * @dataProvider provideGetHomeUrl
     * @covers ::getHomeUrl
     *
     * @param MockObject&Application $app
     */
    public function testGetHomeUrl(
        string $expected,
        Application $app,
    ): void {
        $this->app = $app;
        $this->assertInstanceGetterAndPropertyEqual(
            expected: $expected,
            property: 'homeUrl',
        );
    }

    /**
     * @return array<string,array{string,MockObject&Application}>
     */
    public function provideGetHomeUrl(): array
    {
        $homeUrl = '<homeUrl>';

        $webApp = $this->mockWebApp();
        $webApp->method('getHomeUrl')->willReturn($homeUrl);

        return [
            'base app' => ['', $this->mockBaseApp()],
            'web app' => [$homeUrl, $webApp],
        ];
    }

    protected function instanceClass(): string
    {
        return View::class;
    }

    /**
     * @return array{Application,Formatter,I18n,UserComponent}
     */
    protected function instanceCtorArgs(): array
    {
        return [
            $this->app,
            $this->formatter,
            $this->i18n,
            $this->userComponent,
        ];
    }

    /**
     * @return MockObject&WebApplication
     */
    private function mockWebApp()
    {
        return $this->createMock(WebApplication::class);
    }

    /**
     * @return MockObject&Application
     */
    private function mockBaseApp()
    {
        return $this->createMock(Application::class);
    }

    private function testAppProperyGetter(
        string $expected,
        string $property,
        ?string $viewProp = null,
        ?string $getter = null,
    ): void {
        $viewProp ??= $property;

        $this->app->{$property} = $expected;

        $this->assertInstanceGetterAndPropertyEqual(
            expected: $expected,
            property: $viewProp,
            getter: $getter,
        );
    }

    private function assertInstanceGetterAndPropertyEqual(
        mixed $expected,
        string $property,
        ?string $getter = null,
    ): void {
        $getter ??= "get{$property}";
        $instance = $this->createInstance();

        $this->assertEquals($expected, $instance->{$getter}());
        $this->assertEquals($expected, $instance->{$property});
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\web\assets;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\web\assets\ManifestAsset;
use antichris\rssReader\web\assets\ManifestFile;
use DomainException;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use yii\web\AssetManager;
use yii\web\View;

/**
 * @internal
 * @coversDefaultClass \antichris\rssReader\web\assets\ManifestAsset
 *
 * @phpstan-import-type tManifest from ManifestAsset
 *
 * @extends TestCase<ManifestAsset>
 */
final class ManifestAssetTest extends TestCase
{
    private const METHOD_FILE = 'getFile';
    private const METHOD_FILE_READ = 'read';

    private const KEY_ICONS = ManifestAsset::KEY_ICONS;

    private const DEFAULT = '<default>';

    private const MANAGER_BASE_PATH = '<manager-base-path>';
    private const MANAGER_BASE_URL = '<manager-base-url>';
    private const SRC = '<src>';
    private const URL = '<url>';
    private const PATH = '<path>';

    /**
     * @dataProvider provideRegisterAssetFiles
     * @covers ::registerAssetFiles
     *
     * @param tManifest $manifest
     */
    public function testRegisterAssetFiles(
        bool $expectMeta,
        array $manifest,
        ?string $expectedColor = null,
    ): void {
        $url = self::URL;
        $path = self::PATH;

        $metaExpectation = $expectMeta
            ? $this->once()
            : $this->never();

        $view = $this->mockView();
        $manager = $this->mockAssetManager();
        $instance = $this->mockInstance([self::METHOD_FILE]);
        $file = $this->mockFile($path, [self::METHOD_FILE_READ]);

        $view->method('getAssetManager')->willReturn($manager);
        $manager->method('getAssetUrl')
            ->with($instance, $instance->manifestFile)
            ->willReturn($url);
        $instance->method(self::METHOD_FILE)
            ->with($manager)
            ->willReturn($file);
        $file->method(self::METHOD_FILE_READ)->willReturn($manifest);

        $view->expects($this->once())
            ->method('registerLinkTag')
            ->with(['href' => $url, 'rel' => 'manifest']);
        $view->expects($metaExpectation)
            ->method('registerMetaTag')
            ->with(['name' => 'theme-color', 'content' => $expectedColor]);

        $instance->registerAssetFiles($view);
    }

    /**
     * @return array<string,array{bool,tManifest,2?:string}>
     */
    public function provideRegisterAssetFiles(): array
    {
        $key = ManifestAsset::KEY_THEME_COLOR;
        $color = '#58f';

        return [
            'no theme color' => [false, []],
            'null theme color' => [false, [$key => null]],
            'empty theme color string' => [false, [$key => '']],
            '"0" theme color string' => [false, [$key => '0']],
            'theme color' => [true, [$key => $color], $color],
        ];
    }

    /**
     * @dataProvider providePublish
     * @covers ::publish
     */
    public function testPublish(
        bool $expectPublish,
        ?string $expectedBasePath,
        ?string $expectedBaseUrl,
        ?string $sourcePath,
        ?string $basePath,
        ?string $baseUrl,
    ): void {
        $instance = $this->mockInstance(['publishManifest']);
        if ($sourcePath !== self::DEFAULT) {
            $instance->sourcePath = $sourcePath;
        }
        if ($basePath !== self::DEFAULT) {
            $instance->basePath = $basePath;
        }
        if ($baseUrl !== self::DEFAULT) {
            $instance->baseUrl = $baseUrl;
        }

        $publishExpectation = $expectPublish
            ? $this->once()
            : $this->never();

        $manager = $this->mockAssetManager();
        $manager->expects($publishExpectation)
            ->method('publish')
            ->with($instance->sourcePath, $instance->publishOptions)
            ->willReturn([self::MANAGER_BASE_PATH, self::MANAGER_BASE_URL]);
        $instance->expects($this->once())
            ->method('publishManifest')
            ->with($manager);

        $instance->publish($manager);

        $this->assertEquals($expectedBasePath, $instance->basePath);
        $this->assertEquals($expectedBaseUrl, $instance->baseUrl);
    }

    /**
     * @return array<string,array{bool,?string,?string,?string,?string,?string}>
     */
    public function providePublish(): array
    {
        $path = '<base-path>';
        $url = '<base-url>';
        $src = '<source-path>';

        return [
            'default' => [
                true, self::MANAGER_BASE_PATH, self::MANAGER_BASE_URL,
                self::DEFAULT, self::DEFAULT, self::DEFAULT,
            ],
            'sourcePath, no basePath nor baseUrl' => [
                true, self::MANAGER_BASE_PATH, self::MANAGER_BASE_URL,
                $src, null, null,
            ],
            'sourcePath & basePath, no baseUrl' => [
                true, self::MANAGER_BASE_PATH, self::MANAGER_BASE_URL,
                $src, $path, null,
            ],
            'sourcePath & baseUrl, no basePath' => [
                true, self::MANAGER_BASE_PATH, self::MANAGER_BASE_URL,
                $src, null, $url,
            ],
            'sourcePath, basePath & baseUrl' => [
                false, $path, $url, $src, $path, $url,
            ],
            'basePath & baseUrl, no sourcePath' => [
                false, $path, $url, null, $path, $url,
            ],
            'basePath, no sourcePath nor baseUrl' => [
                false, $path, null, null, $path, null,
            ],
            'baseUrl, no sourcePath nor basePath' => [
                false, null, $url, null, null, $url,
            ],
            'no sourcePath, nor basePath, nor baseUrl' => [
                false, null, null, null, null, null,
            ],
        ];
    }

    /**
     * @dataProvider provideGetFile
     * @covers ::getFile
     */
    public function testGetFile(
        ?Exception $exception,
        string|false $path,
    ): void {
        $manifestFile = '<manifestFile>';
        $expected = new ManifestFile($path ?: '');

        $instance = $this->mockInstance();
        $instance->manifestFile = $manifestFile;

        $manager = $this->mockAssetManager();
        $manager->method('getAssetPath')
            ->with($instance, $instance->manifestFile)
            ->willReturn($path);

        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $actual = $this->internalCall($instance, self::METHOD_FILE)($manager);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{?Exception,string|false}>
     */
    public function provideGetFile(): array
    {
        return [
            'false path' => [new DomainException('Could not resolve manifest file path'), false],
            'a path' => [null, self::PATH],
        ];
    }

    /**
     * @dataProvider providePublishManifest
     * @covers ::publishManifest
     */
    public function testPublishManifest(
        bool $expectUpdate,
        bool $isUpToDate,
    ): void {
        $path = self::PATH;

        $instance = $this->mockInstance([self::METHOD_FILE, 'updateManifest']);
        $manager = $this->mockAssetManager();
        $file = $this->mockFile($path, ['isUpToDate', 'markUpToDate']);
        $instance->method(self::METHOD_FILE)
            ->with($manager)
            ->willReturn($file);
        $file->expects($this->once())
            ->method('isUpToDate')
            ->willReturn($isUpToDate);

        if ($expectUpdate) {
            $expectUpdateManifest = $this->once();
            $expectMarkUpToDate = $this->once();
        } else {
            $expectUpdateManifest = $this->never();
            $expectMarkUpToDate = $this->never();
        }

        $instance->expects($expectUpdateManifest)
            ->method('updateManifest')
            ->with($manager, $file);
        $file->expects($expectMarkUpToDate)
            ->method('markUpToDate')
            ->with();

        $this->internalCall($instance, 'publishManifest')($manager);
    }

    /**
     * @return array<string,array{bool,bool}>
     */
    public function providePublishManifest(): array
    {
        return [
            'up to date' => [false, true],
            'not up to date' => [true, false],
        ];
    }

    /**
     * @dataProvider provideUpdateManifest
     * @covers ::updateManifest
     *
     * @param tManifest $manifest
     * @param tManifest $expectedManifest
     */
    public function testUpdateManifest(
        int $expectGetAssetUrl,
        array $manifest,
        array $expectedManifest = [],
    ): void {
        $path = self::PATH;

        if ($expectGetAssetUrl > 0) {
            $getAssetUrlExpectation = $this->exactly($expectGetAssetUrl);
            $writeExpectation = $this->once();
        } else {
            $getAssetUrlExpectation = $this->never();
            $writeExpectation = $this->never();
        }

        $instance = $this->mockInstance();
        $file = $this->mockFile($path, [self::METHOD_FILE_READ, 'write']);
        $file->method(self::METHOD_FILE_READ)->willReturn($manifest);

        $manager = $this->mockAssetManager();
        $manager->expects($getAssetUrlExpectation)
            ->method('getAssetUrl')
            ->with($instance, self::SRC)
            ->willReturn(self::URL);
        $file->expects($writeExpectation)
            ->method('write')
            ->with($expectedManifest);

        $this->internalCall($instance, 'updateManifest')($manager, $file);
    }

    /**
     * @return array<string,array{int,tManifest,2?:tManifest}>
     */
    public function provideUpdateManifest(): array
    {
        $icon = ['src' => self::SRC];
        $publicIcon = ['src' => self::URL];
        $key = self::KEY_ICONS;

        return [
            'no icons' => [0, []],
            'null icons' => [0, [$key => null]],
            'empty icons' => [0, [$key => []]],
            '1 icon' => [1, [$key => [$icon]], [$key => [$publicIcon]]],
            '3 icons' => [3, [$key => [$icon, $icon, $icon]], [
                $key => [$publicIcon, $publicIcon, $publicIcon],
            ]],
        ];
    }

    protected function instanceClass(): string
    {
        return ManifestAsset::class;
    }

    /**
     * @param string[] $mockedMethods
     *
     * @return ManifestAsset&MockObject
     */
    protected function mockInstance(array $mockedMethods = []): MockObject
    {
        array_unshift($mockedMethods, 'init');

        return $this->buildInstanceMock($mockedMethods)->getMock();
    }

    /**
     * @param string[] $mockedMethods
     *
     * @return ManifestFile&MockObject
     */
    protected function mockFile(
        string $path,
        array $mockedMethods = [],
    ): MockObject {
        return $this->getMockBuilder(ManifestFile::class)
            ->setConstructorArgs([$path])
            ->onlyMethods($mockedMethods)
            ->getMock();
    }

    /**
     * @return View&MockObject
     */
    private function mockView(): MockObject
    {
        return $this->createMock(View::class);
    }

    /**
     * @return AssetManager&MockObject
     */
    private function mockAssetManager(): MockObject
    {
        return $this->createMock(AssetManager::class);
    }
}

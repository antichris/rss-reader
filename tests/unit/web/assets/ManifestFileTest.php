<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\web\assets;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\tests\_support\vfsHelper;
use antichris\rssReader\web\assets\ManifestFile;
use Exception;
use JsonException;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamContainer;
use org\bovigo\vfs\vfsStreamContent;
use org\bovigo\vfs\vfsStreamDirectory;
use RuntimeException;
use stdClass;

/**
 * @internal
 * @coversDefaultClass \antichris\rssReader\web\assets\ManifestFile
 *
 * @phpstan-import-type tManifest from ManifestFile
 *
 * @extends TestCase<ManifestFile>
 */
final class ManifestFileTest extends TestCase
{
    private const METHOD_REFERENCE_FILE = 'referenceFile';
    private const METHOD_CHECK_PERMS = 'checkPerms';

    private string $path;

    protected function setUp(): void
    {
        $this->path = sys_get_temp_dir() . DIRECTORY_SEPARATOR
            . self::base64microtime() . '.manifest';
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
        $this->assertEquals(
            $this->path,
            $this->internalGetter($instance, 'path')(),
        );
    }

    /**
     * @dataProvider provideRead
     * @covers ::read
     *
     * @param tManifest $expected
     */
    public function testRead(
        string $path,
        ?Exception $exception = null,
        ?array $expected = null,
    ): void {
        $this->path = $path;
        $instance = $this->createInstance();

        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $actual = $this->internalCall($instance, 'read')();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string,1?:?Exception,2?:?tManifest}>
     */
    public function provideRead(): array
    {
        $hello = ['hello' => 'world'];

        $vfs = vfsHelper::create([
            'empty' => '',
            'borkt' => '}',
            'empty.json' => '{}',
            'hello.json' => json_encode($hello),
        ]);
        $unreadable = self::vfsAddInaccessible($vfs);

        $vfsUrl = fn ($name) => $vfs->getChild($name)->url();

        $readException = new RuntimeException('Could not read manifest file');
        $syntaxException = new JsonException('Syntax error', JSON_ERROR_SYNTAX);

        return [
            'absent file' => [vfsStream::url('absent'), $readException],
            'unreadable file' => [$unreadable, $readException],
            'empty file' => [$vfsUrl('empty'), $syntaxException],
            'invalid file' => [$vfsUrl('borkt'), $syntaxException],

            'empty manifest' => [$vfsUrl('empty.json'), null, []],
            'hello world' => [$vfsUrl('hello.json'), null, $hello],
        ];
    }

    /**
     * @dataProvider provideIsUpToDate
     * @covers ::isUpToDate
     */
    public function testIsUpToDate(
        bool $expected,
        string $path,
        string $referenceFile = '',
    ): void {
        $this->path = $path;
        $instance = $this->mockInstance([self::METHOD_REFERENCE_FILE]);

        $instance->method(self::METHOD_REFERENCE_FILE)
            ->willReturn($referenceFile);

        $actual = $this->internalCall($instance, 'isUpToDate')();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,string,2?:string}>
     */
    public function provideIsUpToDate(): array
    {
        $vfs = vfsHelper::create([
            'present' => '',
            'older' => '',
            'newer' => '',
        ]);

        $vfsUrl = fn ($name) => $vfs->getChild($name)->url();

        $presentTime = $vfs->getChild('present')->filemtime();
        $vfs->getChild('older')->lastModified($presentTime - 1);
        $vfs->getChild('newer')->lastModified($presentTime + 1);

        $absent = vfsStream::url('absent');
        $present = $vfsUrl('present');
        $older = $vfsUrl('older');
        $newer = $vfsUrl('newer');

        return [
            'target absent' => [false, $absent],
            'ref absent' => [false, $present, $absent],
            'ref older' => [false, $present, $older],
            'ref same age' => [true, $present, $present],
            'ref newer' => [true, $present, $newer],
        ];
    }

    /**
     * @dataProvider provideWrite
     * @covers ::write
     *
     * @param tManifest $manifest
     */
    public function testWrite(
        string $expected,
        string $path,
        array $manifest,
        ?Exception $exception = null,
    ): void {
        $this->path = $path;

        $instance = $this->mockInstance([self::METHOD_REFERENCE_FILE]);
        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $this->internalCall($instance, 'write')($manifest);

        $this->assertEquals($expected, file_get_contents($path));
    }

    /**
     * @return array<string,array{string,string,tManifest,3?:Exception}>
     */
    public function provideWrite(): array
    {
        $vfs = vfsHelper::create(['normal' => '']);
        $normal = $vfs->getChild('normal')->url();
        $unwriteable = self::vfsAddInaccessible($vfs);

        $cycle = new stdClass();
        $cycle->cycle = $cycle;

        $exception = new RuntimeException('Could not write manifest');

        return [
            'bad manifest' => ['', $normal, ['x' => $cycle], new JsonException(
                'Recursion detected',
                JSON_ERROR_RECURSION,
            )],
            'unwriteable' => ['', $unwriteable, [], $exception],
            'success' => ['[]', $normal, []],
        ];
    }

    /**
     * @dataProvider provideMarkUpToDate
     * @covers ::markUpToDate
     */
    public function testMarkUpToDate(
        string $referenceFile,
        ?Exception $exception = null,
    ): void {
        $oldtime = @filemtime($referenceFile);
        clearstatcache();

        $instance = $this->mockInstance([
            self::METHOD_CHECK_PERMS,
            self::METHOD_REFERENCE_FILE,
        ]);
        $instance->method(self::METHOD_REFERENCE_FILE)
            ->willReturn($referenceFile);

        $instance->expects($this->once())
            ->method(self::METHOD_CHECK_PERMS);
        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $this->internalCall($instance, 'markUpToDate')();

        $this->assertGreaterThan($oldtime, filemtime($referenceFile));
    }

    /**
     * @return array<string,array{string,1?:Exception}>
     */
    public function provideMarkUpToDate(): array
    {
        $vfs = vfsHelper::create(['normal' => '']);
        $normal = $vfs->getChild('normal');
        $normal->lastModified($normal->filemtime() - 1);
        $absent = "{$vfs->url()}/absent";

        return [
            'untouchable' => [
                // Was checkPerms not stubbed, this wouldn't work as intended.
                "{$absent}-dir/file",
                new RuntimeException('Could not touch'),
            ],
            'absent' => [$absent],
            'normal' => [$vfs->getChild('normal')->url()],
        ];
    }

    /**
     * @dataProvider provideCheckPerms
     * @covers ::checkPerms
     */
    public function testCheckPerms(
        string $path,
        ?Exception $exception = null,
        bool $permsChecked = false,
    ): void {
        $this->path = $path;
        $instance = $this->createInstance();
        $this->internalSetter($instance, 'permsChecked')($permsChecked);

        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $this->internalCall($instance, self::METHOD_CHECK_PERMS)();

        // If we even get this far
        $this->assertTrue($this->internalGetter($instance, 'permsChecked')());
    }

    /**
     * @return array<string,array{string,1?:?Exception,2?:bool}>
     */
    public function provideCheckPerms(): array
    {
        $vfs = vfsHelper::create([]);
        $normal = vfsHelper::create(['file' => ''], $vfs, 'normal');
        $unwriteable = vfsHelper::create(['file' => ''], $vfs, 'unwriteable');
        $unwriteable->chmod(0);

        $getUrl = fn (vfsStreamDirectory $vfs) => $vfs->getChild('file')->url();

        return [
            'already checked' => ['', null, true],
            'absent' => [
                "{$vfs->url()}/absent/file",
                new RuntimeException('Directory does not exist'),
            ],
            'unwriteable' => [
                $getUrl($unwriteable),
                new RuntimeException('Directory not writeable'),
            ],
            'normal' => [$getUrl($normal)],
        ];
    }

    /**
     * @covers ::referenceFile
     */
    public function testReferenceFile(): void
    {
        $this->path = 'dir/file';
        $expected = 'dir/~file';

        $instance = $this->createInstance();

        $actual = $this->internalCall($instance, self::METHOD_REFERENCE_FILE)();

        $this->assertEquals($expected, $actual);
    }

    protected function instanceClass(): string
    {
        return ManifestFile::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->path];
    }

    private static function base64microtime(): string
    {
        return sodium_bin2base64(
            strrev(pack('d', microtime(true))),
            SODIUM_BASE64_VARIANT_URLSAFE_NO_PADDING,
        );
    }

    /**
     * @param vfsStreamContainer<vfsStreamContent> $vfsDir
     */
    private static function vfsAddInaccessible(
        vfsStreamContainer $vfsDir,
    ): string {
        return vfsHelper::addNewFile($vfsDir, 'inaccessible', 0)->url();
    }
}

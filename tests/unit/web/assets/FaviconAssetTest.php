<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\web\assets;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\web\assets\FaviconAsset;
use PHPUnit\Framework\MockObject\MockObject;
use yii\web\AssetManager;
use yii\web\View;

/**
 * @internal
 * @coversDefaultClass \antichris\rssReader\web\assets\FaviconAsset
 *
 * @phpstan-import-type tIcon from FaviconAsset
 * @phpstan-import-type tSize from FaviconAsset
 * @phpstan-import-type tSizes from FaviconAsset
 *
 * @extends TestCase<FaviconAsset>
 */
final class FaviconAssetTest extends TestCase
{
    private const ANY = FaviconAsset::ANY;
    private const UNSPECIFIED = '<unspecified>';

    /**
     * @dataProvider provideRegisterAssetFiles
     * @covers ::registerAssetFiles
     *
     * @param null|tIcon[] $icons
     */
    public function testRegisterAssetFiles(?array $icons = null): void
    {
        $view = $this->mockView();
        $instance = $this->mockInstance(['registerIcon']);

        if ($icons !== null) {
            $instance->icons = $icons;
        }

        $callArgs = [];
        foreach ($instance->icons as $icon) {
            $callArgs[] = [$view, $icon];
        }
        $instance->expects($this->exactly(count($instance->icons)))
            ->method('registerIcon')
            ->withConsecutive(...$callArgs);

        $instance->registerAssetFiles($view);
    }

    /**
     * @return array<string,array{0?:tIcon[]}>
     */
    public function provideRegisterAssetFiles(): array
    {
        return [
            'no icons' => [[]],
            'default' => [],
            'custom' => [[
                ['file' => '<file1>', 'sizes' => 13, 'type' => '<type1>'],
                ['file' => '<file2>', 'sizes' => self::ANY, 'type' => '<type2>'],
            ]],
        ];
    }

    /**
     * @covers ::registerIcon
     */
    public function testRegisterIcon(): void
    {
        $link = ['<link>'];
        $icon = ['<icon>'];

        $view = $this->mockView();
        $manager = $this->mockAssetManager();
        $instance = $this->mockInstance(['prepLink']);

        $view->method('getAssetManager')->willReturn($manager);
        $instance->method('prepLink')->willReturn($link);

        $view->expects($this->once())
            ->method('registerLinkTag')
            ->with($link);

        $this->internalCall($instance, 'registerIcon')($view, $icon);
    }

    /**
     * @dataProvider providePrepLinkType
     * @xcovers ::prepLink
     */
    public function testPrepLinkType(
        bool $expectType,
        ?string $type = self::UNSPECIFIED,
    ): void {
        $this->testPrepLink($expectType, type: $type);
    }

    /**
     * @return array<string,array{bool,1?:?string}>
     */
    public function providePrepLinkType(): array
    {
        return [
            'no type' => [false],
            'null type' => [false, null],
            'empty type string' => [false, ''],
            '"0" type string' => [false, '0'],
            'type' => [true, '<type>'],
        ];
    }

    /**
     * @dataProvider providePrepLinkRel
     * @xcovers ::prepLink
     */
    public function testPrepLinkRel(
        string $expectedRel,
        ?string $rel = self::UNSPECIFIED,
    ): void {
        $this->testPrepLink(false, $expectedRel, rel: $rel);
    }

    /**
     * @return array<string,array{string,1?:?string}>
     */
    public function providePrepLinkRel(): array
    {
        $relDefault = 'icon';
        $rel = '<rel>';

        return [
            'no rel' => [$relDefault],
            'null rel' => [$relDefault, null],
            'empty rel string' => [$relDefault, ''],
            '"0" rel string' => [$relDefault, '0'],
            'rel' => [$rel, $rel],
        ];
    }

    /**
     * @dataProvider provideSizesString
     * @covers ::sizesString
     * @covers ::sizeString
     *
     * @param tSizes $sizes
     */
    public function testSizesString(
        string $expected,
        array|int|string $sizes,
    ): void {
        $instance = $this->mockInstance();

        $actual = $this->internalCall($instance, 'sizesString')($sizes);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string,tSizes}>
     */
    public function provideSizesString(): array
    {
        return [
            '"any"' => [self::ANY, self::ANY],
            'int' => ['13x13', 13],
            'ints' => ['12x12 14x14', [12, 14]],
            'int array' => ['12x12 13x14', [12, [13, 14]]],
        ];
    }

    /**
     * @covers ::prepLink
     */
    protected function testPrepLink(
        bool $expectType,
        string $expectedRel = 'icon',
        ?string $type = self::UNSPECIFIED,
        ?string $rel = self::UNSPECIFIED,
    ): void {
        $assetUrl = '<asset-url>';
        $sizesString = '<sizes-string>';
        $icon = [
            'file' => '<file>',
            'sizes' => '<sizes>',
        ];
        if ($rel !== self::UNSPECIFIED) {
            $icon['rel'] = $rel;
        }
        if ($type !== self::UNSPECIFIED) {
            $icon['type'] = $type;
        }

        $expected = [
            'href' => $assetUrl,
            'rel' => $expectedRel,
            'sizes' => $sizesString,
        ];
        if ($expectType) {
            $expected['type'] = $type;
        }

        $manager = $this->mockAssetManager();
        $instance = $this->mockInstance(['sizesString']);
        $manager->method('getAssetUrl')->willReturn($assetUrl);
        $instance->method('sizesString')->willReturn($sizesString);

        $actual = $this->internalCall($instance, 'prepLink')($manager, $icon);

        $this->assertEquals($expected, $actual);
    }

    protected function instanceClass(): string
    {
        return FaviconAsset::class;
    }

    /**
     * @param string[] $mockedMethods
     *
     * @return FaviconAsset&MockObject
     */
    protected function mockInstance(array $mockedMethods = []): MockObject
    {
        array_unshift($mockedMethods, 'init');

        return $this->buildInstanceMock($mockedMethods)->getMock();
    }

    /**
     * @return View&MockObject
     */
    protected function mockView(): MockObject
    {
        return $this->createMock(View::class);
    }

    /**
     * @return AssetManager&MockObject
     */
    protected function mockAssetManager(): MockObject
    {
        return $this->createMock(AssetManager::class);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit;

use Closure;
use PHPUnit\Framework\TestCase;
use function antichris\rssReader\base64md5;
use function antichris\rssReader\callerFrame;
use function antichris\rssReader\callerName;
use function antichris\rssReader\getlenv;
use function antichris\rssReader\internalCall;

/**
 * @internal
 * @coversNothing
 *
 * @phpstan-type tEnvMethod int&self::ENV_METHOD_*
 * @phpstan-type tEnvVal null|string|int|bool
 */
final class FunctionsTest extends TestCase
{
    /**
     * @dataProvider provideBase64md5
     * @covers \antichris\rssReader\base64md5
     */
    public function testBase64md5(string $expected, string $string): void
    {
        $this->assertEquals($expected, base64md5($string));
    }

    /**
     * @return array{string,string}[]
     */
    public function provideBase64md5(): array
    {
        return [
            ['1B2M2Y8AsgTpgAmY7PhCfg', ''],
            ['XrY7u-Ae7tCTyyK7j1rNww', 'hello world'],
        ];
    }

    /**
     * @dataProvider provideCallerName
     * @covers \antichris\rssReader\callerName
     */
    public function testCallerName(
        ?string $expected,
        int $offset,
        ?bool $useType = null,
    ): void {
        $args = $useType === null
            ? [$offset]
            : [$offset, $useType];
        /** @var callable($args):string
         * Add a stack frame to make this test the caller
         */
        $wrapperFn = fn ($args) => callerName(...$args);

        if ($expected === null) {
            $expected = __METHOD__;
        }

        $this->assertEquals($expected, $wrapperFn($args));
    }

    /**
     * @return array{?string,int,2?:bool}[]
     */
    public function provideCallerName(): array
    {
        return [
            // Special case `null` `$expected` becomes `__METHOD__` in the test.
            [null, 0],
            [null, 0, false],
            [__CLASS__ . '->testCallerName', 0, true],
            [parent::class . '::runTest', 1],
        ];
    }

    /**
     * @dataProvider provideCallerFrame
     * @covers \antichris\rssReader\callerFrame
     */
    public function testCallerFrame(int $offset, ?int $options = null): void
    {
        /** @var callable(array{int,1?:int}):array<string,int|string>
         * Add a stack frame to make this test the caller
         */
        $wrapperFn = fn (array $args) => callerFrame(...$args);

        $args = $options === null
            ? [$offset]
            : [$offset, $options];

        $backtraceOptions = $options === null
            ? DEBUG_BACKTRACE_PROVIDE_OBJECT
            : $options;
        $expected = debug_backtrace($backtraceOptions, 1 + $offset)[$offset];

        $this->assertEquals($expected, $wrapperFn($args));
    }

    /**
     * @return array{int,1?:int}[]
     */
    public function provideCallerFrame(): array
    {
        return [
            [0, DEBUG_BACKTRACE_PROVIDE_OBJECT],
            [0, DEBUG_BACKTRACE_IGNORE_ARGS],
            [0, DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS],
            [0],
            [1],
            [2],
        ];
    }

    /**
     * @dataProvider provideGetlenv
     * @covers \antichris\rssReader\getlenv
     *
     * @param tEnvMethod            $envMethod
     * @param array<string,tEnvVal> $env
     */
    public function testGetlenv(
        null|string|int|bool $expected,
        int $envMethod,
        string $varname,
        array $env = [],
        null|string|int|bool $defaultValue = null,
    ): void {
        $this->bulkPutenv($env, $envMethod);

        $actual = getlenv($varname, $defaultValue);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{tEnvVal,tEnvMethod,string,3?:array<string,tEnvVal>,4?:tEnvVal}>
     */
    public function provideGetlenv(): array
    {
        $var = 'MYVAR';
        $value = 'value';
        $numVal = 13;

        $baseCases = [
            'unset' => [null, $var, [$var => null]],
            'default string' => [$value, $var, [$var => null], $value],
            'default numeric' => [$numVal, $var, [$var => null], $numVal],
            'default bool' => [false, $var, [$var => null], false],
            'empty string' => ['', $var, [$var => '']],
            'string' => [$value, $var, [$var => $value]],
            'numeric' => ['13', $var, [$var => $numVal]],
        ];

        $cases = [];
        foreach ([
            'SERVER superglobal' => self::ENV_METHOD_SERVER,
            'ENV superglobal' => self::ENV_METHOD_ENV,
            '(do)env function' => self::ENV_METHOD_DOENVFN,
        ] as $methodName => $method) {
            foreach ($baseCases as $caseName => $case) {
                $expected = array_shift($case);
                array_unshift($case, $expected, $method);
                $cases["{$methodName} {$caseName}"] = $case;
            }
        }

        return $cases;
    }

    /**
     * @dataProvider provideInternalCall
     * @covers \antichris\rssReader\internalCall
     *
     * @param mixed[] $arguments
     */
    public function testInternalCall(
        mixed $expected,
        object $instance,
        string $method,
        array $arguments = [],
    ): void {
        $actual = internalCall($instance, $method)(...$arguments);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{mixed,object,string,mixed[]}>
     */
    public function provideInternalCall(): array
    {
        $instance = new FunctionsTestInternalCall(
            fn (int $a, int $b): int => $a + $b,
        );

        return [
            'protected' => [5, $instance,  'protectedFn', [2, 3]],
            'private' => [21, $instance,  'privateFn', [8, 13]],
        ];
    }

    private const ENV_METHOD_DOENVFN = 0;
    private const ENV_METHOD_SERVER = 1;
    private const ENV_METHOD_ENV = 2;

    /**
     * @param array<string,tEnvVal> $env
     * @param tEnvMethod            $method
     */
    private function bulkPutenv(
        array $env,
        int $method = self::ENV_METHOD_DOENVFN,
    ): void {
        /** @var callable(string) $clear */
        $clear = function (string $name) {
            unset($_SERVER[$name], $_ENV[$name]);
            putenv($name);
        };
        /** @var callable(string,tEnvVal) $assign */
        $assign = match ($method) {
            self::ENV_METHOD_SERVER => function (string $name, mixed $value) {
                $_SERVER[$name] = $value;
            },
            self::ENV_METHOD_ENV => function (string $name, mixed $value) {
                $_ENV[$name] = $value;
            },
            default => function (string $name, mixed $value) {
                putenv("{$name}={$value}");
            },
        };

        foreach ($env as $name => $value) {
            $clear($name);
            if ($value !== null) {
                $assign($name, $value);
            }
        }
    }
}

/**
 * @internal
 */
class FunctionsTestInternalCall
{
    public function __construct(
        private Closure $callback,
    ) {
    }

    protected function protectedFn(mixed ...$arguments): mixed
    {
        return $this->privateFn(...$arguments);
    }

    private function privateFn(mixed ...$arguments): mixed
    {
        return ($this->callback)(...$arguments);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\registration;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\registration\RegistrationMailer;
use antichris\rssReader\user\UserEntity;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use yii\mail\MailerInterface;
use yii\mail\MessageInterface;
use yii\web\UrlManager;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\registration\RegistrationMailer
 *
 * @method RegistrationMailer&MockObject mockInstance(string[] $mockedMethods)
 *
 * @extends TestCase<RegistrationMailer>
 */
final class RegistrationMailerTest extends TestCase
{
    /** @var MailerInterface&MockObject */
    private $mailer;
    /** @var UrlManager&MockObject */
    private $urlManager;
    /** @var string */
    private $appName;
    /** @var array<string,string>|string */
    private $sender;

    /** @var RegistrationMailer */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->mailer = $this->createMock(MailerInterface::class);
        $this->urlManager = $this->createMock(UrlManager::class);
        $this->appName = 'appName';
        $this->sender = 'sender@app';

        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideSendEmail
     * @covers ::sendEmail
     */
    public function testSendEmail(bool $expected, UserEntity $user, MessageInterface $message): void
    {
        $instance = $this->mockInstance(['composeEmail']);
        $instance->expects($this->once())
            ->method('composeEmail')
            ->with($user)
            ->willReturn($message);
        $this->mockMailerSend($message, $expected);

        $acutal = $instance->sendEmail($user);

        $this->assertEquals($expected, $acutal);
    }

    /**
     * @return array<string,array{bool,UserEntity,MessageInterface}>
     */
    public function provideSendEmail(): array
    {
        $user = $this->mockUserEntity();
        $message = $this->mockMessage();

        return [
            'failure' => [false, $user, $message],
            'success' => [true, $user, $message],
        ];
    }

    /**
     * @covers ::composeEmail
     */
    public function testComposeEmail(): void
    {
        $user = $this->mockUserEntity();
        $message = $this->mockMessage();
        $expected = true;

        $instance = $this->instance;
        $this->mailer->expects($this->once())
            ->method('compose')
            ->willReturn($message);
        $message->expects($this->once())->method('setFrom')->willReturnSelf();
        $message->expects($this->once())->method('setTo')->willReturnSelf();
        $message->expects($this->once())->method('setSubject')->willReturnSelf();
        $this->mockMailerSend($message, true);

        $acutal = $instance->sendEmail($user);

        $this->assertEquals($expected, $acutal);
    }

    /**
     * @return class-string<RegistrationMailer>
     */
    protected function instanceClass(): string
    {
        return RegistrationMailer::class;
    }

    /**
     * @return array{MailerInterface,UrlManager,string,string|array<string,string>}
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->mailer, $this->urlManager, $this->appName, $this->sender];
    }

    /**
     * @param mixed $message
     * @param mixed $result
     *
     * @return InvocationMocker<MailerInterface>
     */
    private function mockMailerSend($message, $result): InvocationMocker
    {
        return $this->mailer->expects($this->once())
            ->method('send')
            ->with($message)
            ->willReturn($result);
    }

    /**
     * @return MockObject&MessageInterface
     */
    private function mockMessage()
    {
        return $this->createMock(MessageInterface::class);
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

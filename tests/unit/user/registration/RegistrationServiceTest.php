<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\registration;

use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\user\registration\RegistrationFormInterface;
use antichris\rssReader\user\registration\RegistrationFormMapper;
use antichris\rssReader\user\registration\RegistrationMailer;
use antichris\rssReader\user\registration\RegistrationService;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserService;
use antichris\rssReader\user\UserStatusService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Throwable;
use yii\base\InvalidArgumentException;
use yii\base\Security;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\registration\RegistrationService
 * @phpstan-type tDescribeThrowable array{class-string<Throwable>,string}
 */
final class RegistrationServiceTest extends TestCase
{
    use MockHelperTrait;
    /**
     * @var RegistrationService
     */
    private $instance;
    /**
     * @var UserService&MockObject
     */
    private $userService;
    /**
     * @var RegistrationFormMapper&MockObject
     */
    private $formMapper;
    /**
     * @var UserStatusService&MockObject
     */
    private $statusService;
    /**
     * @var RegistrationMailer&MockObject
     */
    private $mailer;
    /**
     * @var Security&MockObject
     */
    private $security;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->userService = $this->createMock(UserService::class);
        $this->formMapper = $this->createMock(RegistrationFormMapper::class);
        $this->statusService = $this->createMock(UserStatusService::class);
        $this->mailer = $this->createMock(RegistrationMailer::class);
        $this->security = $this->createMock(Security::class);

        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(RegistrationService::class, $instance);
    }

    /**
     * @dataProvider provideRegister
     * @covers ::register
     */
    public function testRegister(bool $expected, bool $valid, bool $mailed, bool $saved): void
    {
        /** @var RegistrationService&MockObject $instance */
        $instance = $this->mockInstance(['generateEmailToken', 'sendEmail']);
        $form = $this->mockRegistrationFormWithValidate($valid);
        $user = $this->mockUserEntity();

        if ($valid) {
            $expectMapperMap = $this->once();
            $expectSendMail = $this->once();
        } else {
            $expectMapperMap = $this->never();
            $expectSendMail = $this->never();
        }
        $expectSave = $mailed
            ? $this->once()
            : $this->never();

        $this->formMapper->expects($expectMapperMap)
            ->method('map')
            ->with($form)
            ->willReturn($user);
        $instance->expects($expectSendMail)
            ->method('sendEmail')
            ->with($user)
            ->willReturn($mailed);
        $this->userService->expects($expectSave)
            ->method('save')
            ->with($user)
            ->willReturn($saved);

        $actual = $instance->register($form);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,bool,bool,bool}>
     */
    public function provideRegister(): array
    {
        return [
            'failing validation' => [false, false, false, false],
            'failing mail' => [false, true, false, false],
            'failing save' => [false, true, true, false],
            'success' => [true, true, true, true],
        ];
    }

    /**
     * @covers ::mapToUser
     */
    public function testMapToUser(): void
    {
        $form = $this->mockRegistrationForm();
        $user = $this->mockUserEntity();
        $expected = $user;

        $instance = $this->instance;
        $this->mockMapperMap($form, $user);

        $actual = $instance->mapToUser($form);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideGetUserByToken
     * @covers ::getUserByToken
     *
     * @param null|tDescribeThrowable $exception
     */
    public function testGetUserByToken(?string $token, bool $userExists = true, ?array $exception = null): void
    {
        /** @var RegistrationService $instance */
        $instance = $this->instance;

        $expected = null;
        if ($userExists) {
            $user = $this->mockUserEntity();
            $this->helpMock()->methodOnce($this->userService, 'findByEmailToken', $user);
            $expected = $user;
        }

        if ($exception) {
            [$type, $message] = $exception;
            $this->expectException($type);
            $this->expectExceptionMessage($message);
        }

        $actual = $instance->getUserByToken($token);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<array{?string,1?:bool,2?:tDescribeThrowable}>
     */
    public function provideGetUserByToken(): array
    {
        return [
            ['<token>'],
            ['<token>', false, [InvalidArgumentException::class, 'Invalid email confirmation token']],
            ['', false, [InvalidArgumentException::class, 'Email confirmation token cannot be blank']],
            [null, false, [InvalidArgumentException::class, 'Email confirmation token cannot be blank']],
        ];
    }

    /**
     * @covers ::activate
     */
    public function testActivate(): void
    {
        $status = 13;
        $user = $this->mockUserEntity();
        $expected = $user;

        $this->mockServiceSave($user, true);
        $this->statusService = $this->mockStatusService(['getActive']);
        $this->helpMock()->methodOnce($this->statusService, 'getActive', $status);
        $instance = $this->createInstance();

        $actual = $instance->activate($user);

        $this->assertEquals($expected, $actual);
        $this->assertNotNull($actual);
        $this->assertEquals($status, $actual->status);
    }

    /**
     * @covers ::sendEmail
     */
    public function testSendEmail(): void
    {
        $form = $this->mockRegistrationFormWithValidate(true);
        $user = $this->mockUserEntity();
        $expected = true;

        /** @var RegistrationService&MockObject $instance */
        $instance = $this->mockInstance(['generateEmailToken']);
        $this->mockMapperMap($form, $user);
        $this->mockServiceSave($user, true);
        $this->mailer->expects($this->once())->method('sendEmail')->with($user)->willReturn($expected);

        $actual = $instance->register($form);

        $this->assertTrue($actual);
    }

    private function createInstance(): RegistrationService
    {
        return new RegistrationService(...$this->instanceCtorArgs());
    }

    /**
     * @return array{UserService,RegistrationFormMapper,UserStatusService,RegistrationMailer,Security}
     */
    private function instanceCtorArgs(): array
    {
        return [
            $this->userService,
            $this->formMapper,
            $this->statusService,
            $this->mailer,
            $this->security,
        ];
    }

    /**
     * @param string[] $methods
     */
    private function mockInstance($methods): MockObject
    {
        return $this->helpMock()->build(get_class($this->instance), $this->instanceCtorArgs(), $methods)->getMock();
    }

    private function mockRegistrationFormWithValidate(bool $validateResult): RegistrationFormInterface
    {
        /** @var MockObject&RegistrationFormInterface $form */
        $form = $this->mockRegistrationForm();
        $this->helpMock()->methodOnce($form, 'validate', $validateResult);

        return $form;
    }

    private function mockMapperMap(RegistrationFormInterface $form, UserEntity $user): void
    {
        $this->helpMock()->methodOnce($this->formMapper, 'map', $user, $form);
    }

    /**
     * @return MockObject&RegistrationFormInterface
     */
    private function mockRegistrationForm()
    {
        return $this->createMock(RegistrationFormInterface::class);
    }

    private function mockServiceSave(UserEntity $user, bool $result): void
    {
        $this->helpMock()->methodOnce($this->userService, 'save', $result, $user);
    }

    /**
     * @param string[] $addMethods
     *
     * @return UserStatusService&MockObject
     */
    private function mockStatusService(array $addMethods = [])
    {
        return $this->getMockBuilder(UserStatusService::class)->addMethods($addMethods)->getMock();
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

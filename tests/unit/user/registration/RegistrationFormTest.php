<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\registration;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\registration\RegistrationForm;
use antichris\rssReader\user\repository\UserArProvider;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\registration\RegistrationForm
 *
 * @method RegistrationForm createInstance() Constructs the instance under test.
 *
 * @extends TestCase<RegistrationForm>
 */
final class RegistrationFormTest extends TestCase
{
    /**
     * @var UserArProvider&MockObject
     */
    private $userArProvider;
    /**
     * @var RegistrationForm
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->userArProvider = $this->createMock(UserArProvider::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::rules
     */
    public function testRules(): void
    {
        $modelClass = '<modelClass>';
        $expected = [
            'email',
            'unique',
            'targetClass' => $modelClass,
            'message' => 'This email address is not available.',
        ];

        $instance = $this->instance;
        $this->userArProvider->expects($this->once())->method('getModelClass')->willReturn($modelClass);

        $actual = $instance->rules();

        $this->assertContains($expected, $actual);
    }

    /**
     * @dataProvider provideFields
     * @covers ::getEmail
     * @covers ::getFirstName
     * @covers ::getLastName
     * @covers ::getPassword
     */
    public function testFieldGetters(string $field): void
    {
        $expected = "<{$field}>";
        $this->instance->{$field} = $expected;

        $actual = $this->instance->{"get{$field}"}();

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{string}>
     */
    public function provideFields(): array
    {
        $fields = ['firstName', 'lastName', 'email', 'password'];

        return array_combine($fields, array_map(null, $fields, []));
    }

    protected function instanceClass(): string
    {
        return RegistrationForm::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->userArProvider];
    }
}

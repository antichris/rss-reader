<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\registration;

use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\registration\RegistrationForm;
use antichris\rssReader\user\registration\RegistrationFormInterface;
use antichris\rssReader\user\registration\RegistrationFormMapper;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserEntityProvider;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\registration\RegistrationFormMapper
 *
 * @method RegistrationFormMapper&MockObject mockInstance(array $mockedMethods = [])
 *
 * @extends TestCase<RegistrationFormMapper>
 */
final class RegistrationFormMapperTest extends TestCase
{
    use MockHelperTrait;
    /**
     * @var UserEntityProvider&MockObject
     */
    private $userProvider;
    /**
     * @var PasswordService&MockObject
     */
    private $passwordService;
    /**
     * @var RegistrationFormMapper
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->userProvider = $this->createMock(UserEntityProvider::class);
        $this->passwordService = $this->createMock(PasswordService::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(RegistrationFormMapper::class, $instance);
    }

    /**
     * @covers ::map
     */
    public function testMap(): void
    {
        $form = $this->createMock(RegistrationFormInterface::class);
        $userEntity = $this->mockUserEntity();
        $expected = $this->mockUserEntity();

        $mockHelper = $this->helpMock();

        $instance = $this->mockInstance(['fill']);
        $mockHelper->methodOnce($this->userProvider, 'provide', $userEntity);
        $mockHelper->methodOnce($instance, 'fill', $expected, $form, $userEntity);

        $actual = $instance->map($form);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::fill
     */
    public function testFill(): void
    {
        $password = '<password>';

        $instance = $this->instance;
        /** @var RegistrationForm $form */
        $form = $this->createPartialMock(RegistrationForm::class, []);
        $form->firstName = '<firstName>';
        $form->lastName = '<lastName>';
        $form->email = '<email>';
        $userEntity = $this->mockUserEntity();

        $form->password = $password;
        $this->passwordService->expects($this->once())
            ->method('setPassword')
            ->with($userEntity, $password);

        $actual = $instance->fill($form, $userEntity);

        $this->assertEquals($userEntity, $actual);
        $this->assertEquals($form->firstName, $actual->first_name);
        $this->assertEquals($form->lastName, $actual->last_name);
        $this->assertEquals($form->email, $actual->email);
    }

    protected function instanceClass(): string
    {
        return RegistrationFormMapper::class;
    }

    /**
     * @return array{UserEntityProvider,PasswordService}
     */
    protected function instanceCtorArgs(): array
    {
        return [
            $this->userProvider,
            $this->passwordService,
        ];
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

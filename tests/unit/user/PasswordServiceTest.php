<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\UserEntity;
use PHPUnit\Framework\MockObject\MockObject;
use yii\base\Security;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\PasswordService
 *
 * @method PasswordService            createInstance()
 * @method PasswordService&MockObject mockInstance($mockedMethods=[])
 * @extends TestCase<PasswordService>
 */
class PasswordServiceTest extends TestCase
{
    /**
     * @var MockObject&Security
     */
    private $security;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->security = $this->createMock(Security::class);
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $expectedSecurity = $this->security;

        $instance = $this->createInstance();
        $actualSecurity = $this->internalGetter($instance, 'security')();

        $this->assertInstanceOf($this->instanceClass(), $instance);
        $this->assertSame($expectedSecurity, $actualSecurity);
    }

    /**
     * @covers ::generatePasswordHash
     */
    public function testGeneratePasswordHash(): void
    {
        $password = '<password>';
        $expected = '<hash>';

        $instance = $this->createInstance();
        $this->mockGeneratePasswordHash($this->security, $password, $expected);

        $actual = $instance->generatePasswordHash($password);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideValidatePassword
     * @covers ::validatePassword
     */
    public function testValidatePassword(bool $expected): void
    {
        $password = '<password>';
        $hash = '<hash>';

        $entity = $this->mockUserEntity();
        $entity->password_hash = $hash;
        $instance = $this->createInstance();
        $this->security->method('validatePassword')
            ->with($password, $hash)
            ->willReturn($expected);

        $actual = $instance->validatePassword($entity, $password);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool}>
     */
    public function provideValidatePassword(): array
    {
        return [
            'invalid' => [false],
            'valid' => [true],
        ];
    }

    /**
     * @covers ::setPassword
     */
    public function testSetPassword(): void
    {
        $password = '<password>';
        $expected = '<hash>';

        $entity = $this->mockUserEntity();
        $instance = $this->mockInstance(['generatePasswordHash']);
        $this->mockGeneratePasswordHash($instance, $password, $expected);

        $alteredUser = $instance->setPassword($entity, $password);
        $actual = $alteredUser->password_hash;

        $this->assertEquals($expected, $actual);
    }

    protected function instanceClass(): string
    {
        return PasswordService::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->security];
    }

    private function mockGeneratePasswordHash(
        MockObject $mock,
        string $password,
        string $hash,
    ): void {
        $mock->method('generatePasswordHash')
            ->with($password)
            ->willReturn($hash);
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

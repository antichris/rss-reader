<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\repository;

use antichris\rssReader\misc\activeRepository\ActiveRecordCache;
use antichris\rssReader\misc\activeRepository\ActiveRepository;
use antichris\rssReader\misc\activeRepository\ActiveRepositoryProvider;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\repository\UserArMapper;
use antichris\rssReader\user\repository\UserArProvider;
use antichris\rssReader\user\repository\UserArRepository;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserStatusService;
use PHPUnit\Framework\MockObject\MockObject;
use RangeException;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\repository\UserArRepository
 *
 * @method UserArRepository            createInstance()                           Constructs the instance under test.
 * @method UserArRepository&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<UserArRepository>
 */
final class UserArRepositoryTest extends TestCase
{
    use MockHelperTrait;

    private const METHOD_GET_REPO = 'getRepo';

    /**
     * @var ActiveRepositoryProvider<UserEntity,ActiveRecord>&MockObject
     */
    private $activeRepoProvider;
    /**
     * @var UserArProvider&MockObject
     */
    private $arProvider;
    /**
     * @var UserStatusService&MockObject
     */
    private $statusService;
    /**
     * @var UserArMapper&MockObject
     */
    private $mapper;
    /**
     * @var ActiveRecordCache<ActiveRecord>&MockObject
     */
    private $cache;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->activeRepoProvider = $this->createMock(ActiveRepositoryProvider::class);
        $this->arProvider = $this->createMock(UserArProvider::class);
        $this->statusService = $this->createMock(UserStatusService::class);
        $this->mapper = $this->createMock(UserArMapper::class);
        $this->cache = $this->createMock(ActiveRecordCache::class);
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(UserArRepository::class, $instance);
    }

    /**
     * @covers ::save
     */
    public function testSave(): void
    {
        $entity = $this->mockUserEntity();
        $expected = true;

        $repo = $this->mockActiveRepository();
        $instance = $this->mockInstanceGetRepo($repo);
        $this->helpMock()->methodOnce($repo, 'save', $expected, $entity);

        $actual = $instance->save($entity);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideFindOne
     * @covers ::findOne
     */
    public function testFindOne(?UserEntity $entity): void
    {
        $condition = '<condition>';
        $expected = $entity;

        $repo = $this->mockActiveRepository();
        $instance = $this->mockInstanceGetRepo($repo);
        $this->helpMock()->methodOnce($repo, 'findOne', $expected, $condition);

        $actual = $instance->findOne($condition);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{null|(MockObject&UserEntity)}>
     */
    public function provideFindOne(): array
    {
        return [
            'found' => [$this->mockUserEntity()],
            'not found' => [null],
        ];
    }

    /**
     * @covers ::findIdentity
     */
    public function testFindIdentity(): void
    {
        $id = 13;
        $status = 1;
        $entity = $this->mockUserEntity();
        $expected = $entity;

        $condition = compact('id', 'status');
        $this->statusService = $this->mockStatusServiceGetter('getActive', $status);
        /** @var UserArRepository $instance */
        $instance = $this->mockInstanceFindOne($entity, $condition);

        $actual = $instance->findIdentity($id);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::findByEmailToken
     */
    public function testFindByEmailToken(): void
    {
        $email_token = '<emailToken>';
        $status = 13;
        $entity = $this->mockUserEntity();
        $expected = $entity;

        $condition = compact('email_token', 'status');
        $this->statusService = $this->mockStatusServiceGetter('getInactive', $status);
        /** @var UserArRepository $instance */
        $instance = $this->mockInstanceFindOne($entity, $condition);

        $actual = $instance->findByEmailToken($email_token);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideTestFindByEmail
     * @covers ::findByEmail
     */
    public function testFindByEmail(
        ?int $status = null,
        bool $validStatus = true,
    ): void {
        $email = '<email>';
        $entity = $this->mockUserEntity();
        $expected = $entity;

        $condition = $status !== null
            ? compact('email', 'status')
            : compact('email');
        $this->statusService->method('isValidValue')->willReturn($validStatus);
        if ($validStatus) {
            /** @var UserArRepository $instance */
            $instance = $this->mockInstanceFindOne($entity, $condition);
        } else {
            $instance = $this->mockInstance();
            $this->expectException(RangeException::class);
            $this->expectExceptionMessage('Not a valid user status');
        }

        $actual = $instance->findByEmail($email, $status);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{0?:?int,1?:bool}>
     */
    public function provideTestFindByEmail(): array
    {
        return [
            'no status' => [],
            'null status' => [null],
            'active' => [UserStatusService::ACTIVE],
            'arbitrary status' => [13],
            'invalid status' => [21, false],
        ];
    }

    /**
     * @covers ::getModelClass
     */
    public function testGetModelClass(): void
    {
        $expected = '<modelClass>';

        $repo = $this->mockActiveRepository();
        $instance = $this->mockInstanceGetRepo($repo);
        $this->helpMock()->methodOnce($repo, 'getModelClass', $expected);

        $actual = $instance->getModelClass();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::getTableName
     */
    public function testGetTableName(): void
    {
        $expected = '<tableName>';

        $repo = $this->mockActiveRepository();
        $instance = $this->mockInstanceGetRepo($repo);
        $this->helpMock()->methodOnce($repo, 'getTableName', $expected);

        $actual = $instance->getTableName();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::getRepo
     */
    public function testGetRepo(): void
    {
        $expected = $this->mockActiveRepository();

        $this->activeRepoProvider = $this->mockActiveRepoProviderProvide($expected);
        $instance = $this->createInstance();
        $instance__getRepo = $this->internalCall($instance, self::METHOD_GET_REPO);

        $actual = $instance__getRepo();

        $this->assertSame($expected, $actual);
    }

    /**
     * @return MockObject[]
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->activeRepoProvider, $this->arProvider, $this->mapper, $this->cache, $this->statusService];
    }

    protected function instanceClass(): string
    {
        return UserArRepository::class;
    }

    /**
     * @param tActiveRCondition $condition
     *
     * @return UserArRepository&MockObject
     */
    private function mockInstanceFindOne(?UserEntity $entity, $condition): MockObject
    {
        $instance = $this->mockInstance(['findOne']);
        $this->helpMock()->methodOnce($instance, 'findOne', $entity, $condition);

        return $instance;
    }

    /**
     * @param ActiveRepository<UserEntity,ActiveRecord> $repo
     *
     * @return UserArRepository&MockObject
     */
    private function mockInstanceGetRepo(ActiveRepository $repo): MockObject
    {
        $instance = $this->mockInstance([self::METHOD_GET_REPO]);
        $instance->expects($this->once())
            ->method(self::METHOD_GET_REPO)
            ->willReturn($repo);

        return $instance;
    }

    /**
     * @return MockObject&ActiveRepository<UserEntity,ActiveRecord>
     */
    private function mockActiveRepository()
    {
        return $this->createMock(ActiveRepository::class);
    }

    /**
     * @param string[] $mockedMethods
     *
     * @return ActiveRepositoryProvider<UserEntity,ActiveRecord>&MockObject
     */
    private function mockActiveRepoProvider(array $mockedMethods = [])
    {
        return $this->createPartialMock(ActiveRepositoryProvider::class, $mockedMethods);
    }

    /**
     * @return ActiveRepositoryProvider<UserEntity,ActiveRecord>&MockObject
     */
    private function mockActiveRepoProviderProvide(MockObject $repo): MockObject
    {
        $provider = $this->mockActiveRepoProvider(['provide']);
        $this->helpMock()->methodOnce($provider, 'provide', $repo);

        return $provider;
    }

    /**
     * @param string[] $addMethods
     *
     * @return MockObject&UserStatusService
     */
    private function mockStatusService(array $addMethods = []): MockObject
    {
        return $this->getMockBuilder(UserStatusService::class)->addMethods($addMethods)->getMock();
    }

    /**
     * @param string $getterMethod magic status getter method (such as getActive or getInactive) name
     *
     * @return MockObject&UserStatusService
     */
    private function mockStatusServiceGetter(string $getterMethod, int $status): MockObject
    {
        $statusService = $this->mockStatusService([$getterMethod]);
        $this->helpMock()->methodOnce($statusService, $getterMethod, $status);

        return $statusService;
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

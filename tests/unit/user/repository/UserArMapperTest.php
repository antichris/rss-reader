<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\repository;

use antichris\rssReader\user\repository\UserArMapper;
use antichris\rssReader\user\repository\UserArProvider;
use antichris\rssReader\user\UserEntityProvider;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\repository\UserArMapper
 */
final class UserArMapperTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $entityProvider = $this->createMock(UserEntityProvider::class);
        $arProvider = $this->createMock(UserArProvider::class);

        $instance = new UserArMapper($entityProvider, $arProvider);

        $this->assertInstanceOf(UserArMapper::class, $instance);
    }
}

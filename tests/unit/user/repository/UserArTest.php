<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\repository;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\tests\_support\YiiContainerPreserverTrait;
use antichris\rssReader\user\repository\UserAr;
use antichris\rssReader\user\repository\UserArProvider;
use antichris\rssReader\user\UserStatusService;
use PHPUnit\Framework\MockObject\MockObject;
use Yii;
use yii\base\ModelEvent;
use yii\base\Security;
use yii\behaviors\TimestampBehavior;
use yii\di\Container;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\repository\UserAr
 *
 * @method UserAr createInstance() Constructs the instance under test.
 *
 * @extends TestCase<UserAr>
 */
final class UserArTest extends TestCase
{
    use YiiContainerPreserverTrait;

    public const CLASS_STATUS_SERVICE = UserStatusService::class;

    /**
     * @var MockObject&Security
     */
    private $security;
    /**
     * @var UserStatusService&MockObject
     */
    private $statusService;
    /**
     * @var UserAr
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->security = $this->createMock(Security::class);
        $this->statusService = $this->createMock(self::CLASS_STATUS_SERVICE);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::instantiate
     */
    public function testInstantiate(): void
    {
        $expected = $this->instance;

        /** @var MockObject&Container $container */
        $container = $this->createMock(Container::class);
        /** @var MockObject&UserArProvider $provider */
        $provider = $this->createMock(UserArProvider::class);
        $container->method('get')->willReturn($provider);
        $provider->method('provide')->willReturn($expected);
        Yii::$container = $container;

        $actual = $this->instanceClass()::instantiate();

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::tableName
     */
    public function testTableName(): void
    {
        $expected = '{{%user}}';

        $actual = $this->instance->tableName();

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::behaviors
     */
    public function testBehaviors(): void
    {
        $timestampBehavior = TimestampBehavior::class;

        $behaviors = $this->instance->behaviors();

        $this->assertContains($timestampBehavior, $behaviors);
    }

    /**
     * @covers ::rules
     */
    public function testRules(): void
    {
        $defaultStatus = 13;
        $allStatuses = [13, 333, 999];
        $statusDefaultRule = ['status', 'default', 'value' => $defaultStatus];
        $statusRangeRule = ['status', 'in', 'range' => $allStatuses];

        $this->statusService = $this->getMockBuilder(self::CLASS_STATUS_SERVICE)
            ->addMethods(['getDefault'])
            ->onlyMethods(['getAll'])
            ->getMock();
        $this->statusService->expects($this->once())
            ->method('getDefault')
            ->willReturn($defaultStatus);
        $this->statusService->expects($this->once())
            ->method('getAll')
            ->willReturn($allStatuses);
        $instance = $this->createInstance();

        $rules = $instance->rules();

        $this->assertContains($statusDefaultRule, $rules);
        $this->assertContains($statusRangeRule, $rules);
    }

    /**
     * @dataProvider provideBeforeSave
     * @covers ::beforeSave
     */
    public function testBeforeSave(bool $expected, bool $insert, bool $isNew): void
    {
        $randomString = '<random_string>';

        /** @var UserAr&MockObject $instance */
        $instance = $this->mockInstance(['trigger', 'getIsNewRecord', 'attributes', '__set']);

        $eventName = $insert
            ? $instance::EVENT_BEFORE_INSERT
            : $instance::EVENT_BEFORE_UPDATE;

        $instance->expects($this->once())
            ->method('trigger')
            ->with($eventName, $this->isInstanceOf(ModelEvent::class))
            ->willReturnCallback(function (string $name, ModelEvent $event) use ($eventName, $expected) {
                $this->assertSame($eventName, $name);
                $event->isValid = $expected;
            });

        if ($expected) {
            $instance->expects($this->once())
                ->method('getIsNewRecord')
                ->willReturn($isNew);
            $instance->expects($this->once())
                ->method('attributes')
                ->willReturn([['auth_key' => null]]);

            if ($isNew) {
                $this->security->expects($this->once())
                    ->method('generateRandomString')
                    ->willReturn($randomString);
                $instance->expects($this->once())
                    ->method('__set')
                    ->with('auth_key', $randomString);
            }
        }

        $actual = $instance->beforeSave($insert);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,bool,bool}>
     */
    public function provideBeforeSave(): array
    {
        return [
            'parent false' => [false, true, true],
            'not new' => [true, false, false],
            'new insert' => [true, true, true],
            // XXX The following should not happen in normal operation, but are handled.
            'new not insert' => [true, false, true],
            'not new insert' => [true, true, false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return UserAr::class;
    }

    /**
     * @return array{Security,UserStatusService}
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->security, $this->statusService];
    }
}

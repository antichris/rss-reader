<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user;

use antichris\rssReader\tests\_support\CaseNamerTrait;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserRepositoryInterface;
use antichris\rssReader\user\UserService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\UserService
 */
final class UserServiceTest extends TestCase
{
    use CaseNamerTrait;
    use MockHelperTrait;
    /**
     * @var UserRepositoryInterface&MockObject
     */
    private $repository;
    /**
     * @var UserService
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->repository = $this->createMock(UserRepositoryInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(UserService::class, $instance);
    }

    /**
     * @dataProvider provideCallRepository
     * @covers ::findByEmail
     * @covers ::findByEmailToken
     * @covers ::findIdentity
     * @covers ::save
     *
     * @param mixed   $expected
     * @param mixed[] $args
     */
    public function testCallRepository(string $method, $expected, array $args): void
    {
        $instance = $this->instance;
        $this->mockRepositoryMethod($method, $expected, ...$args);

        $actual = $instance->{$method}(...$args);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,mixed[]>
     */
    public function provideCallRepository(): array
    {
        $methods = [
            'findIdentity',
            'findByEmailToken',
            'findByEmail',
            'save',
        ];
        $args = [
            [13],
            ['<token>'],
            ['<email>', 21],
            [$this->mockUserEntity(), true, ['attributes']],
        ];
        $results = [
            $this->mockUserEntity(),
            $this->mockUserEntity(),
            $this->mockUserEntity(),
            true,
        ];

        $caseParams = array_map(null, $methods, $results, $args);
        $caseNames = $this->nameCases('%s()', $methods);
        $cases = array_combine($caseNames, $caseParams);
        if (!$cases) {
            throw new RuntimeException('Test case data generation failed');
        }

        return $cases;
    }

    /**
     * @covers ::findActiveByEmail
     */
    public function testFindActiveByEmail(): void
    {
        $email = '<email>';
        $active = true;
        $expected = $this->mockUserEntity();

        $instance = $this->instance;
        $this->mockRepositoryMethod('findByEmail', $expected, $email, $active);

        $actual = $instance->findActiveByEmail($email);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @param mixed[] $args
     * @param mixed   $result
     */
    private function mockRepositoryMethod(string $method, $result, ...$args): void
    {
        $this->helpMock()->methodOnce($this->repository, $method, $result, ...$args);
    }

    private function createInstance(): UserService
    {
        return new UserService($this->repository);
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

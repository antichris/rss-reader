<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user;

use antichris\rssReader\tests\_support\misc\ProviderAbstractTest;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserEntityProvider;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\UserEntityProvider
 *
 * @extends ProviderAbstractTest<UserEntityProvider>
 */
final class UserEntityProviderTest extends ProviderAbstractTest
{
    public function provideGetClassName(): array
    {
        return [[UserEntity::class]];
    }

    protected function instanceClass(): string
    {
        return UserEntityProvider::class;
    }
}

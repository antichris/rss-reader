<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user;

use antichris\rssReader\tests\_support\CaseNamerTrait;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\YiiContainerPreserverTrait;
use antichris\rssReader\user\Identity;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserService;
use PHPUnit\Framework\TestCase;
use Yii;
use yii\base\NotSupportedException;
use yii\di\Container;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\Identity
 */
final class IdentityTest extends TestCase
{
    use CaseNamerTrait;
    use MockHelperTrait;
    use YiiContainerPreserverTrait;
    /**
     * @var Identity
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = new Identity();
    }

    /**
     * @covers ::findIdentity
     */
    public function testFindIdentity(): void
    {
        $id = 13;
        $expected = $this->createMock(UserEntity::class);

        $className = UserService::class;
        $userService = $this->createMock($className);
        Yii::$container = $this->createMock(Container::class);
        $this->helpMock()->methodOnce($userService, 'findIdentity', $expected, $id);
        $this->helpMock()->methodOnce(Yii::$container, 'get', $userService, $className);

        $actual = Identity::findIdentity($id);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::getId
     */
    public function testGetId(): void
    {
        $instance = $this->instance;
        $expected = 13;
        $instance->id = 13;

        $actual = $instance->getId();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::findIdentityByAccessToken
     */
    public function testFindIdentityByAccessToken(): void
    {
        $instance = $this->instance;

        $this->expectException(NotSupportedException::class);
        $this->expectExceptionMessage('"findIdentityByAccessToken" is not implemented');

        $instance->findIdentityByAccessToken(null);
    }

    /**
     * @covers ::getAuthKey
     */
    public function testGetAuthKey(): void
    {
        $instance = $this->instance;
        $expected = random_bytes(32);
        $instance->auth_key = $expected;

        $actual = $instance->getAuthKey();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideValidateAuthKey
     * @covers ::validateAuthKey
     */
    public function testValidateAuthKey(bool $expected, string $instanceKey, string $authKey): void
    {
        $instance = $this->instance;
        $instance->auth_key = $instanceKey;

        $actual = $instance->validateAuthKey($authKey);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,string,string}>
     */
    public function provideValidateAuthKey(): array
    {
        return [
            'different' => [false, 'key', 'other'],
            'same' => [true, 'key', 'key'],
        ];
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\authentication;

use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\YiiContainerPreserverTrait;
use antichris\rssReader\user\authentication\AuthenticationService;
use antichris\rssReader\user\authentication\LoginFormInterface;
use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserService;
use antichris\rssReader\user\UserStatusService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use yii\web\IdentityInterface;
use yii\web\User;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\authentication\AuthenticationService
 */
final class AuthenticationServiceTest extends TestCase
{
    use MockHelperTrait;
    use YiiContainerPreserverTrait;
    /**
     * @var UserService&MockObject
     */
    private $userService;
    /**
     * @var UserStatusService&MockObject
     */
    private $statusService;
    /**
     * @var PasswordService&MockObject
     */
    private $passwordService;
    /**
     * @var User&MockObject
     */
    private $userComponent;
    /**
     * @var AuthenticationService
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->userService = $this->createMock(UserService::class);
        $this->statusService = $this->createMock(UserStatusService::class);
        $this->passwordService = $this->createMock(PasswordService::class);
        $this->userComponent = $this->createMock(User::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(AuthenticationService::class, $instance);
    }

    /**
     * @covers ::login
     */
    public function testLogin(): void
    {
        $instance = $this->instance;
        $userComponent = $this->userComponent;
        $identity = $this->createMock(IdentityInterface::class);
        /** @var MockObject&LoginFormInterface $form */
        $form = $this->createMock(LoginFormInterface::class);
        $this->helpMock()->methodOnce($form, 'validate', true);
        $this->helpMock()->methodOnce($form, 'getUser', $identity);
        $this->helpMock()->methodOnce($userComponent, 'login', true, $identity);

        $actual = $instance->login($form);

        $this->assertTrue($actual);
    }

    /**
     * @covers ::login
     */
    public function testLoginFailsWithInvalidForm(): void
    {
        /** @var AuthenticationService $instance */
        $instance = $this->instance;
        /** @var MockObject&LoginFormInterface $form */
        $form = $this->createMock(LoginFormInterface::class);
        $this->helpMock()->methodOnce($form, 'validate', false);

        $actual = $instance->login($form);

        $this->assertFalse($actual);
    }

    /**
     * @covers ::validatePassword
     */
    public function testValidatePassword(): void
    {
        $password = '<password>';

        $instance = $this->instance;
        $userEntity = $this->mockUserEntity();
        $this->passwordService->expects($this->once())
            ->method('validatePassword')
            ->with($userEntity, $password)
            ->willReturn(true);

        $actual = $instance->validatePassword($userEntity, $password);

        $this->assertTrue($actual);
    }

    /**
     * @dataProvider provideCanLogIn
     * @covers ::canLogIn
     */
    public function testCanLogIn(bool $expected, UserEntity $user, bool $isStatusLoggableIn, bool $isValidPassword): void
    {
        $status = 13;
        $password = '<password>';
        $user->status = $status;

        $mockHelper = $this->helpMock();
        /** @var AuthenticationService&MockObject $instance */
        $instance = $mockHelper->build(
            AuthenticationService::class,
            $this->instanceCtorArgs(),
            ['isStatusLoggableIn', 'validatePassword'],
        )->getMock();
        $mockHelper->methodOnce($instance, 'isStatusLoggableIn', $isStatusLoggableIn, $status);
        if ($isStatusLoggableIn) {
            $mockHelper->methodOnce($instance, 'validatePassword', $isValidPassword, $user, $password);
        }

        $actual = $instance->canLogIn($user, $password);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,MockObject&UserEntity,bool,bool}>
     */
    public function provideCanLogIn(): array
    {
        return [
            'valid status and password' => [true, $this->mockUserEntity(), true, true],
            'unauthenticable status (not)' => [false, $this->mockUserEntity(), false, true],
            'invalid password (not)' => [false, $this->mockUserEntity(), true, false],
        ];
    }

    /**
     * @covers ::getUser
     */
    public function testGetUser(): void
    {
        $email = '<email>';
        $expected = $this->mockUserEntity();

        $instance = $this->instance;
        $this->helpMock()->methodOnce($this->userService, 'findActiveByEmail', $expected, $email);

        $actual = $instance->getUser($email);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideIsStatusAuthenticable
     * @covers ::isStatusLoggableIn
     */
    public function testIsStatusAuthenticable(bool $expected): void
    {
        $status = 13;

        $this->statusService = $this->mockStatusService(['isActive']);
        $this->helpMock()->methodOnce($this->statusService, 'isActive', $expected, $status);
        $instance = $this->createInstance();

        $actual = $instance->isStatusLoggableIn($status);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool}>
     */
    public function provideIsStatusAuthenticable(): array
    {
        return [
            'active' => [true],
            'not active (not)' => [false],
        ];
    }

    /**
     * @param string[] $addMethods
     *
     * @return UserStatusService&MockObject
     */
    private function mockStatusService(array $addMethods = []): MockObject
    {
        return $this->getMockBuilder(UserStatusService::class)->addMethods($addMethods)->getMock();
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }

    private function createInstance(): AuthenticationService
    {
        return new AuthenticationService(...$this->instanceCtorArgs());
    }

    /**
     * @return array{UserService,UserStatusService,PasswordService,User}
     */
    private function instanceCtorArgs(): array
    {
        return [
            $this->userService,
            $this->statusService,
            $this->passwordService,
            $this->userComponent,
        ];
    }
}

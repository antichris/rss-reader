<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\authentication;

use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\authentication\AuthenticationService;
use antichris\rssReader\user\authentication\LoginForm;
use antichris\rssReader\user\UserEntity;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\authentication\LoginForm
 *
 * @method LoginForm            createInstance()                           Constructs the instance under test.
 * @method LoginForm&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<LoginForm>
 */
final class LoginFormTest extends TestCase
{
    private const METHOD_HAS_ERRORS = 'hasErrors';
    private const METHOD_GET_USER = 'getUser';
    private const METHOD_ADD_ERROR = 'addError';

    /**
     * @var AuthenticationService&MockObject
     */
    private $service;
    /**
     * @var LoginForm
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->service = $this->createMock(AuthenticationService::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideValidatePassword
     * @covers ::validatePassword
     */
    public function testValidatePassword(bool $hasErrors, ?UserEntity $user, bool $canLogIn): void
    {
        $password = '<password>';
        $attribute = 'password';
        if ($hasErrors) {
            $expectGetUer = $this->never();
        } else {
            $expectGetUer = $this->once();
        }
        if (!$hasErrors && $user) {
            $expectCanLogIn = $this->once();
        } else {
            $expectCanLogIn = $this->never();
        }
        if ($user && $canLogIn) {
            $expectAddError = $this->never();
        } else {
            $expectAddError = $this->once();
        }

        $instance = $this->mockInstance([self::METHOD_HAS_ERRORS, self::METHOD_GET_USER, self::METHOD_ADD_ERROR]);
        $instance->{$attribute} = $password;
        $instance->expects($this->once())
            ->method(self::METHOD_HAS_ERRORS)
            ->willReturn($hasErrors);
        $instance->expects($expectGetUer)
            ->method(self::METHOD_GET_USER)
            ->willReturn($user);
        $this->service->expects($expectCanLogIn)
            ->method('canLogIn')
            ->with($user, $password)
            ->willReturn($canLogIn);
        $instance->expects($expectAddError)
            ->method(self::METHOD_ADD_ERROR)
            ->with($attribute, 'Incorrect email or password.');

        $instance->validatePassword($attribute);
    }

    /**
     * @return array<string,array{bool,?UserEntity,bool}>
     */
    public function provideValidatePassword(): array
    {
        $user = $this->mockUserEntity();

        return [
            'form has errors' => [true, $user, true],
            'could not get user' => [false, null, false],
            'cannot log in' => [false, $user, false],
            'all good' => [false, $user, true],
        ];
    }

    /**
     * @dataProvider provideGetUser
     * @covers ::getUser
     *
     * @param null|false|UserEntity $cachedUser
     */
    public function testGetUser(?UserEntity $expected, $cachedUser, ?UserEntity $serviceUser): void
    {
        $email = '<email>';
        $expectGetUser = false === $cachedUser
            ? $this->once()
            : $this->never();

        $instance = $this->instance;
        $instance->email = $email;
        $this->internalSetter($instance, 'user')($cachedUser);
        $this->service->expects($expectGetUser)
            ->method('getUser')
            ->with($email)
            ->willReturn($serviceUser);

        $actual = $instance->getUser();

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{?UserEntity,null|false|UserEntity,?UserEntity}>
     */
    public function provideGetUser(): array
    {
        $user = $this->mockUserEntity();

        return [
            'nothing cached' => [$user, false, $user],
            'null by service' => [null, false, null],
            'cached null' => [null, null, $user],
            'cached, null by service' => [$user, $user, null],
        ];
    }

    protected function instanceClass(): string
    {
        return LoginForm::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->service];
    }

    /**
     * @return MockObject&UserEntity
     */
    private function mockUserEntity()
    {
        return $this->createMock(UserEntity::class);
    }
}

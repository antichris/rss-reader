<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\user\authentication;

use antichris\rssReader\tests\_support\misc\ProviderAbstractTest;
use antichris\rssReader\user\authentication\LoginForm;
use antichris\rssReader\user\authentication\LoginFormProvider;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\user\authentication\LoginFormProvider
 *
 * @extends ProviderAbstractTest<LoginFormProvider>
 */
final class LoginFormProviderTest extends ProviderAbstractTest
{
    public function provideGetClassName(): array
    {
        return [[LoginForm::class]];
    }

    protected function instanceClass(): string
    {
        return LoginFormProvider::class;
    }
}

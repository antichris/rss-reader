<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\fetcher;

use antichris\rssReader\feed\fetcher\BasicStringFetcher;
use antichris\rssReader\feed\fetcher\FetcherException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\fetcher\BasicStringFetcher
 */
final class BasicStringFetcherTest extends TestCase
{
    /**
     * @dataProvider provideFetch
     * @covers ::fetch
     *
     * @param false|string $expected
     */
    public function testFetch($expected, string $url): void
    {
        /** @var BasicStringFetcher&MockObject $instance */
        $instance = $this->createPartialMock(BasicStringFetcher::class, ['getContents']);
        $instance
            ->expects($this->once())
            ->method('getContents')
            ->with($url)
            ->willReturn($expected);

        if (false === $expected) {
            $this->expectException(FetcherException::class);
            $this->expectExceptionMessage('Failed fetching the feed data');
        }

        $actual = $instance->fetch($url);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{false|string,string}>
     */
    public function provideFetch(): array
    {
        return [
            'normal' => ['<expected>', '<url>'],
            'no contents' => ['', '<url>'],
            'error' => [false, '<url>'],
        ];
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\Item;
use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\feed\parser\AbstractSxItemMapper;
use antichris\rssReader\feed\parser\DateTimeParserInterface;
use antichris\rssReader\tests\_support\AbstractTestCase;
use antichris\rssReader\tests\_support\DateTimeHelper;
use DateTimeImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\AbstractSxItemMapper
 *
 * @method AbstractSxItemMapper&MockObject createInstance()                           Creates a mock instance of the abstract class under test.
 * @method AbstractSxItemMapper&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock instance of the abstract class under test.
 *
 * @extends AbstractTestCase<AbstractSxItemMapper>
 */
final class AbstractSxItemMapperTest extends AbstractTestCase
{
    private const METHOD_FILL = 'fill';
    private const METHOD_GET_TITLE = 'getTitle';
    private const METHOD_GET_DESCRIPTION = 'getDescription';
    private const METHOD_GET_LINK = 'getLink';
    private const METHOD_GET_PUB_DATE = 'getPubDate';
    private const METHOD_GET_UPDATED = 'getUpdated';
    private const METHOD_GET_IMAGE_URL = 'getImageURL';

    /**
     * @var ItemProvider&MockObject
     */
    private $provider;
    /**
     * @var DateTimeParserInterface&MockObject
     */
    private $dateTimeParser;
    /**
     * @var AbstractSxItemMapper&MockObject
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->provider = $this->createMock(ItemProvider::class);
        $this->dateTimeParser = $this->createMock(DateTimeParserInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::map
     */
    public function testMap(): void
    {
        $itemElement = new SimpleXMLElement('<root/>');
        $item = $this->createMock(Item::class);
        $expected = $item;

        $instance = $this->mockInstance([self::METHOD_FILL]);
        $this->provider->expects($this->once())->method('provide')->willReturn($item);
        $instance->expects($this->once())->method(self::METHOD_FILL)->with($itemElement, $item)->willReturn($expected);

        $actual = $instance->map($itemElement);

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::fill
     */
    public function testFill(): void
    {
        $itemElement = new SimpleXMLElement('<root/>');
        $item = new Item();
        $expected = new Item();
        $expected->title = '<title>';
        $expected->description = '<description>';
        $expected->link = '<link>';
        $expected->pubDate = new DateTimeImmutable('yesterday');
        $expected->updated = new DateTimeImmutable();
        $expected->image = '<image>';

        $instance = $this->mockInstance([
            self::METHOD_GET_TITLE,
            self::METHOD_GET_DESCRIPTION,
            self::METHOD_GET_LINK,
            self::METHOD_GET_PUB_DATE,
            self::METHOD_GET_UPDATED,
            self::METHOD_GET_IMAGE_URL,
        ]);
        $instance->expects($this->once())->method(self::METHOD_GET_TITLE)->with($itemElement)
            ->willReturn($expected->title);
        $instance->expects($this->once())->method(self::METHOD_GET_DESCRIPTION)->with($itemElement)
            ->willReturn($expected->description);
        $instance->expects($this->once())->method(self::METHOD_GET_LINK)->with($itemElement)
            ->willReturn($expected->link);
        $instance->expects($this->once())->method(self::METHOD_GET_PUB_DATE)->with($itemElement)
            ->willReturn($expected->pubDate);
        $instance->expects($this->once())->method(self::METHOD_GET_UPDATED)->with($itemElement)
            ->willReturn($expected->updated);
        $instance->expects($this->once())->method(self::METHOD_GET_IMAGE_URL)->with($itemElement)
            ->willReturn($expected->image);

        $actual = $instance->fill($itemElement, $item);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::getTitle
     */
    public function testGetTitle(): void
    {
        $title = 'XtitleX';
        $itemElement = new SimpleXMLElement("<root><title>{$title}</title></root>");
        $expected = $title;

        $instance = $this->instance;

        $actual = $instance->getTitle($itemElement);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::getDescription
     */
    public function testGetDescription(): void
    {
        $description = 'XdescriptionX';
        $itemElement = new SimpleXMLElement("<root><description>{$description}</description></root>");
        $expected = $description;

        $instance = $this->instance;

        $actual = $instance->getDescription($itemElement);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideGetLink
     * @covers ::getLink
     */
    public function testGetLink(?string $link, string $xml): void
    {
        $itemElement = new SimpleXMLElement("<root>{$xml}</root>");
        $expected = $link;

        $instance = $this->instance;

        $actual = $instance->getLink($itemElement);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Provides case data for testing the getLink() method.
     *
     * @return array<string,array{?string,string}>
     */
    public function provideGetLink(): array
    {
        return [
            'valid' => ['xURLx', '<link>xURLx</link>'],
            'empty link' => ['', '<link></link>'],
            'empty link tag' => ['', '<link/>'],
            'no link' => [null, ''],
        ];
    }

    /**
     * @covers ::parseDateTime
     */
    public function testParseDateTime(): void
    {
        $dti = (new DateTimeHelper())->createImmutable();
        $dateTime = $dti->format($dti::ISO8601);
        $expected = $dti;

        $instance = $this->instance;
        $instance__parseDateTime = $this->internalCall($instance, 'parseDateTime');
        $this->dateTimeParser->expects($this->once())->method('parse')->with($dateTime)->willReturn($expected);

        $actual = $instance__parseDateTime($dateTime);

        $this->assertEquals($expected, $actual);
    }

    /**
     * {@inheritdoc}
     *
     * @return MockObject[]
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->provider, $this->dateTimeParser];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return AbstractSxItemMapper::class;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\atom;

use antichris\rssReader\feed\parser\atom\SxAtomFeedLocator;
use antichris\rssReader\feed\parser\atom\SxAtomValidator;
use antichris\rssReader\tests\_support\feed\parser\SxFeedValidatorAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\atom\SxAtomValidator
 *
 * @method SxAtomValidator            createInstance()                           Constructs the instance under test.
 * @method SxAtomValidator&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock for instance under test.
 *
 * @extends SxFeedValidatorAbstractTest<SxAtomValidator>
 */
final class SxAtomValidatorTest extends SxFeedValidatorAbstractTest
{
    public function provideValidateRoot(): array
    {
        return [
            'valid' => [true, '<feed/>'],
            'non-Atom XML' => [false, '<xml/>'],
        ];
    }

    /**
     * @return array<string,array{bool,string}>
     */
    public function provideGetFeed(): array
    {
        return [
            'valid' => [true, '<feed/>'],
            'no feed' => [false, '<xml/>'],
        ];
    }

    /**
     * @return array<string,array{bool,string}>
     */
    public function provideValidateFeed(): array
    {
        $title = '<title>XtitleX</title>';
        $id = '<id>XidX</id>';
        $updated = '<updated>XupdatedX</updated>';

        return [
            'valid' => [true, "<feed>{$title}{$id}{$updated}</feed>"],
            'empty id' => [false, "<feed><id/>{$title}{$updated}</feed>"],
            'empty title' => [false, "<feed>{$id}<title/>{$updated}</feed>"],
            'empty updated' => [false, "<feed>{$id}{$title}<updated/></feed>"],
            'missing id' => [false, "<feed>{$title}{$updated}</feed>"],
            'missing title' => [false, "<feed>{$id}{$updated}</feed>"],
            'missing updated' => [false, "<feed>{$id}{$title}</feed>"],
        ];
    }

    protected function instanceClass(): string
    {
        return SxAtomValidator::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->createMock(SxAtomFeedLocator::class)];
    }
}

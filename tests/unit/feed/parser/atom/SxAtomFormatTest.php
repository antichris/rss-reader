<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\atom;

use antichris\rssReader\feed\parser\atom\SxAtomFormat;
use antichris\rssReader\feed\parser\atom\SxAtomMapper;
use antichris\rssReader\feed\parser\atom\SxAtomValidator;
use antichris\rssReader\tests\_support\feed\parser\SxFeedFormatAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\atom\SxAtomFormat
 *
 * @extends SxFeedFormatAbstractTest<SxAtomFormat>
 */
final class SxAtomFormatTest extends SxFeedFormatAbstractTest
{
    /**
     * {@inheritdoc}
     */
    public function provideGetName(): array
    {
        return [['Atom']];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxAtomFormat::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [
            $this->createMock(SxAtomValidator::class),
            $this->createMock(SxAtomMapper::class),
        ];
    }
}

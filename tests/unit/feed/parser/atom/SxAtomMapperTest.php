<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\atom;

use antichris\rssReader\feed\parser\atom\SxAtomFeedLocator;
use antichris\rssReader\feed\parser\atom\SxAtomItemMapper;
use antichris\rssReader\feed\parser\atom\SxAtomMapper;
use antichris\rssReader\tests\_support\feed\parser\SxFeedMapperAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\atom\SxAtomMapper
 *
 * @extends SxFeedMapperAbstractTest<SxAtomMapper>
 */
final class SxAtomMapperTest extends SxFeedMapperAbstractTest
{
    /**
     * {@inheritdoc}
     */
    public function provideGetItemTag(): array
    {
        return [['entry']];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxAtomMapper::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [
            $this->createMock(SxAtomFeedLocator::class),
            $this->createMock(SxAtomItemMapper::class),
        ];
    }
}

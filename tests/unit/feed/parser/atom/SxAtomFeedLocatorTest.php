<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\atom;

use antichris\rssReader\feed\parser\atom\SxAtomFeedLocator;
use PHPUnit\Framework\TestCase;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\atom\SxAtomFeedLocator
 */
final class SxAtomFeedLocatorTest extends TestCase
{
    /**
     * @var SxAtomFeedLocator
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = new SxAtomFeedLocator();
    }

    /**
     * @covers ::getFeed
     */
    public function testGetFeed(): void
    {
        $root = new SimpleXMLElement('<xml></xml>');
        $expected = $root;

        $instance = $this->instance;

        $actual = $instance->getFeed($root);

        $this->assertEquals($expected, $actual);
    }
}

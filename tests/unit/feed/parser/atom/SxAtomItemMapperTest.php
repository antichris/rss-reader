<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\atom;

use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\feed\parser\atom\AtomDateTimeParser;
use antichris\rssReader\feed\parser\atom\SxAtomItemMapper;
use antichris\rssReader\feed\parser\MapperException;
use antichris\rssReader\tests\_support\feed\parser\SxItemMapperAbstractTest;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\atom\SxAtomItemMapper
 *
 * @method SxAtomItemMapper getInstance() Returns the instance under test.
 *
 * @extends SxItemMapperAbstractTest<SxAtomItemMapper>
 */
final class SxAtomItemMapperTest extends SxItemMapperAbstractTest
{
    /**
     * @dataProvider provideGetLink
     * @covers ::getLink
     */
    public function testGetLink(?string $expected, string $xml): void
    {
        $item = $this->newSimpleXMLItem($xml);
        $instance = $this->getInstance();

        if (null === $expected) {
            $this->expectException(MapperException::class);
            $this->expectExceptionMessage('Could not extract link URL from entry');
        }

        $actual = $instance->getLink($item);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Provides case data for testing the getLink() method.
     *
     * @return array<string,array{?string,string}>
     */
    public function provideGetLink(): array
    {
        $link = 'XlinkX';

        return [
            'valid' => [$link, '<link/><link rel="alternate" href="' . $link . '"/>'],
            'empty alternate link' => [null, '<link/><link rel="alternate"/>'],
            'no alternate link' => [null, '<link/>'],
            'no links' => [null, ''],
        ];
    }

    /**
     * @dataProvider provideGetDescription
     * @covers ::getDescription
     */
    public function testGetDescription(string $expected, string $xml): void
    {
        $item = $this->newSimpleXMLItem($xml);
        $instance = $this->getInstance();

        $actual = $instance->getDescription($item);

        $this->assertEquals($expected, $actual);
    }

    /**
     * Provides case data for testing the getDescription() method.
     *
     * @return array<string,array{string,string}>
     */
    public function provideGetDescription(): array
    {
        $summary = 'XsummaryX';

        return [
            'valid' => [$summary, "<summary>{$summary}</summary>"],
            'empty' => ['', '<summary></summary>'],
            'missing' => ['', ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function provideGetPubDate(): array
    {
        $date = $this->createDateTimeImmutable();

        return [
            'valid' => [$date, "<published>{$date->format($date::ATOM)}</published>"],
            'empty' => [null, '<published></published>'],
            'missing' => [null, ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function provideGetUpdated(): array
    {
        $date = $this->createDateTimeImmutable();

        return [
            'valid' => [$date, "<updated>{$date->format($date::ATOM)}</updated>"],
            'empty' => [null, '<updated></updated>'],
            'missing' => [null, ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function provideGetImageUrl(): array
    {
        return [
            'not implemented' => [null, ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function expectParseDateTimeInGetPubDate(): InvocationOrder
    {
        return $this->once();
    }

    /**
     * {@inheritdoc}
     */
    protected function expectParseDateTimeInGetUpdated(): InvocationOrder
    {
        return $this->once();
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxAtomItemMapper::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->createMock(ItemProvider::class), $this->createMock(AtomDateTimeParser::class)];
    }
}

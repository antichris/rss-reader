<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\Item;
use antichris\rssReader\feed\parser\AbstractSxFeedMapper;
use antichris\rssReader\feed\parser\MapperException;
use antichris\rssReader\feed\parser\SxFeedElementLocatorInterface;
use antichris\rssReader\feed\parser\SxItemMapperInterface;
use antichris\rssReader\tests\_support\AbstractTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\AbstractSxFeedMapper
 *
 * @method AbstractSxFeedMapper&MockObject createInstance()                           Creates a mock instance of the abstract class under test.
 * @method AbstractSxFeedMapper&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock instance of the abstract class under test.
 *
 * @extends AbstractTestCase<AbstractSxFeedMapper>
 */
final class AbstractSxFeedMapperTest extends AbstractTestCase
{
    private const METHOD_GET_ITEM_MAPPER = 'getItemMapper';
    private const METHOD_GET_ITEM_TAG = 'getItemTag';
    private const METHOD_GET_FEED = 'getFeed';
    private const METHOD_MAP_FEED_ITEMS = 'mapFeedItems';

    /**
     * @var SxFeedElementLocatorInterface&MockObject
     */
    private $feedLocator;
    /**
     * @var SxItemMapperInterface&MockObject
     */
    private $itemMapper;
    /**
     * @var AbstractSxFeedMapper
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->feedLocator = $this->createMock(SxFeedElementLocatorInterface::class);
        $this->itemMapper = $this->createMock(SxItemMapperInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::map
     */
    public function testMap(): void
    {
        $root = new SimpleXMLElement('<root/>');
        $limit = 13;

        $mapper = $this->itemMapper;
        $itemTag = '<itemTag>';
        $feed = new SimpleXMLElement('<feed/>');
        $expected = [new Item()];

        $instance = $this->mockInstance([
            self::METHOD_GET_ITEM_MAPPER,
            self::METHOD_GET_ITEM_TAG,
            self::METHOD_GET_FEED,
            self::METHOD_MAP_FEED_ITEMS,
        ]);
        $instance->expects($this->once())->method(self::METHOD_GET_ITEM_MAPPER)->willReturn($mapper);
        $instance->expects($this->once())->method(self::METHOD_GET_ITEM_TAG)->willReturn($itemTag);
        $instance->expects($this->once())->method(self::METHOD_GET_FEED)->with($root)->willReturn($feed);
        $instance->expects($this->once())
            ->method(self::METHOD_MAP_FEED_ITEMS)
            ->with($mapper, $itemTag, $feed, $limit)
            ->willReturn($expected);

        $actual = $instance->map($root, $limit);

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider provideGetFeed
     * @covers ::getFeed
     */
    public function testGetFeed(?string $feedXml, bool $valid): void
    {
        $root = new SimpleXMLElement('<root/>');
        $feed = $feedXml
            ? new SimpleXMLElement($feedXml)
            : null;
        $expected = $feed;

        $instance = $this->instance;
        $instance__getFeed = $this->internalCall($instance, self::METHOD_GET_FEED);
        $this->feedLocator->expects($this->once())->method('getFeed')->with($root)->willReturn($feed);

        if (!$valid) {
            $this->expectException(MapperException::class);
            $this->expectExceptionMessage('Feed element not found');
        }

        $actual = $instance__getFeed($root);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{?string,bool}>
     */
    public function provideGetFeed(): array
    {
        return [
            'with item' => ['<feed><item/></feed>', true],
            'empty' => ['<feed/>', false],
            'no feed' => [null, false],
        ];
    }

    /**
     * @covers ::getItemMapper
     */
    public function testGetItemMapper(): void
    {
        $expected = $this->itemMapper;

        $instance = $this->instance;
        $instance__getItemMapper = $this->internalCall($instance, self::METHOD_GET_ITEM_MAPPER);

        $actual = $instance__getItemMapper();

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider provideMapFeedItems
     * @covers ::mapFeedItems
     */
    public function testMapFeedItems(int $expectedCount, string $itemTag, string $feedXml, int $limit): void
    {
        $mapper = $this->itemMapper;
        $feed = new SimpleXMLElement("<feed>{$feedXml}</feed>");
        $item = $this->createMock(Item::class);

        $instance = $this->instance;
        $instance__mapFeedItems = $this->internalCall($instance, self::METHOD_MAP_FEED_ITEMS);
        $this->itemMapper->expects($this->exactly(min([$expectedCount, $limit])))->method('map')->willReturn($item);

        $actual = $instance__mapFeedItems($mapper, $itemTag, $feed, $limit);

        $this->assertCount($expectedCount, $actual);
        if ($expectedCount) {
            $this->assertContains($item, $actual);
        }
    }

    /**
     * @return array<string,array{int,string,string,int}>
     */
    public function provideMapFeedItems(): array
    {
        $tag = 'item';
        $limit = 2;

        return [
            '1 item' => [1, $tag, '<item/>', $limit],
            '2 items' => [2, $tag, '<item/><item/>', $limit],
            '3 items' => [2, $tag, '<item/><item/><item/>', $limit],
            '3 entries' => [2, 'entry', '<entry/><entry/><entry/>', $limit],
            'no items' => [0, $tag, '', $limit],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @return MockObject[]
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->feedLocator, $this->itemMapper];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return AbstractSxFeedMapper::class;
    }
}

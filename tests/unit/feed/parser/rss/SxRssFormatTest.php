<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\rss;

use antichris\rssReader\feed\parser\rss\SxRssFormat;
use antichris\rssReader\feed\parser\rss\SxRssMapper;
use antichris\rssReader\feed\parser\rss\SxRssValidator;
use antichris\rssReader\tests\_support\feed\parser\SxFeedFormatAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\rss\SxRssFormat
 *
 * @method SxRssFormat createInstance() Constructs the instance under test.
 *
 * @extends SxFeedFormatAbstractTest<SxRssFormat>
 */
final class SxRssFormatTest extends SxFeedFormatAbstractTest
{
    /**
     * {@inheritdoc}
     */
    public function provideGetName(): array
    {
        return [['RSS']];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxRssFormat::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [
            $this->createMock(SxRssValidator::class),
            $this->createMock(SxRssMapper::class),
        ];
    }
}

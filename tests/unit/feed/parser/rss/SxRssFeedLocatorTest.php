<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\rss;

use antichris\rssReader\feed\parser\rss\SxRssFeedLocator;
use PHPUnit\Framework\TestCase;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\rss\SxRssFeedLocator
 */
final class SxRssFeedLocatorTest extends TestCase
{
    /**
     * @var SxRssFeedLocator
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = new SxRssFeedLocator();
    }

    /**
     * @covers ::getFeed
     */
    public function testGetFeed(): void
    {
        $root = new SimpleXMLElement('<xml></xml>');
        $expected = $root;

        $instance = $this->instance;

        $actual = $instance->getFeed($root);

        $this->assertEquals($expected, $actual);
    }
}

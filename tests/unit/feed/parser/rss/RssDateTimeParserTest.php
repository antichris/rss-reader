<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\rss;

use antichris\rssReader\feed\parser\rss\RssDateTimeParser;
use antichris\rssReader\tests\_support\feed\parser\DateTimeParserAbstractTest;
use DateTimeImmutable;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\rss\RssDateTimeParser
 *
 * @extends DateTimeParserAbstractTest<RssDateTimeParser>
 */
final class RssDateTimeParserTest extends DateTimeParserAbstractTest
{
    public function provideGetDateFormat(): array
    {
        return [[DateTimeImmutable::RSS]];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return RssDateTimeParser::class;
    }
}

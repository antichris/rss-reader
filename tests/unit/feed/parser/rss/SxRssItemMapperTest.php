<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\rss;

use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\feed\parser\rss\RssDateTimeParser;
use antichris\rssReader\feed\parser\rss\SxRssItemMapper;
use antichris\rssReader\tests\_support\feed\parser\SxItemMapperAbstractTest;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\rss\SxRssItemMapper
 *
 * @method SxRssItemMapper getInstance() Returns the instance under test.
 *
 * @extends SxItemMapperAbstractTest<SxRssItemMapper>
 */
final class SxRssItemMapperTest extends SxItemMapperAbstractTest
{
    /**
     * @dataProvider provideIsImageTypeSupported
     * @covers ::isImageTypeSupported
     */
    public function testIsImageTypeSupported(bool $expected, ?string $type): void
    {
        $typeAttribute = null !== $type
            ? " type=\"{$type}\""
            : null;
        $url = 'xURLx';
        $item = $this->newSimpleXMLItem("<enclosure url=\"{$url}\"{$typeAttribute}/>");

        $instance = $this->getInstance();

        $actual = $instance->getImageURL($item);

        $this->assertEquals($expected, $actual === $url);
    }

    /**
     * @return array<string,array{bool,?string}>
     */
    public function provideIsImageTypeSupported(): array
    {
        return [
            'image/jpeg' => [true, 'image/jpeg'],
            'image/png' => [true, 'image/png'],
            'application/octet-stream (not)' => [false, 'application/octet-stream'],
            'empty type attribute (not)' => [false, ''],
            'no type attribute (not)' => [false, null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function provideGetPubDate(): array
    {
        $date = $this->createDateTimeImmutable();

        return [
            'valid' => [$date, "<pubDate>{$date->format($date::RSS)}</pubDate>"],
            'empty' => [null, '<pubDate></pubDate>'],
            'missing' => [null, ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function provideGetUpdated(): array
    {
        return [
            'not implemented' => [null, ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function provideGetImageUrl(): array
    {
        return [
            'normal' => ['xURLx', '<enclosure url="xURLx" type="image/jpeg"/>'],
            'empty attribute (not)' => [null, '<enclosure url="" type="image/jpeg"/>'],
            'no attribute (not)' => [null, '<enclosure type="image/jpeg"/>'],
            'no type attribute (not)' => [null, '<enclosure url="xURLx"/>'],
            'no enclosure (not)' => [null, ''],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function expectParseDateTimeInGetPubDate(): InvocationOrder
    {
        return $this->once();
    }

    /**
     * {@inheritdoc}
     */
    protected function expectParseDateTimeInGetUpdated(): InvocationOrder
    {
        return $this->never();
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxRssItemMapper::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->createMock(ItemProvider::class), $this->createMock(RssDateTimeParser::class)];
    }
}

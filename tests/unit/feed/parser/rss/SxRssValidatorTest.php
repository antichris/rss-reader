<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\rss;

use antichris\rssReader\feed\parser\rss\SxRssFeedLocator;
use antichris\rssReader\feed\parser\rss\SxRssValidator;
use antichris\rssReader\tests\_support\feed\parser\SxFeedValidatorAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\rss\SxRssValidator
 *
 * @method SxRssValidator            createInstance()                           Constructs the instance under test.
 * @method SxRssValidator&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock for instance under test.
 *
 * @extends SxFeedValidatorAbstractTest<SxRssValidator>
 */
final class SxRssValidatorTest extends SxFeedValidatorAbstractTest
{
    public function provideValidateRoot(): array
    {
        return [
            'valid' => [true, '<rss/>'],
            'non-RSS XML' => [false, '<xml/>'],
        ];
    }

    /**
     * @return array<string,array{bool,string}>
     */
    public function provideGetFeed(): array
    {
        return [
            'valid' => [true, '<rss><channel/></rss>'],
            'no feed' => [false, '<rss/>'],
        ];
    }

    /**
     * @return array<string,array{bool,string}>
     */
    public function provideValidateFeed(): array
    {
        $title = '<title>XtitleX</title>';
        $link = '<link>XlinkX</link>';
        $description = '<description>XdescriptionX</description>';

        return [
            'valid' => [true, "<channel>{$title}{$link}{$description}</channel>"],
            'empty title' => [false, "<channel><title/>{$link}{$description}</channel>"],
            'empty link' => [false, "<channel>{$title}<link/>{$description}</channel>"],
            'empty description' => [false, "<channel>{$title}{$link}<description/></channel>"],
            'missing title' => [false, "<channel><title/>{$link}{$description}</channel>"],
            'missing link' => [false, "<channel>{$title}<link/>{$description}</channel>"],
            'missing description' => [false, "<channel>{$title}{$link}<description/></channel>"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxRssValidator::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->createMock(SxRssFeedLocator::class)];
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser\rss;

use antichris\rssReader\feed\parser\rss\SxRssFeedLocator;
use antichris\rssReader\feed\parser\rss\SxRssItemMapper;
use antichris\rssReader\feed\parser\rss\SxRssMapper;
use antichris\rssReader\tests\_support\feed\parser\SxFeedMapperAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\rss\SxRssMapper
 *
 * @extends SxFeedMapperAbstractTest<SxRssMapper>
 */
final class SxRssMapperTest extends SxFeedMapperAbstractTest
{
    /**
     * {@inheritdoc}
     */
    public function provideGetItemTag(): array
    {
        return [['item']];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return SxRssMapper::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [
            $this->createMock(SxRssFeedLocator::class),
            $this->createMock(SxRssItemMapper::class),
        ];
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxFeedValidator;
use antichris\rssReader\feed\parser\SxFeedElementLocatorInterface;
use antichris\rssReader\tests\_support\AbstractTestCase;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\AbstractSxFeedValidator
 *
 * @method AbstractSxFeedValidator&MockObject createInstance()                           Creates a mock instance of the abstract class under test.
 * @method AbstractSxFeedValidator&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock instance of the abstract class under test.
 *
 * @extends AbstractTestCase<AbstractSxFeedValidator>
 */
final class AbstractSxFeedValidatorTest extends AbstractTestCase
{
    private const METHOD_GET_FEED = 'getFeed';

    /**
     * @var AbstractSxFeedValidator&MockObject
     */
    private $instance;
    /**
     * @var SxFeedElementLocatorInterface&MockObject
     */
    private $feedLocator;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->feedLocator = $this->createMock(SxFeedElementLocatorInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideValidate
     * @covers ::validate
     */
    public function testValidate(bool $expected, bool $rootValid, ?SimpleXMLElement $feed, bool $feedValid): void
    {
        $root = new SimpleXMLElement('<xml/>');

        $expectGetFeed = $rootValid
            ? $this->once()
            : $this->never();
        $expectValidateFeed = $rootValid && $feed
            ? $this->once()
            : $this->never();

        $instance = $this->mockInstance([self::METHOD_GET_FEED]);
        $this->mockValidateRoot($instance, $root, $rootValid);
        $instance->expects($expectGetFeed)->method(self::METHOD_GET_FEED)->with($root)->willReturn($feed);
        $this->mockValidateFeed($instance, $expectValidateFeed, $feed, $feedValid);

        $actual = $instance->validate($root);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,bool,?SimpleXMLElement,bool}>
     */
    public function provideValidate(): array
    {
        $feed = new SimpleXMLElement('<xml><foo/></xml>');

        return [
            'valid' => [true, true, $feed, true],
            'invalid feed' => [false, true, $feed, false],
            'no feed' => [false, true, null, false],
            'invalid/borked root' => [false, false, null, false],
        ];
    }

    /**
     * @dataProvider provideGetFeed
     * @covers ::getFeed
     */
    public function testGetFeed(bool $expected, ?SimpleXMLElement $feed): void
    {
        $root = new SimpleXMLElement('<xml/>');
        $rootValid = true;

        $expectValidateFeed = $feed
            ? $this->once()
            : $this->never();

        $instance = $this->instance;
        $this->mockValidateRoot($instance, $root, $rootValid);
        $this->feedLocator->expects($this->once())->method('getFeed')->with($root)->willReturn($feed);
        $this->mockValidateFeed($instance, $expectValidateFeed, $feed, $expected);

        $actual = $instance->validate($root);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,?SimpleXMLElement}>
     */
    public function provideGetFeed(): array
    {
        $feed = new SimpleXMLElement('<xml><foo/></xml>');

        return [
            'valid' => [true, $feed->foo],
            'no feed' => [false, null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return AbstractSxFeedValidator::class;
    }

    /**
     * {@inheritdoc}
     *
     * @return MockObject[]
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->feedLocator];
    }

    /**
     * @return InvocationMocker<AbstractSxFeedValidator>
     */
    private function mockValidateRoot(
        MockObject $instance,
        SimpleXMLElement $root,
        bool $rootValid,
    ): InvocationMocker {
        return $instance->expects($this->once())
            ->method('validateRoot')
            ->with($root)
            ->willReturn($rootValid);
    }

    /**
     * @return InvocationMocker<AbstractSxFeedValidator>
     */
    private function mockValidateFeed(
        MockObject $instance,
        InvocationOrder $invocationRule,
        ?SimpleXMLElement $feed,
        bool $feedValid,
    ): InvocationMocker {
        return $instance->expects($invocationRule)
            ->method('validateFeed')
            ->with($feed)
            ->willReturn($feedValid);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\parser\FormatDetector;
use antichris\rssReader\feed\parser\SxFeedFormatInterface;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\TestCase;
use InvalidArgumentException;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;
use stdClass;
use Throwable;
use TypeError;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\FormatDetector
 *
 * @extends TestCase<FormatDetector>
 */
final class FormatDetectorTest extends TestCase
{
    use MockHelperTrait;
    private const XML = '<xml/>';

    /**
     * @var SxFeedFormatInterface&MockObject
     */
    private $format;
    /**
     * @var FormatDetector
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->format = $this->createMock(SxFeedFormatInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @dataProvider provideConstruct
     * @covers ::__construct
     *
     * @param class-string<Throwable> $exception
     * @param (null|object|SxFeedFormatInterface)[] $formats
     */
    public function testConstruct(?string $exception, ?string $message, array $formats): void
    {
        if ($exception && $message) {
            $this->expectException($exception);
            $this->expectErrorMessage($message);
        }

        $instance = $this->createInstance($formats);

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @return array<string,array{?class-string<Throwable>,?string,(null|object|SxFeedFormatInterface)[]}>
     */
    public function provideConstruct(): array
    {
        $formatinterface = SxFeedFormatInterface::class;
        $format = $this->createMock($formatinterface);

        $typeError = TypeError::class;

        $constructor = FormatDetector::class . '::__construct()';
        $fmtMsg = fn (int $n, string $given) => "{$constructor}: Argument #{$n}"
            . " must be of type {$formatinterface}, {$given} given";

        return [
            'valid one' => [null, null, [$format]],
            'valid two' => [null, null, [$format, $format]],
            'first not an object' => [$typeError, $fmtMsg(1, 'null'), [null, $format]],
            'second not a format' => [$typeError, $fmtMsg(2, 'stdClass'), [$format, new stdClass()]],
            'third not an object' => [$typeError, $fmtMsg(3, 'null'), [$format, $format, null]],
            'missing' => [InvalidArgumentException::class, 'At least one feed format required', []],
        ];
    }

    /**
     * @dataProvider provideDetect
     * @covers ::detect
     *
     * @todo Consider a more robust test design and provider.
     */
    public function testDetect(bool $detected): void
    {
        $root = new SimpleXMLElement(self::XML);
        $expected = $detected
            ? $this->format
            : null;

        $instance = $this->instance;
        $this->format->expects($this->once())->method('validate')->with($root)->willReturn($detected);

        $actual = $instance->detect($root);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{bool}>
     */
    public function provideDetect(): array
    {
        return [
            'detected' => [true],
            'not detected' => [false],
        ];
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->format];
    }

    protected function instanceClass(): string
    {
        return FormatDetector::class;
    }

    /**
     * @param null|(null|object|SxFeedFormatInterface)[] $ctorArgs
     */
    protected function createInstance(?array $ctorArgs = null): FormatDetector
    {
        if (null === $ctorArgs) {
            $ctorArgs = $this->instanceCtorArgs();
        }
        $instanceClass = $this->instanceClass();

        return new $instanceClass(...$ctorArgs);
    }
}

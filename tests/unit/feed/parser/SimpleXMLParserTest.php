<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxFeedFormat;
use antichris\rssReader\feed\parser\AbstractSxFeedMapper;
use antichris\rssReader\feed\parser\FormatDetectorInterface;
use antichris\rssReader\feed\parser\ParserException;
use antichris\rssReader\feed\parser\SimpleXMLParser;
use antichris\rssReader\feed\parser\SxFeedFormatInterface;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\SimpleXMLParser
 *
 * @method SimpleXMLParser             createInstance()                             Constructs the instance under test.
 * @method SimpleXMLParser&MockObject  mockInstance(array $mockedMethods = [])      Creates a partial mock of the instance under test.
 * @method SimpleXMLParser&MockBuilder buildInstanceMock(array $mockedMethods = []) Returns an instance mock builder with constructor arguments, and, optionally, mocked methods, already set.
 *
 * @extends TestCase<SimpleXMLParser>
 */
final class SimpleXMLParserTest extends TestCase
{
    use MockHelperTrait;
    private const XML = '<xml/>';
    private const METHOD_CREATE_SIMPLE_XML_ELEMENT = 'createSimpleXMLElement';

    /**
     * @var FormatDetectorInterface&MockObject
     */
    private $formatDetector;
    /**
     * @var SimpleXMLParser
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->formatDetector = $this->createMock(FormatDetectorInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::parse
     */
    public function testParse(): void
    {
        $xml = '<xml>';
        $element = new SimpleXMLElement(self::XML);
        $expected = ['<expected>'];

        $format = $this->createMock(AbstractSxFeedFormat::class);
        $mapper = $this->createMock(AbstractSxFeedMapper::class);
        $instance = $this->mockInstanceCreateElement($xml, $element);
        $this->mockFormatDetectorDetect($element, $format);
        $this->helpMock()->methodOnce($format, 'getMapper', $mapper);
        $this->helpMock()->methodOnce($mapper, 'map', $expected, $element);

        $actual = $instance->parse($xml);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::parse
     */
    public function testParseThrowsOnBadRss(): void
    {
        $xml = '<xml>';
        $element = new SimpleXMLElement(self::XML);

        /** @var SimpleXMLParser $instance */
        $instance = $this->mockInstanceCreateElement($xml, $element);
        $this->mockFormatDetectorDetect($element, null);
        $this->expectException(ParserException::class);
        $this->expectExceptionMessage('Could not detect feed format of the XML data');

        $instance->parse($xml);
    }

    /**
     * @covers ::parse
     */
    public function testParseThrowsOnBadXml(): void
    {
        $xml = '<xml>';
        /** @var SimpleXMLParser $instance */
        $instance = $this->mockInstanceCreateElement($xml, false);

        $this->expectException(ParserException::class);
        $this->expectExceptionMessage('Could not parse feed data as XML');

        $instance->parse($xml);
    }

    /**
     * @covers ::createSimpleXMLElement
     */
    public function testCreateElement(): void
    {
        $xmlContents = 'rainbows and unicorns';
        $xml = "<xml>{$xmlContents}</xml>";

        /** @var SimpleXMLParser $instance */
        $instance = $this->instance;
        $instance__createSimpleXMLElement = $this->internalCall($instance, self::METHOD_CREATE_SIMPLE_XML_ELEMENT);

        $actual = $instance__createSimpleXMLElement($xml);

        $this->assertInstanceOf(SimpleXMLElement::class, $actual);
        $actualXml = $actual->asXML();
        $this->assertIsString($actualXml);
        $this->assertStringContainsString($xmlContents, $actualXml);
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->formatDetector];
    }

    protected function instanceClass(): string
    {
        return SimpleXMLParser::class;
    }

    /**
     * @param false|SimpleXMLElement $result
     *
     * @return SimpleXMLParser&MockObject
     */
    private function mockInstanceCreateElement(string $xml, $result): MockObject
    {
        $instance = $this->mockInstance([self::METHOD_CREATE_SIMPLE_XML_ELEMENT]);
        $instance->expects($this->once())
            ->method(self::METHOD_CREATE_SIMPLE_XML_ELEMENT)
            ->with($xml)
            ->willReturn($result);

        return $instance;
    }

    /**
     * @return InvocationMocker<FormatDetectorInterface>
     */
    private function mockFormatDetectorDetect(SimpleXMLElement $root, ?SxFeedFormatInterface $format): InvocationMocker
    {
        return $this->formatDetector->expects($this->once())
            ->method('detect')
            ->with($root)
            ->willReturn($format);
    }
}

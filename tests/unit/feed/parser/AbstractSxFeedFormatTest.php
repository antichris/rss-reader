<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\parser\AbstractSxFeedFormat;
use antichris\rssReader\feed\parser\SxFeedMapperInterface;
use antichris\rssReader\feed\parser\SxFeedValidatorInterface;
use antichris\rssReader\tests\_support\AbstractTestCase;
use antichris\rssReader\tests\_support\MockHelperTrait;
use PHPUnit\Framework\MockObject\MockObject;
use SimpleXMLElement;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\AbstractSxFeedFormat
 *
 * @method AbstractSxFeedFormat&MockObject createInstance()                           Creates a mock instance of the abstract class under test.
 * @method AbstractSxFeedFormat&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock instance of the abstract class under test.
 *
 * @extends AbstractTestCase<AbstractSxFeedFormat>
 */
final class AbstractSxFeedFormatTest extends AbstractTestCase
{
    use MockHelperTrait;
    /**
     * @var SxFeedMapperInterface&MockObject
     */
    private $mapper;
    /**
     * @var SxFeedValidatorInterface&MockObject
     */
    private $validator;
    /**
     * @var AbstractSxFeedFormat
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->mapper = $this->createMock(SxFeedMapperInterface::class);
        $this->validator = $this->createMock(SxFeedValidatorInterface::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideValidate
     * @covers ::validate
     */
    public function testValidate(bool $isValid): void
    {
        $root = new SimpleXMLElement('<xml></xml>');
        $expected = $isValid;

        $instance = $this->instance;
        $this->validator->expects($this->once())->method('validate')->with($root)->willReturn($isValid);

        $actual = $instance->validate($root);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool}>
     */
    public function provideValidate(): array
    {
        return [
            'valid' => [true],
            'invalid' => [true],
        ];
    }

    /**
     * @covers ::getMapper
     */
    public function testGetMapper(): void
    {
        $expected = $this->mapper;
        $instance = $this->instance;

        $actual = $instance->getMapper();

        $this->assertEquals($expected, $actual);
    }

    /**
     * {@inheritdoc}
     *
     * @return MockObject[]
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->validator, $this->mapper];
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return AbstractSxFeedFormat::class;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\parser;

use antichris\rssReader\feed\parser\AbstractDateTimeParser;
use antichris\rssReader\tests\_support\AbstractTestCase;
use antichris\rssReader\tests\_support\DateTimeHelper;
use DateTimeImmutable;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\parser\AbstractDateTimeParser
 *
 * @method AbstractDateTimeParser&MockObject createInstance()                           Creates a mock instance of the abstract class under test.
 * @method AbstractDateTimeParser&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock instance of the abstract class under test.
 *
 * @extends AbstractTestCase<AbstractDateTimeParser>
 */
final class AbstractDateTimeParserTest extends AbstractTestCase
{
    private const METHOD_CREATE_FROM_FORMAT = 'createFromFormat';
    private const METHOD_GET_DATE_FORMAT = 'getDateFormat';

    /**
     * @covers ::parse
     */
    public function testParse(): void
    {
        $dateTime = '<dateTime>';
        $format = '<format>';
        $expected = new DateTimeImmutable();

        $instance = $this->mockInstance([self::METHOD_CREATE_FROM_FORMAT]);
        $instance->expects($this->once())->method(self::METHOD_GET_DATE_FORMAT)->willReturn($format);
        $instance->expects($this->once())
            ->method(self::METHOD_CREATE_FROM_FORMAT)
            ->with($format, $dateTime)
            ->willReturn($expected);

        $actual = $instance->parse($dateTime);

        $this->assertSame($expected, $actual);
    }

    /**
     * @covers ::createFromFormat
     */
    public function testCreateFromFormat(): void
    {
        $dti = (new DateTimeHelper())->createImmutable();

        $format = $dti::RSS;
        $dateTime = $dti->format($format);
        $expected = $dti;

        $instance = $this->createInstance();
        $instance__createFromFormat = $this->internalCall($instance, self::METHOD_CREATE_FROM_FORMAT);

        $actual = $instance__createFromFormat($format, $dateTime);

        $this->assertEquals($expected, $actual);
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return AbstractDateTimeParser::class;
    }
}

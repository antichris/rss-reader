<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed;

use antichris\rssReader\feed\FeedService;
use antichris\rssReader\feed\StringFetcherInterface;
use antichris\rssReader\feed\StringParserInterface;
use antichris\rssReader\misc\profiling\ProfilerInterface;
use antichris\rssReader\tests\_support\misc\profiling\ProfilingDoubler;
use antichris\rssReader\tests\_support\MockHelperTrait;
use Exception;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\FeedService
 */
final class FeedServiceTest extends TestCase
{
    use MockHelperTrait;
    /**
     * @var FeedService
     */
    private $instance;
    /**
     * @var MockObject&StringFetcherInterface
     */
    private $fetcher;
    /**
     * @var MockObject&StringParserInterface
     */
    private $parser;
    /** @var Stub&ProfilerInterface */
    private Stub $profiler;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->fetcher = $this->createMock(StringFetcherInterface::class);
        $this->parser = $this->createMock(StringParserInterface::class);
        $this->profiler = (new ProfilingDoubler($this))->stubProfiler();
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(FeedService::class, $instance);
    }

    /**
     * @dataProvider provideRetrieve
     * @covers ::retrieve
     *
     * @param string[] $items
     */
    public function testRetrieve(string $xml, array $items, string $error): void
    {
        $url = '<url>';
        $expected = $items;

        $instance = $this->instance;
        $instance->feedURL = $url;
        $fetchInvoation = $this->mockFetcherFetch($url, $xml);
        if (empty($xml)) {
            $fetchInvoation->willThrowException(new Exception($error));
        } else {
            $parseInvoation = $this->mockParserParse($xml, $items);
            if (empty($items)) {
                $parseInvoation->willThrowException(new Exception($error));
            }
        }
        if (!empty($error)) {
            $this->expectException(Exception::class);
            $this->expectExceptionMessage($error);
        }

        $actual = $instance->retrieve();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string,string[],string}>
     */
    public function provideRetrieve(): array
    {
        return [
            'normal' => ['<xml>', ['<parsed>'], ''],
            'fetcher error' => ['', [], '<fetcher error>'],
            'parser error' => ['<xml>', [], '<parser error>'],
        ];
    }

    /**
     * @covers ::retrieve
     */
    public function testRetrieveCustomUrl(): void
    {
        $url = '<url>';
        $xml = '<xml>';
        $expected = ['<expected>'];

        $instance = $this->instance;
        $instance->feedURL = '<defaultURL>';
        $this->mockFetcherFetch($url, $xml);
        $this->mockParserParse($xml, $expected);

        $actual = $instance->retrieve($url);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return InvocationMocker<StringFetcherInterface&MockObject>
     */
    private function mockFetcherFetch(string $url, string $xml): InvocationMocker
    {
        return $this->helpMock()->methodOnce($this->fetcher, 'fetch', $xml, $url);
    }

    /**
     * @param null|string[] $result
     *
     * @return InvocationMocker<StringParserInterface&MockObject>
     */
    private function mockParserParse(string $xml, ?array $result): InvocationMocker
    {
        return $this->helpMock()->methodOnce($this->parser, 'parse', $result, $xml);
    }

    private function createInstance(): FeedService
    {
        $instanceClass = $this->instanceClass();

        return new $instanceClass(...$this->instanceCtorArgs());
    }

    /**
     * @return array{StringFetcherInterface,StringParserInterface,ProfilerInterface}
     */
    private function instanceCtorArgs(): array
    {
        return [$this->fetcher, $this->parser, $this->profiler];
    }

    /**
     * @return class-string<FeedService>
     */
    private function instanceClass(): string
    {
        return FeedService::class;
    }
}

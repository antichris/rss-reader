<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\keywordExtractor\WordExtractor
 */
final class WordExtractorTest extends TestCase
{
    /**
     * @dataProvider provideExtract
     * @covers ::extract
     *
     * @param string[] $expected
     * @param string[] $words
     */
    public function testExtract(array $expected, string $string, array $words): void
    {
        $instance = $this->mockInstance(['extractWords', 'filterNonWords']);
        $this->mockExtractWords($instance, $string, $words);
        if (empty($words)) {
            $this->mockNeverFilterNonwords($instance);
        } else {
            $this->mockFilterNonwordsOnce($instance, $expected)->with($words);
        }

        $actual = $instance->extract($string);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string[],string,string[]}>
     */
    public function provideExtract(): array
    {
        return [
            'words' => [['<filteredWords>'], '<string>', ['<words>']],
            'no words' => [[], '<string with no words>', []],
            'empty string' => [[], '', []],
        ];
    }

    /**
     * @dataProvider provideExtractWords
     * @covers ::extractWords
     *
     * @param string[] $expected
     */
    public function testExtractWords(array $expected, string $string, bool $expectFilter): void
    {
        $instance = $this->mockInstance(['filterNonWords']);
        $filtered = [];

        if ($expectFilter) {
            $filtered = ['filtered'];
            $this->mockFilterNonwordsOnce($instance, $filtered)->with($expected);
        } else {
            $this->mockNeverFilterNonwords($instance);
        }

        $actual = $instance->extract($string);

        $this->assertEquals($filtered, $actual);
    }

    /**
     * @return array<string,array{string[],string,bool}>
     */
    public function provideExtractWords(): array
    {
        return [
            'words' => [[
                'Word', 'another', 'dash-word', "apostroph'd", 'l33t',
            ], "Word: another; dash-word, apostroph'd, l33t", true],
            'symbols' => [['-', "'"], '` - = [ ] \ ; \' , . / ~ ! @ # $ % ^ & * ( ) _ + { } | : " < > ?', true],
            'symbols clumped' => [['-', "'"], '`-=[]\;\',. /~!@#$%^& *()_+{ }|:"<>?', true],
            'empty string' => [[], '', false],
        ];
    }

    /**
     * @dataProvider provideFilterNonWords
     * @covers ::filterNonWords
     *
     * @param string[] $expected
     * @param string[] $words
     */
    public function testFilterNonWords(array $expected, array $words): void
    {
        $string = '<string>';
        $instance = $this->mockInstance(['extractWords']);
        $this->mockExtractWords($instance, $string, $words);

        $actual = $instance->extract($string);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string[],string[]}>
     */
    public function provideFilterNonWords(): array
    {
        return [
            'words' => [['word', 'dash-word', "apostroph'd"], ['word', 'dash-word', "apostroph'd"]],
            'number-words' => [["cloud9's", 'Java11', 'haxx0r'], ["cloud9's", 'Java11', 'haxx0r']],
            'numbers' => [[], ['123', '1-2', '12.34', '1,234.56']],
            'symbols' => [[], ['-_-', "-'-", "_-'-_", '-']],
            'nothing' => [[], []],
        ];
    }

    /**
     * @param string[] $methods
     *
     * @return WordExtractor&MockObject
     */
    private function mockInstance(array $methods): MockObject
    {
        return $instance = $this->createPartialMock(WordExtractor::class, $methods);
    }

    /**
     * @return InvocationMocker<WordExtractor>
     */
    private function mockNeverFilterNonwords(MockObject $instance): InvocationMocker
    {
        return $instance->expects($this->never())
            ->method('filterNonWords');
    }

    /**
     * @param string[] $result
     *
     * @return InvocationMocker<WordExtractor>
     */
    private function mockFilterNonwordsOnce(MockObject $instance, array $result): InvocationMocker
    {
        return $instance->expects($this->once())
            ->method('filterNonWords')
            ->willReturn($result);
    }

    /**
     * @param string[] $result
     *
     * @return InvocationMocker<WordExtractor>
     */
    private function mockExtractWords(MockObject $instance, string $string, array $result): InvocationMocker
    {
        return $instance->expects($this->once())
            ->method('extractWords')
            ->with($string)
            ->willReturn($result);
    }
}

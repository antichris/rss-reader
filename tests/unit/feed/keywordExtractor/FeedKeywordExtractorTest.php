<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\keywordExtractor;

use antichris\rssReader\feed\Item;
use antichris\rssReader\feed\keywordExtractor\FeedKeywordExtractor;
use antichris\rssReader\feed\keywordExtractor\KeywordExtractor;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\keywordExtractor\FeedKeywordExtractor
 *
 * @method FeedKeywordExtractor            createInstance()                           Constructs the instance under test.
 * @method FeedKeywordExtractor&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<FeedKeywordExtractor>
 */
final class FeedKeywordExtractorTest extends TestCase
{
    private const METHOD_AGGREGATE_TEXT = 'aggregateText';
    private const METHOD_STRIP = 'strip';

    /**
     * @var FeedKeywordExtractor
     */
    private $instance;
    /**
     * @var KeywordExtractor&MockObject
     */
    private $extractor;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->extractor = $this->createMock(KeywordExtractor::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::extractTop
     */
    public function testExtractTop(): void
    {
        $count = 13;
        $items = [new Item()];
        $aggregatedText = '<aggregatedText>';
        $expected = ['<expected>' => 6];

        $instance = $this->mockInstance([self::METHOD_AGGREGATE_TEXT]);
        $instance->expects($this->once())
            ->method(self::METHOD_AGGREGATE_TEXT)
            ->with($items)
            ->willReturn($aggregatedText);
        $this->extractor->expects($this->once())
            ->method('extractTop')
            ->with($count, $aggregatedText)
            ->willReturn($expected);

        $actual = $instance->extractTop($count, $items);

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider provideAggregateText
     * @covers ::aggregateText
     *
     * @param Item[] $items
     */
    public function testAggregateText(string $aggregatedText, array $items): void
    {
        $expected = '<expected>';

        $instance = $this->mockInstance([self::METHOD_STRIP]);
        $instance__aggregateText = $this->internalCall($instance, 'aggregateText');
        $instance->expects($this->once())
            ->method(self::METHOD_STRIP)
            ->with($aggregatedText)
            ->willReturn($expected);

        $actual = $instance__aggregateText($items);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{string,Item[]}>
     */
    public function provideAggregateText(): array
    {
        $items = [];
        for ($i = 1; $i <= 3; ++$i) {
            $item = new Item();
            $items[] = $item;

            foreach (['title', 'description'] as $field) {
                $item->{$field} = "<{$field[0]}{$i}>";
            }
        }
        $item1text = "<t1>\n<d1>";
        $item1and2text = "{$item1text}\n<t2>\n<d2>";

        return [
            'one item' => [$item1text, array_slice($items, 0, 1)],
            'two items' => [$item1and2text, array_slice($items, 0, 2)],
            'three items' => ["{$item1and2text}\n<t3>\n<d3>", $items],
            'no items' => ['', []],
        ];
    }

    /**
     * @dataProvider provideStrip
     * @covers ::strip
     */
    public function testStrip(string $expected, string $text): void
    {
        $instance = $this->instance;
        $instance__strip = $this->internalCall($instance, 'strip');

        $actual = $instance__strip($text);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{string,string}>
     */
    public function provideStrip(): array
    {
        $ipsum = 'Bacon ipsum dolor amet venison filet mignon tri-tip chuck cupim sirloin.';

        return [
            'plain text' => [$ipsum, $ipsum],
            'text with tags' => [$ipsum, '<div>Bacon <strong>ipsum</strong> dolor amet venison filet <p>mignon tri-tip <span>chuck cupim sirloin.</div>'],
            'no text' => ['', ''],
        ];
    }

    protected function instanceClass(): string
    {
        return FeedKeywordExtractor::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->extractor];
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed\keywordExtractor;

use antichris\rssReader\feed\keywordExtractor\KeywordExtractor;
use antichris\rssReader\feed\keywordExtractor\WordExtractor;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\keywordExtractor\KeywordExtractor
 *
 * @method KeywordExtractor            createInstance()                           Constructs the instance under test.
 * @method KeywordExtractor&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<KeywordExtractor>
 */
final class KeywordExtractorTest extends TestCase
{
    private const METHOD_EXTRACT = 'extract';
    private const METHOD_EXTRACT_SORTED = 'extractSorted';
    private const METHOD_SORT = 'sort';
    private const METHOD_EXTRACTOR_EXTRACT = 'extract';
    private const METHOD_FILTER = 'filter';
    private const METHOD_COUNT = 'count';

    /**
     * @var KeywordExtractor
     */
    private $instance;
    /**
     * @var WordExtractor&MockObject
     */
    private $extractor;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->extractor = $this->createMock(WordExtractor::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideExtract
     * @covers ::extract
     *
     * @param tWordCount $expected
     */
    public function testExtract(array $expected, string $string): void
    {
        $extracted = ['<extracted>'];
        $filtered = ['<filtered>'];

        $instance = $this->mockInstance([self::METHOD_FILTER, self::METHOD_COUNT]);
        $this->extractor->expects($this->once())
            ->method(self::METHOD_EXTRACTOR_EXTRACT)
            ->with($string)
            ->willReturn($extracted);
        $instance->expects($this->once())
            ->method(self::METHOD_FILTER)
            // TODO: Think of a better design that would allow specifying (expecting) a specific blacklist.
            ->with($extracted, $this->anything())
            ->willReturn($filtered);
        $instance->expects($this->once())
            ->method(self::METHOD_COUNT)
            ->with($filtered)
            ->willReturn($expected);

        $actual = $instance->extract($string);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{tWordCount,string}>
     */
    public function provideExtract(): array
    {
        $string = 'Another word another word yetAnother';

        return [
            'string' => [['another' => 2, 'word' => 2, 'yetanother' => 1], $string],
            'empty string' => [[], ''],
        ];
    }

    /**
     * @dataProvider provideExtractSorted
     * @covers ::extractSorted
     *
     * @param tWordCount $expected
     */
    public function testExtractSorted(array $expected, string $string, int $order): void
    {
        $extracted = ['<words>' => 13];

        $instance = $this->mockInstance([self::METHOD_EXTRACT, self::METHOD_SORT]);
        $this->mockInstanceExtract($instance, $string, $extracted);
        $instance->expects($this->once())
            ->method(self::METHOD_SORT)
            ->with($extracted, $order)
            ->willReturn($expected);

        $actual = $instance->extractSorted($string, $order);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{tWordCount,string,int}>
     */
    public function provideExtractSorted(): array
    {
        $string = 'Another word another word yetAnother';

        return [
            'descending' => [['another' => 2, 'word' => 2, 'yetanother' => 1], $string, SORT_DESC],
            'ascending' => [['yetanother' => 1, 'another' => 2, 'word' => 2], $string, SORT_ASC],
        ];
    }

    /**
     * @dataProvider provideExtractTop
     * @covers ::extractTop
     *
     * @param tWordCount $expected
     */
    public function testExtractTop(array $expected, int $count, string $string): void
    {
        $order = KeywordExtractor::ORDER_DEFAULT;

        $instance = $this->mockInstance([self::METHOD_EXTRACT_SORTED]);
        $instance->expects($this->once())
            ->method(self::METHOD_EXTRACT_SORTED)
            ->with($string, $order)
            ->willReturn($expected);

        $actual = $instance->extractTop($count, $string);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{tWordCount,int,string}>
     */
    public function provideExtractTop(): array
    {
        $string = 'Another word another word yetAnother';

        return [
            'top 3' => [['another' => 2, 'word' => 2, 'yetanother' => 1], 3, $string],
            'top 2' => [['another' => 2, 'word' => 2], 2, $string],
        ];
    }

    /**
     * @covers ::count
     */
    public function testCount(): void
    {
        $words = ['Word', 'word', 'word', 'another'];
        $expected = ['word' => 3, 'another' => 1];

        $instance = $this->instance;
        $instance__count = $this->internalCall($instance, self::METHOD_COUNT);

        $actual = $instance__count($words);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::filter
     */
    public function testFilter(): void
    {
        $words = ['Word', 'word', 'Blacklisted', 'blacklisted'];
        $blacklist = ['blacklisted'];
        $expected = ['Word', 'word'];

        $instance = $this->instance;
        $instance__filter = $this->internalCall($instance, self::METHOD_FILTER);

        $actual = $instance__filter($words, $blacklist);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideSort
     * @covers ::sort
     *
     * @param tWordCount $expected
     * @param tWordCount $wordCounts
     */
    public function testSort(array $expected, array $wordCounts, int $order): void
    {
        $string = '<string>';
        $instance = $this->mockInstance([self::METHOD_EXTRACT]);
        $this->mockInstanceExtract($instance, $string, $wordCounts);

        $actual = $instance->extractSorted($string, $order);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{tWordCount,tWordCount,int}>
     */
    public function provideSort(): array
    {
        $wordCounts = ['word' => 1, 'another' => 2, 'yetAnother' => 1];

        return [
            'descending' => [['another' => 2, 'word' => 1, 'yetAnother' => 1], $wordCounts, SORT_DESC],
            'ascending' => [['word' => 1, 'yetAnother' => 1, 'another' => 2], $wordCounts, SORT_ASC],
        ];
    }

    protected function instanceClass(): string
    {
        return KeywordExtractor::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->extractor];
    }

    /**
     * @param tWordCount $results
     *
     * @return InvocationMocker<KeywordExtractor>
     */
    private function mockInstanceExtract(
        MockObject $instance,
        string $string,
        array $results,
    ): InvocationMocker {
        return $instance->expects($this->once())
            ->method(self::METHOD_EXTRACT)
            ->with($string)
            ->willReturn($results);
    }
}

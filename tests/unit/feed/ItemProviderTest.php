<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\feed;

use antichris\rssReader\feed\Item;
use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\tests\_support\misc\ProviderAbstractTest;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\feed\ItemProvider
 *
 * @extends ProviderAbstractTest<ItemProvider>
 */
final class ItemProviderTest extends ProviderAbstractTest
{
    public function provideGetClassName(): array
    {
        return [[Item::class]];
    }

    protected function instanceClass(): string
    {
        return ItemProvider::class;
    }
}

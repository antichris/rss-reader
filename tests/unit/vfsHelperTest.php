<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// TODO Is this the right place for a test-support component test?

namespace antichris\rssReader\tests\unit;

use antichris\rssReader\tests\_support\vfsHelper;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversDefaultClass \antichris\rssReader\tests\_support\vfsHelper
 */
final class vfsHelperTest extends TestCase
{
    private const UNSPECFIED = '<UNSPECIFIED>';

    /**
     * vfsHelper::create() nameSeed fallback to caller name from backtrace.
     *
     * @dataProvider provideCreateNameSeed
     */
    public function testCreateNameSeed(
        string $expected,
        ?string $nameSeed = self::UNSPECFIED,
    ): void {
        $args = [['foo' => 'bar']];
        if ($nameSeed !== self::UNSPECFIED) {
            $args['nameSeed'] = $nameSeed ?? __METHOD__;
        }
        $vfs = vfsHelper::create(...$args);

        $actual = $vfs->getName();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{string,1?:?string}>
     */
    public function provideCreateNameSeed(): array
    {
        return [
            'default fallback' => ['gWDCZ68M1TKkHKBuUEwEwA'],
            'empty string' => ['gWDCZ68M1TKkHKBuUEwEwA', ''],
            // Special case `null` `$expected` becomes `__METHOD__` in the test.
            "caller's __METHOD__" => ['gWDCZ68M1TKkHKBuUEwEwA', null],
            'arbitrary string' => ['XrY7u-Ae7tCTyyK7j1rNww', 'hello world'],
        ];
    }

    /**
     * @covers ::addNewFile
     */
    public function testAddNewFile(): void
    {
        $dir = vfsHelper::create([]);
        $file = vfsHelper::addNewFile($dir, 'file');

        $this->assertStringContainsString($dir->path(), $file->path());
    }
}

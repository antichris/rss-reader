<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\profiling\concrete;

use antichris\rssReader\misc\profiling\concrete\Sample;
use antichris\rssReader\misc\profiling\concrete\SampleProvider;
use antichris\rssReader\tests\_support\misc\ProviderAbstractTest;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Stub;
use yii\log\Logger;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\profiling\concrete\SampleProvider
 *
 * @method SampleProvider&MockObject mockInstance(string[] $mockedMethods = [])
 *
 * @extends ProviderAbstractTest<SampleProvider>
 */
final class SampleProviderTest extends ProviderAbstractTest
{
    /** @var Logger&Stub */
    private Stub $logger;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->logger = $this->createStub(Logger::class);
        parent::setUp();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideCreate
     * @covers ::create
     */
    public function testCreate(?string $category): void
    {
        $token = '<token>';

        $providerCategroy = $category !== null
            ? $category
            : 'application';

        $instance = $this->mockInstance(['provide']);
        $sample = $this->createStub(Sample::class);
        $instance->expects($this->once())
            ->method('provide')
            ->with([$this->logger, $token, $providerCategroy])
            ->willReturn($sample);

        $expected = $sample;

        $actual = $category !== null
            ? $instance->create($token, $category)
            : $instance->create($token);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{?string}>
     */
    public function provideCreate(): array
    {
        return [
            'no category' => [null],
            'category' => ['<category>'],
        ];
    }

    public function provideGetClassName(): array
    {
        return [[Sample::class]];
    }

    protected function instanceClass(): string
    {
        return SampleProvider::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [
            parent::instanceCtorArgs()[0],
            $this->logger,
        ];
    }
}

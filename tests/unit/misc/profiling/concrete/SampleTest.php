<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\profiling\concrete;

use antichris\rssReader\misc\profiling\concrete\Sample;
use antichris\rssReader\misc\profiling\SampleInterface;
use antichris\rssReader\tests\_support\TestCase;
use Exception;
use LogicException;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;
use yii\log\Logger;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\profiling\concrete\Sample
 *
 * @method Sample&MockObject mockInstance(string[] $mockedMethods = [])
 *
 * @extends TestCase<Sample>
 */
final class SampleTest extends TestCase
{
    public const TEST_CATEGORY = '<category>';

    private Sample $instance;
    /** @var MockObject&Logger */
    private MockObject $logger;
    private string $token;
    private string $category;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->logger = $this->createMock(Logger::class);
        $this->token = '<token>';
        $this->category = self::TEST_CATEGORY;
        $this->instance = $this->createInstance();
    }

    /**
     * @dataProvider provideConstruct
     * @covers ::__construct
     */
    public function testConstruct(?string $category): void
    {
        $token = '<token>';

        $ctorArgs = [$this->logger, $token];

        if ($category !== null) {
            $expectCategory = $category;
            $ctorArgs[] = $category;
        } else {
            $expectCategory = 'application';
        }

        /** @var Sample $instance */
        $instance = new ($this->instanceClass())(...$ctorArgs);

        $this->assertEquals($expectCategory, $instance->category);
    }

    /**
     * @return array<string,array{?string}>
     */
    public function provideConstruct(): array
    {
        return [
            'no category' => [null],
            'empty category string' => [''],
            'category' => ['<category>'],
        ];
    }

    /**
     * @dataProvider provideDestruct
     * @covers ::__destruct
     */
    public function testDestruct(bool $active): void
    {
        $instance = $this->mockInstance(['end']);

        $expectEnd = $active
            ? $this->once()
            : $this->never();
        $instance->expects($expectEnd)->method('end');

        if ($active) {
            $instance->begin();
        }

        $instance->__destruct();
    }

    /**
     * @return array<string,array{bool}>
     */
    public function provideDestruct(): array
    {
        return [
            'active' => [true],
            'not active' => [false],
        ];
    }

    /**
     * @dataProvider provideBegin
     * @covers ::begin
     */
    public function testBegin(bool $active, ?Exception $exception = null): void
    {
        $instance = $this->instance;

        if ($active) {
            $instance->begin();
            $expectLog = $this->never();
        } else {
            $expectLog = $this->once();
        }
        $this->loggerExpectsLog($expectLog, Logger::LEVEL_PROFILE_BEGIN);

        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $result = $instance->begin();

        $this->assertInstanceOf(SampleInterface::class, $result);
    }

    /**
     * @return array<string,array{bool,1?:Exception}>
     */
    public function provideBegin(): array
    {
        return [
            'active' => [true, new LogicException('Sample already active')],
            'not active' => [false],
        ];
    }

    /**
     * @dataProvider provideNext
     * @covers ::next
     */
    public function testNext(string $expectedCategory, ?string $category): void
    {
        $token = "different from {$this->token}";

        $instance = $this->instance;
        $instance->begin();
        $this->logger->expects($this->exactly(2))
            ->method('log')
            ->withConsecutive(
                [$this->token, Logger::LEVEL_PROFILE_END, $this->category],
                [$token, Logger::LEVEL_PROFILE_BEGIN, $expectedCategory],
            );

        $result = $instance->next($token, $category);

        $this->assertInstanceOf(SampleInterface::class, $result);
    }

    /**
     * @return array<string,array{string,?string}>
     */
    public function provideNext(): array
    {
        $category = '<new-category>';

        return [
            'no category' => [self::TEST_CATEGORY, null],
            'empty category string' => ['', ''],
            'new category' => [$category, $category],
        ];
    }

    /**
     * @dataProvider provideEnd
     * @covers ::end
     */
    public function testEnd(bool $active, ?Exception $exception = null): void
    {
        $instance = $this->instance;

        if ($active) {
            $instance->begin();
            $expectLog = $this->once();
        } else {
            $expectLog = $this->never();
        }
        $this->loggerExpectsLog($expectLog, Logger::LEVEL_PROFILE_END);

        if ($exception) {
            $this->expectExceptionObject($exception);
        }

        $instance->end();
    }

    /**
     * @return array<string,array{bool,1?:Exception}>
     */
    public function provideEnd(): array
    {
        return [
            'active' => [true],
            'not active' => [false, new LogicException('Sample not active')],
        ];
    }

    protected function instanceClass(): string
    {
        return Sample::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->logger, $this->token, $this->category];
    }

    /**
     * @return InvocationMocker<Logger>
     */
    private function loggerExpectsLog(
        InvocationOrder $invocationRule,
        int $level,
    ): InvocationMocker {
        return $this->logger->expects($invocationRule)
            ->method('log')
            ->with($this->token, $level, $this->category);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\profiling\concrete;

use antichris\rssReader\misc\profiling\concrete\Profiler;
use antichris\rssReader\misc\profiling\concrete\Sample;
use antichris\rssReader\misc\profiling\concrete\SampleProvider;
use antichris\rssReader\tests\_support\TestCase;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use RuntimeException;
use Throwable;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\profiling\concrete\Profiler
 *
 * @method Profiler&MockObject mockInstance(string[] $mockedMethods = [])
 *
 * @extends TestCase<Profiler>
 */
final class ProfilerTest extends TestCase
{
    private Profiler $instance;
    /** @var SampleProvider&MockObject */
    private MockObject $provider;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->provider = $this->createMock(SampleProvider::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideStart
     * @covers ::start
     */
    public function testStart(?string $category): void
    {
        $token = '<token>';

        $sample = $this->mockSampleBeginOnce();
        $this->provider->expects($this->once())
            ->method('create')
            ->with($token, $this->fallbackCategory($category))
            ->willReturn($sample);
        $instance = $this->instance;

        $expected = $sample;

        $actual = $category !== null
            ? $instance->start($token, $category)
            : $instance->start($token);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<array{?string}>
     */
    public function provideStart(): array
    {
        return [
            'no category' => [null],
            'empty category string' => [''],
            'category' => ['<category>'],
        ];
    }

    /**
     * @dataProvider provideWrap
     * @covers ::wrap
     */
    public function testWrap(
        mixed $expected,
        callable $fn,
        ?Throwable $throwable = null,
        ?string $category = null,
    ): void {
        $token = '<token>';

        $sample = $this->mockSampleBeginOnce();
        $sample->expects($this->once())->method('end');
        $this->provider->expects($this->once())
            ->method('create')
            ->with($token, $this->fallbackCategory($category))
            ->willReturn($sample);
        $instance = $this->instance;

        if ($throwable) {
            $this->expectException(RuntimeException::class);
            $this->expectExceptionMessage(
                "Exception during profiling: {$throwable->getMessage()}",
            );
        }

        $actual = $category !== null
            ? $instance->wrap($token, $fn, $category)
            : $instance->wrap($token, $fn);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{mixed,callable,2?:?Throwable,3?:string}>
     */
    public function provideWrap(): array
    {
        $bool = true;
        $boolFn = fn () => $bool;

        $exception = new Exception('foo');
        $throwFn = fn () => throw $exception;

        $string = 'hello';
        $stringFn = fn () => $string;

        return [
            'exception' => ['(nothing)', $throwFn, $exception],
            'no category' => [$bool, $boolFn],
            'category' => [$bool, $boolFn, null, '<category>'],
            'string return' => [$string, $stringFn],
        ];
    }

    protected function instanceClass(): string
    {
        return Profiler::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->provider];
    }

    /**
     * @return Sample&MockObject
     */
    private function mockSampleBeginOnce(): MockObject
    {
        $sample = $this->mockSample();
        $sample->expects($this->once())
            ->method('begin')
            ->willReturnSelf();

        return $sample;
    }

    /**
     * @return Sample&MockObject
     */
    private function mockSample(): MockObject
    {
        return $this->createMock(Sample::class);
    }

    private function fallbackCategory(?string $category = null): string
    {
        return $category ?? Profiler::DEFAULT_CATEGORY;
    }
}

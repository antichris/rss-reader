<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\activeRepository\CacheRepositoryAdapter;
use antichris\rssReader\misc\activeRepository\CacheRepositoryAdapterProvider;
use antichris\rssReader\tests\_support\misc\ProviderAbstractTest;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\CacheRepositoryAdapterProvider
 *
 * @extends ProviderAbstractTest<CacheRepositoryAdapterProvider<ActiveRecord>>
 */
final class CacheRepositoryAdapterProviderTest extends ProviderAbstractTest
{
    public function provideGetClassName(): array
    {
        return [[CacheRepositoryAdapter::class]];
    }

    protected function instanceClass(): string
    {
        return CacheRepositoryAdapterProvider::class;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\AbstractProvider;
use antichris\rssReader\misc\activeRepository\AbstractActiveRecordProvider;
use antichris\rssReader\misc\activeRepository\ActiveRecordMapper;
use antichris\rssReader\tests\_support\MockHelperTrait;
use antichris\rssReader\tests\_support\TestCase;
use antichris\rssReader\user\UserEntity;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\ActiveRecordMapper
 *
 * @method ActiveRecordMapper<UserEntity,ActiveRecord>            createInstance()                           Constructs the instance under test.
 * @method ActiveRecordMapper<UserEntity,ActiveRecord>&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<ActiveRecordMapper<UserEntity,ActiveRecord>>
 */
final class ActiveRecordMapperTest extends TestCase
{
    use MockHelperTrait;
    /**
     * @var UserEntity&MockObject
     */
    private $entity;
    /**
     * @var AbstractProvider<UserEntity>&MockObject
     */
    private $entityProvider;
    /**
     * @var AbstractActiveRecordProvider<ActiveRecord>&MockObject
     */
    private $arProvider;
    /**
     * @var ActiveRecordMapper<UserEntity,ActiveRecord>
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->entity = $this->createMock(UserEntity::class);
        $this->entityProvider = $this->createMock(AbstractProvider::class);
        $this->arProvider = $this->createMock(AbstractActiveRecordProvider::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(ActiveRecordMapper::class, $instance);
    }

    /**
     * @covers ::mapToAr
     */
    public function testMapEntityToActiveRecord(): void
    {
        $instance = $this->instance;
        $activeRecord = $this->createPartialMock(ActiveRecord::class, ['__set']);
        $expected = $activeRecord;

        $this->helpMock()->methodVoidOnce($activeRecord, '__set', 'attributes', (array) $this->entity);

        $actual = $instance->mapToAr($this->entity, $activeRecord);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::mapToEntity
     */
    public function testMapActiveRecordToEntity(): void
    {
        $attributes = [
            'id' => 13,
            'email' => '<email>',
            'password_hash' => '<hash>',
        ];
        $activeRecord = $this->createPartialMock(ActiveRecord::class, ['__get']);
        $expected = $this->entity;

        $this->helpMock()->methodOnce($activeRecord, '__get', $attributes, 'attributes');
        $instance = $this->instance;

        $actual = $instance->mapToEntity($activeRecord, $this->entity);

        $this->assertEquals($expected, $actual);
        foreach ($attributes as $attribute => $value) {
            $this->assertEquals($value, $actual->{$attribute});
        }
    }

    /**
     * @covers ::mapToNewAr
     */
    public function testMapEntityToNewActiveRecord(): void
    {
        $activeRecord = $this->mockActiveRecord();
        $expected = $activeRecord;

        /** @var ActiveRecordMapper<UserEntity,ActiveRecord>&MockObject $instance */
        $instance = $this->mockInstance(['mapToAr']);
        $this->mockProviderProvide($this->arProvider, $activeRecord);
        $this->helpMock()->methodOnce($instance, 'mapToAr', $activeRecord, $this->entity);

        $actual = $instance->mapToNewAr($this->entity);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::mapToNewEntity
     */
    public function testMapActiveRecordToNewEntity(): void
    {
        $activeRecord = $this->mockActiveRecord();
        $expected = $this->entity;

        /** @var ActiveRecordMapper<UserEntity,ActiveRecord>&MockObject $instance */
        $instance = $this->mockInstance(['mapToEntity']);
        $this->mockProviderProvide($this->entityProvider, $this->entity);
        $this->helpMock()->methodOnce($instance, 'mapToEntity', $this->entity, $activeRecord);

        $actual = $instance->mapToNewEntity($activeRecord);

        $this->assertEquals($expected, $actual);
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceClass(): string
    {
        return ActiveRecordMapper::class;
    }

    /**
     * {@inheritdoc}
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->entityProvider, $this->arProvider];
    }

    /**
     * @return MockObject&ActiveRecord
     */
    private function mockActiveRecord()
    {
        return $this->createMock(ActiveRecord::class);
    }

    /**
     * @return InvocationMocker<AbstractProvider<UserEntity>>
     */
    private function mockProviderProvide(MockObject $provider, object $result): InvocationMocker
    {
        return $provider->expects($this->once())
            ->method('provide')
            ->willReturn($result);
    }
}

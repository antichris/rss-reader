<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\activeRepository\ActiveRecordCache;
use antichris\rssReader\misc\activeRepository\CacheRepositoryAdapter;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\CacheRepositoryAdapter
 *
 * @method CacheRepositoryAdapter<ActiveRecord>            createInstance()                           Constructs the instance under test.
 * @method CacheRepositoryAdapter<ActiveRecord>&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<CacheRepositoryAdapter<ActiveRecord>>
 */
final class CacheRepositoryAdapterTest extends TestCase
{
    private const METHOD_FETCH = 'fetch';
    private const METHOD_GET_ID_FROM_CONDITION = 'getIdFromCondition';

    /**
     * @var ActiveRecordCache<ActiveRecord>&MockObject
     */
    private $cache;
    /**
     * @var CacheRepositoryAdapter<ActiveRecord>
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->cache = $this->createMock(ActiveRecordCache::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::store
     */
    public function testStore(): void
    {
        $activeRecord = $this->mockActiveRecord();

        $instance = $this->instance;
        $this->cache->expects($this->once())->method('store')->with($activeRecord);

        $instance->store($activeRecord);
    }

    /**
     * @dataProvider provideFetch
     * @covers ::fetch
     */
    public function testFetch(?int $id): void
    {
        if (null === $id) {
            $expected = null;
            $expectFetch = $this->never();
        } else {
            $expected = $this->mockActiveRecord();
            $expectFetch = $this->once();
        }

        $instance = $this->instance;
        $this->cache->expects($expectFetch)->method('fetch')->with($id)->willReturn($expected);

        $actual = $instance->fetch($id);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{?int}>
     */
    public function provideFetch(): array
    {
        return [
            'valid ID' => [13],
            'no ID' => [null],
        ];
    }

    /**
     * @covers ::fetchByCondition
     */
    public function testFetchByCondition(): void
    {
        $condition = '<condition>';
        $id = 13;
        $expected = $this->mockActiveRecord();

        $instance = $this->mockInstance([self::METHOD_FETCH, self::METHOD_GET_ID_FROM_CONDITION]);
        $instance->expects($this->once())
            ->method(self::METHOD_GET_ID_FROM_CONDITION)
            ->with($condition)
            ->willReturn($id);
        $instance->expects($this->once())->method(self::METHOD_FETCH)->with($id)->willReturn($expected);

        $actual = $instance->fetchByCondition($condition);

        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider provideGetIdFromCondition
     * @covers ::getIdFromCondition
     *
     * @param ?tActiveRCondition $condition
     */
    public function testGetIdFromCondition(?int $expected, $condition): void
    {
        $instance = $this->instance;
        $instance__getIdFromCondition = $this->internalCall($instance, self::METHOD_GET_ID_FROM_CONDITION);

        $actual = $instance__getIdFromCondition($condition);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{?int,?tActiveRCondition}>
     */
    public function provideGetIdFromCondition(): array
    {
        return [
            'int ID' => [13, 13],
            'numeric ID' => [13, '13'],
            'condition has ID' => [13, ['id' => 13]],
            "condition doesn't have ID" => [null, []],
            'bad condition' => [null, null],
        ];
    }

    protected function instanceClass(): string
    {
        return CacheRepositoryAdapter::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [$this->cache];
    }

    /**
     * @return MockObject&ActiveRecord
     */
    private function mockActiveRecord()
    {
        return $this->createMock(ActiveRecord::class);
    }
}

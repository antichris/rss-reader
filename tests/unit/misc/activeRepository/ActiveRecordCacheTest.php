<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\activeRepository\ActiveRecordCache;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\ActiveRecordCache
 */
final class ActiveRecordCacheTest extends TestCase
{
    private const METHOD_HAS = 'has';
    private const METHOD_GET = 'get';
    /**
     * @var ActiveRecordCache<ActiveRecord>
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->instance = new ActiveRecordCache();
    }

    /**
     * @dataProvider provideHas
     * @covers ::has
     */
    public function testHas(bool $expected, int $id): void
    {
        $instance = $this->instance;
        $activeRecord = $this->mockActiveRecord(13);
        $instance->store($activeRecord);

        $actual = $instance->has($id);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{bool,int}>
     */
    public function provideHas(): array
    {
        return [
            'existing' => [true, 13],
            'not existing' => [false, 12],
        ];
    }

    /**
     * @covers ::store
     */
    public function testStore(): void
    {
        $id = 13;
        $instance = $this->instance;
        $activeRecord = $this->mockActiveRecord($id);

        $instance->store($activeRecord);

        $this->assertSame($activeRecord, $instance->fetch($id));
    }

    /**
     * @covers ::get
     */
    public function testGet(): void
    {
        $id = 13;
        $instance = $this->mockInstance([self::METHOD_HAS]);
        $this->mockInstanceHas($instance, $id, true);
        $activeRecord = $this->mockActiveRecord($id);
        $instance->store($activeRecord);

        $actual = $instance->fetch($id);

        $this->assertSame($activeRecord, $actual);
    }

    /**
     * @dataProvider provideHas
     * @covers ::fetch
     */
    public function testFetch(bool $has, int $id): void
    {
        $instance = $this->mockInstance([self::METHOD_HAS, self::METHOD_GET]);
        $this->mockInstanceHas($instance, $id, $has);
        if ($has) {
            $activeRecord = $this->mockActiveRecord($id);
            $instance->expects($this->once())->method(self::METHOD_GET)->with($id)->willReturn($activeRecord);
            $instance->store($activeRecord);
            $expected = $activeRecord;
        } else {
            $expected = null;
        }

        $actual = $instance->fetch($id);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return ActiveRecord&MockObject
     */
    private function mockActiveRecord(int $id): MockObject
    {
        $activeRecord = $this->createMock(ActiveRecord::class);
        $activeRecord->expects($this->once())
            ->method('getAttribute')
            ->with('id')
            ->willReturn($id);

        return $activeRecord;
    }

    /**
     * @param string[] $methods
     *
     * @return ActiveRecordCache<ActiveRecord>&MockObject
     */
    private function mockInstance(array $methods = []): MockObject
    {
        return $this->createPartialMock(ActiveRecordCache::class, $methods);
    }

    /**
     * @return InvocationMocker<ActiveRecordCache<ActiveRecord>>
     */
    private function mockInstanceHas(MockObject $instance, int $id, bool $result): InvocationMocker
    {
        return $instance->expects($this->once())->method(self::METHOD_HAS)->with($id)->willReturn($result);
    }
}

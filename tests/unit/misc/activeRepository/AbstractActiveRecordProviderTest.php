<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\activeRepository\AbstractActiveRecordProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use yii\db\ActiveRecord;
use yii\di\Container;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\AbstractActiveRecordProvider
 */
final class AbstractActiveRecordProviderTest extends TestCase
{
    /**
     * @var AbstractActiveRecordProvider<ActiveRecord>&MockObject
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $container = $this->createMock(Container::class);
        $this->instance = $this->getMockBuilder(AbstractActiveRecordProvider::class)
            ->setConstructorArgs([$container])
            ->getMockForAbstractClass();
    }

    /**
     * @covers ::getModelClass
     */
    public function testGetModelClass(): void
    {
        $className = '<className>';
        $expected = $className;

        $instance = $this->instance;
        $instance->expects($this->once())->method('getClassName')->willReturn($className);

        $actual = $instance->getModelClass();

        $this->assertEquals($expected, $actual);
    }
}

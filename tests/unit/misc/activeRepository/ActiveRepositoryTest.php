<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\activeRepository\AbstractActiveRecordProvider;
use antichris\rssReader\misc\activeRepository\ActiveRecordCache;
use antichris\rssReader\misc\activeRepository\ActiveRecordMapper;
use antichris\rssReader\misc\activeRepository\ActiveRepository;
use antichris\rssReader\misc\activeRepository\CacheRepositoryAdapter;
use antichris\rssReader\misc\activeRepository\CacheRepositoryAdapterProvider;
use antichris\rssReader\tests\_support\TestCase;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;
use stdClass;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\ActiveRepository
 *
 * @method ActiveRepository<object,ActiveRecord>            createInstance()                           Constructs the instance under test.
 * @method ActiveRepository<object,ActiveRecord>&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock of the instance under test.
 *
 * @extends TestCase<ActiveRepository<object,ActiveRecord>>
 */
final class ActiveRepositoryTest extends TestCase
{
    private const METHOD_SAVE_RECORD = 'saveRecord';
    private const METHOD_FIND_ONE_RECORD = 'findOneRecord';
    private const METHOD_CAHCE_ADAPTER_STORE = 'store';

    /**
     * @var ActiveRecordMapper<object,ActiveRecord>&MockObject
     */
    private $mapper;
    /**
     * @var ActiveRecordCache<ActiveRecord>&MockObject
     */
    private $cache;
    /**
     * @var CacheRepositoryAdapter<ActiveRecord>&MockObject
     */
    private $cacheAdapter;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->mapper = $this->createMock(ActiveRecordMapper::class);
        $this->cache = $this->createMock(ActiveRecordCache::class);
        $this->cacheAdapter = $this->createMock(CacheRepositoryAdapter::class);
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @dataProvider provideSave
     * @covers ::save
     *
     * @param stdClass $entity
     * @param string[] $attributeNames
     */
    public function testSave(bool $isSaved, bool $isCached, $entity, bool $runValidation = true, ?array $attributeNames = null): void
    {
        $record = $this->mockActiveRecord();
        if ($isCached) {
            $cachedRecord = $record;
            $expectMapToNewAr = $this->never();
            $expectMapToAr = $this->once();
        } else {
            $cachedRecord = null;
            $expectMapToNewAr = $this->once();
            $expectMapToAr = $this->never();
        }
        $expectStore = $isSaved
            ? $this->once()
            : $this->never();

        $instance = $this->mockInstance([self::METHOD_SAVE_RECORD]);
        $this->cacheAdapter->expects($this->once())
            ->method('fetch')
            ->with($entity->id)
            ->willReturn($cachedRecord);
        $this->mapper->expects($expectMapToNewAr)
            ->method('mapToNewAr')
            ->with($entity)
            ->willReturn($record);
        $this->mapper->expects($expectMapToAr)
            ->method('mapToAr')
            ->with($entity, $record)
            ->willReturn($record);
        $instance->expects($this->once())
            ->method(self::METHOD_SAVE_RECORD)
            ->with($record, $runValidation, $attributeNames)
            ->willReturn($isSaved);
        $this->mockCacheAdapterStore($expectStore, $record);

        $actual = $instance->save($entity, $runValidation, $attributeNames);

        $this->assertSame($isSaved, $actual);
    }

    /**
     * @return array<string,array{bool,bool,object}>
     */
    public function provideSave(): array
    {
        $entity = new stdClass();
        $entity->id = 13;

        return [
            'saved cached' => [true, true, $entity],
            'saved uncached' => [true, false, $entity],
            'not saved cached' => [false, true, $entity],
            'not saved uncached' => [false, false, $entity],
        ];
    }

    /**
     * @dataProvider provideFindOne
     * @covers ::findOne
     */
    public function testFindOne(?object $expected, bool $isCached, bool $isFound): void
    {
        $condition = 13;
        $record = $this->mockActiveRecord();
        if ($isCached) {
            $cachedRecord = $record;
            $expectFindOneRecord = $this->never();
        } else {
            $cachedRecord = null;
            $expectFindOneRecord = $this->once();
        }
        if ($isFound) {
            $foundRecord = $record;
            $expectStore = $this->once();
            $expectMapToNewEntity = $this->once();
        } else {
            $foundRecord = null;
            $expectStore = $this->never();
            $expectMapToNewEntity = $this->never();
        }

        $instance = $this->mockInstance([self::METHOD_FIND_ONE_RECORD]);
        $this->cacheAdapter->expects($this->once())
            ->method('fetchByCondition')
            ->with($condition)
            ->willReturn($cachedRecord);
        $instance->expects($expectFindOneRecord)
            ->method(self::METHOD_FIND_ONE_RECORD)
            ->with($condition)
            ->willReturn($foundRecord);
        $this->mockCacheAdapterStore($expectStore, $record);
        $mapToNewExpectation = $this->mapper->expects($expectMapToNewEntity)
            ->method('mapToNewEntity');
        if ($isFound) {
            $mapToNewExpectation->with($record)->willReturn($expected);
        }

        $actual = $instance->findOne($condition);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array<string,array{?object,bool,bool}>
     */
    public function provideFindOne(): array
    {
        $entity = new stdClass();
        $entity->id = 13;

        return [
            'found cached' => [$entity, true, true],
            'found uncached' => [$entity, false, true],
            'not found uncached' => [null, false, false],
        ];
    }

    protected function instanceClass(): string
    {
        return ActiveRepository::class;
    }

    protected function instanceCtorArgs(): array
    {
        return [
            $this->createMock(AbstractActiveRecordProvider::class),
            $this->mapper,
            $this->cache,
            $this->mockCacheAdapterProvider($this->cache, $this->cacheAdapter),
        ];
    }

    /**
     * @return MockObject&ActiveRecord
     */
    private function mockActiveRecord()
    {
        return $this->createMock(ActiveRecord::class);
    }

    /**
     * @return CacheRepositoryAdapterProvider<ActiveRecord>&MockObject
     */
    private function mockCacheAdapterProvider(MockObject $cache, MockObject $cacheAdapter): MockObject
    {
        $provider = $this->createMock(CacheRepositoryAdapterProvider::class);
        $provider->expects($this->once())
            ->method('provide')
            ->with([$cache])
            ->willReturn($cacheAdapter);

        return $provider;
    }

    /**
     * @return InvocationMocker<CacheRepositoryAdapter<ActiveRecord>>
     */
    private function mockCacheAdapterStore(InvocationOrder $invocationRule, ActiveRecord $record): InvocationMocker
    {
        return $this->cacheAdapter->expects($invocationRule)
            ->method(self::METHOD_CAHCE_ADAPTER_STORE)
            ->with($record);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc\activeRepository;

use antichris\rssReader\misc\activeRepository\AbstractActiveRecordProvider;
use antichris\rssReader\misc\activeRepository\BaseActiveRepository;
use antichris\rssReader\tests\_support\AbstractTestCase;
use antichris\rssReader\tests\_support\FakeArClass;
use PHPUnit\Framework\MockObject\Builder\InvocationMocker;
use PHPUnit\Framework\MockObject\MockObject;
use yii\db\ActiveRecord;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\activeRepository\BaseActiveRepository
 *
 * @method BaseActiveRepository<ActiveRecord>&MockObject createInstance()                           Creates a mock instance of the abstract class under test.
 * @method BaseActiveRepository<ActiveRecord>&MockObject mockInstance(string[] $mockedMethods = []) Creates a partial mock instance of the abstract class under test.
 *
 * @extends AbstractTestCase<BaseActiveRepository<ActiveRecord>>
 */
final class BaseActiveRepositoryTest extends AbstractTestCase
{
    private const METHOD_GET_MODEL_CLASS = 'getModelClass';

    /**
     * @var AbstractActiveRecordProvider<ActiveRecord>&MockObject
     */
    private $provider;
    /**
     * @var BaseActiveRepository<ActiveRecord>&MockObject
     */
    private $instance;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->provider = $this->createMock(AbstractActiveRecordProvider::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf($this->instanceClass(), $instance);
    }

    /**
     * @covers ::getModelClass
     */
    public function testGetModelClass(): void
    {
        $expected = '<modelClass>';

        $instance = $this->instance;
        $this->provider->expects($this->once())->method('getModelClass')->willReturn($expected);

        $actual = $instance->getModelClass();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::getTableName
     */
    public function testGetTableName(): void
    {
        $modelClass = FakeArClass::class;
        $expected = '<tableName>';

        $instance = $this->mockInstance([self::METHOD_GET_MODEL_CLASS]);
        $this->mockInstanceGetModelClass($instance, $modelClass);
        FakeArClass::$callback = function () use ($expected) {
            return $expected;
        };

        $actual = $instance->getTableName();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @covers ::saveRecord
     */
    public function testSaveRecord(): void
    {
        $record = $this->mockActiveRecord();
        $runValidation = true;
        $attributeNames = null;
        $expected = true;

        $instance = $this->instance;
        $instance__save = $this->internalCall($instance, 'saveRecord');
        $record->expects($this->once())->method('save')->with($runValidation, $attributeNames)->willReturn($expected);

        $actual = $instance__save($record, $runValidation, $attributeNames);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider provideFindOne
     * @covers ::findOneRecord
     */
    public function testFindOneRecord(?ActiveRecord $record): void
    {
        $condition = '<condition>';
        $expected = $record;
        $modelClass = FakeArClass::class;

        $instance = $this->mockInstance([self::METHOD_GET_MODEL_CLASS]);
        $this->mockInstanceGetModelClass($instance, $modelClass);
        $instance__findOne = $this->internalCall($instance, 'findOneRecord');
        FakeArClass::$callback = function ($passedCondition) use ($record, $condition) {
            $this->assertSame($condition, $passedCondition);

            return $record;
        };

        $actual = $instance__findOne($condition);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array<string,array{null|(MockObject&ActiveRecord)}>
     */
    public function provideFindOne(): array
    {
        return [
            'found' => [$this->mockActiveRecord()],
            'not found' => [null],
        ];
    }

    /**
     * @return MockObject[]
     */
    protected function instanceCtorArgs(): array
    {
        return [$this->provider];
    }

    protected function instanceClass(): string
    {
        return BaseActiveRepository::class;
    }

    /**
     * @return MockObject&ActiveRecord
     */
    private function mockActiveRecord()
    {
        return $this->createMock(ActiveRecord::class);
    }

    /**
     * @return InvocationMocker<BaseActiveRepository<ActiveRecord>>
     */
    private function mockInstanceGetModelClass(MockObject $instance, string $modelClass): InvocationMocker
    {
        return $instance->expects($this->once())
            ->method(self::METHOD_GET_MODEL_CLASS)
            ->willReturn($modelClass);
    }
}

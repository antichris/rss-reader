<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\tests\unit\misc;

use antichris\rssReader\misc\AbstractProvider;
use antichris\rssReader\tests\_support\MockHelperTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use yii\di\Container;

/**
 * @internal
 * @coversNothing
 * @coversDefaultClass \antichris\rssReader\misc\AbstractProvider
 */
final class AbstractProviderTest extends TestCase
{
    use MockHelperTrait;
    /**
     * @var AbstractProvider<object>&MockObject
     */
    private $instance;
    /**
     * @var Container&MockObject
     */
    private $container;

    /**
     * @before
     */
    protected function setUp(): void
    {
        $this->container = $this->createMock(Container::class);
        $this->instance = $this->createInstance();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct(): void
    {
        $instance = $this->createInstance();

        $this->assertInstanceOf(AbstractProvider::class, $instance);
    }

    /**
     * @covers ::provide
     */
    public function testProvide(): void
    {
        $className = '<className>';
        $expected = '<expected>';

        $instance = $this->instance;
        $instance->expects($this->once())->method('getClassName')->willReturn($className);
        $this->helpMock()->methodOnce($this->container, 'get', $expected, $className);

        $actual = $instance->provide();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return AbstractProvider<object>&MockObject
     */
    private function createInstance(): AbstractProvider
    {
        return $this->getMockBuilder(AbstractProvider::class)
            ->setConstructorArgs([$this->container])
            ->onlyMethods(['getClassName'])
            ->getMockForAbstractClass();
    }
}

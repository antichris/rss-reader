## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at http://mozilla.org/MPL/2.0/.

.ONESHELL:
MAKEFLAGS += --no-builtin-rules --no-builtin-variables

PROJECT_NAME := rss-reader
DEV_NAME = ${PROJECT_NAME}-dev
DEVOPS_DIR := devops
NGINX_DIR = ${DEVOPS_DIR}/nginx

DF_NAME := Dockerfile
DF = ${DEVOPS_DIR}/${DF_NAME}
DF_DIST := ${DF}.dist

MF := Makefile
MF_ALIASES := ${DEVOPS_DIR}/Makefile.aliases

. :=
SP := ${.} ${.}
define EOL :=


endef


##
## Project initialization
##
init: .env ${DF}
.PHONY: init

.env:
	sed 's/^COOKIE_VALIDATION_KEY=$$/\0$(rand)/' .env.example > "$@"

${DF}: ${DF_DIST}
	if [ -f "$@" ]; then
		if ! diff=$$(diff "$@" "$<"); then
			printf 'diff "%s" "%s"\n%s\n' "$@" "$<" "$$diff"
			printf 'The file "%s" has been recently updated.\n' "$<"
			printf 'Replace "%s" with the new version? [y/n/A] ' "$@"
			read response
		else
			response="Nah, it's just the timestamp, the file's the same."
		fi
	else
		response="Yeppers, the local one ain't there, we need it created."
	fi
	case $$response in
		[Yy]*) cp "$<" "$@";;
		[Nn]*) touch "$@";;             # Make make buzz off.
			*) printf 'Abort\n' && exit 1
	esac

.SILENT: ${DF}


##
## Functions
##
## Return hexadecimal string of bytes from /dev/urandom. ([byteCount=32])
rand = $(shell b=${1};n=$$((b?b:32));w=$$((n*2));\
	od -An -tx1 -N$$n -w$$w /dev/urandom|sed 's/ //g')

## Return Docker Compose file name with an optional suffix. ([nameSuffix=])
composeFile = ${DEVOPS_DIR}/docker-compose${1}.yml
## Return spaced option with value (e.g. " -t foo") if value is non-empty. (option,value)
opt = $(if ${2}, ${1} ${2})

## Return result, if values are equal, else return alternative. (valueA,valueB,result,alternative)
ifeq = $(if $(subst ${1},,${2}),${4},${3})
## Return result, if list has a single value, else return alternative. (list,result,alternative)
ifHasOne = $(call ifeq,$(words ${1}),1,${2},${3})

## Add suffixes to each of the prefixes. (prefixes,suffixes,[separator=/])
combine = $(foreach prefix,${1},$(addprefix ${prefix}$(or ${3},/),${2}))

## Double the dollar signs in string for eval while restoring normal dollars at placeholders.
## (string,[restorablePlaceholder=\$$])
doubleDollars = $(subst [restorableDollar],$$,$(subst $$,$$$$,$(subst $(or ${2},\$$),[restorableDollar],${1})))

## Return recipe wildcard based on service(s) and/or environment(s).
## (recipeSuffix,[services=],[environments=])
recipeWildcard = $(strip $(if ${2},$(call ifHasOne,${2},$(call ifHasOne,${3},${3}/${2}/${1},%/${2}/${1}),%/${1}),\
	$(if ${3},$(call ifHasOne,${3},${3}/${1},%/${1}),${1})))

## [eval] Add target-specific variable. (target,varName,[varValue=],[forceEmpty=])
addSpecificVar = $(if $(or ${3},${4}),${1}: $(strip ${2}) =$(if ${3}, $(call doubleDollars,${3}))${EOL})

## [eval] Add target-specific variable on recipe wildcard.
## (recipeSuffix,[services=],[environments=],varName,[varValue=])
addWcSpecificVar = $(call addSpecificVar,$(call recipeWildcard,${1},${2},${3}),${4},${5})

## [eval] Add command parameter variables on recipe wildcard.
## (recipeSuffix,[services=],[environments=],[cmd=],[cmdParams=],[subCmd=],[subParams=],[svc=],[params=],[env=])
addCmdVars = $(foreach n,4 5 6 7 8 9 10,$(call addWcSpecificVar,${1},${2},${3},\
	$(word ${n},. . . cmd cmdParams subCmd subParams svc params env),${${n}}))

## Return variable to append recipe name(s) to based on service(s) and/or environment(s).
## ([services=],[environments=])
recipesVar = $(if ${1},$(if ${2},envSvcRecipes,svcRecipes),$(if ${2},envRecipes,recipes))
## Return recipe names incorporating service(s) and/or environment(s).
## (recipeSuffix,[services=],[environments=])
recipeNames = $(strip $(if ${2},$(if ${3},$(call combine,${3},$(call combine,${2},${1})),$(call combine,${2},${1})),\
	$(if ${3},$(call combine,${3},${1}),${1})))

## [eval] Add a recipe.
## (recipeSuffix,[services=],[environments=],[cmd=],[cmdParams=],[subCmd=],[subParams=],[svc=],[params=],[env=])
define addRecipe
$(call addCmdVars,${1},${2},${3},${4},${5},${6},${7},${8},${9},${10})\
$(call recipesVar,${2},${3}) += $(call recipeNames,${1},${2},${3})
endef
## [eval] Add recipe for a Docker Compose command.
## (recipeSuffix,[services=],[environments=],[cmdParams=],[subCmd=],[subParams=],[svc=],[params=],[env=])
addCompose = $(call addRecipe,${1},${2},${3},\$${composeCmd},${4},${5},${6},${7},${8},${9})
## [eval] Add a recipe matching a Docker Compose subcommand name (e.g. build).
## (recipeSuffix,[services=],[environments=],[cmdParams=],[subParams=],[svc=],[params=],[env=])
addComposeSub = $(call addCompose,${1},${2},${3},${4},${1},${5},${6},${7},${8})
## [eval] Add a recipe for the Docker Compose exec subcommand.
## (recipeSuffix,[services=],[environments=],[cmdParams=],[subParams=],[svc=],[params=],[env=])
addComposeExec = $(call addCompose,${1},${2},${3},${4},exec,${5},${6},${7},${8})

## [eval] Define a recipe with prerequisites. (recipePrefix,recipeSuffix,prereqSuffixes)
prereqRule = ${1}/${2}: $(addprefix ${1}/,${3});
## [eval] Add a prerequisite recipe.
define addPrereq ## (recipePrefixes,recipeSuffix,prereqSuffixes,[prereqRule=prereqRule])
$(foreach prefix,${1},$(call $(or ${4},prereqRule),${prefix},${2},${3})${EOL})\
prereqRecipes += $(addsuffix /${2},${1})
endef

## Put words in the same order as wildcards. (wildcards,words)
wcOrder = $(strip $(foreach wc,${1},$(filter ${wc},${2})) $(filter-out ${1},${2}))
## Put words in the same order as prefixes. (prefixes,words)
prefixOrder = $(call wcOrder,$(addsuffix %,${1}),${2})

###

composeCmd = docker-compose --project-directory . -p ${project} -f ${composeFile} -f $(call composeFile,.${env})
dBuildCmd = docker build$(call opt,-f,${dockerFile})$(call opt,-t,${tag})$(call opt,--target,${target}) ${contextDir}


##
## Environments
##
envs := prod dev
envWcs := $(addsuffix /%,${envs}) ## e.g. prod/%

$(eval $(foreach env,${envs},${env} ${env}/% %/${env}: env = ${env}${EOL}))
prod prod/% %/prod: project = ${PROJECT_NAME}
dev dev/% %/dev: project = ${DEV_NAME}

${envs}: init
	@printf '%s\n' "${composeCmd}"

.PHONY: ${envs}


##
## Services
##
## Common to all environments
cmnSvcs := php mysql nginx
## Specific to dev environment
devSvcs := pma
## The above combined
svcs := ${cmnSvcs} ${devSvcs}

envSvcs := $(call combine,${envs},${cmnSvcs}) $(addprefix dev/,${devSvcs}) ## e.g. prod/php e.g. dev/pma
svcWcs := $(addsuffix /%,${svcs}) ## e.g. php/%
devSvcWcs := $(addsuffix /%,${devSvcs}) ## e.g. pma/%
envSvcWcs := $(addsuffix /%,${envSvcs}) ## e.g. prod/php/%
envsAndEnvSvcs := ${envs} ${envSvcs}

$(eval $(foreach svc,${cmnSvcs},$(call addSpecificVar,$(addsuffix /${svc}/%,${envs}),svc,${svc}))) # e.g. prod/php/% dev/php/%: svc = php
$(eval $(foreach svc,${devSvcs},$(call addSpecificVar,dev/${svc}/%,svc,${svc}))) # e.g. dev/pma/%: svc = pma


##
## Recipes
##
envRecipes := ## Recipes that are specific to each environment. (e.g. prod/build)
svcRecipes := ## Recipes that are specific to each service, but common among environments. (e.g. php/build)
envSvcRecipes := ## Specific recipes, not shared among environments or services. (e.g. dev/php/userShell)
prereqRecipes := ## Recipes that alias a sequence of other recipes as prerequisites. (e.g. prod/recreate: prod/down prod/up)
recipes := ## Other generated recipes.

## Docker image build recipes
##
%/image: cmd = ${dBuildCmd}
%/image: dockerFile = ${DF}
%/image: tag    = ${PROJECT_NAME}_${svc}${tagSuffix}
%/image: target = ${env}
%/image: subParams := \# ## HACK "Escapes" the svc, e.g. "docker build (...) . # php".

cmnImgSvcs := nginx php
$(eval $(call addRecipe,image,${cmnImgSvcs},prod))
$(eval $(call addRecipe,image,${cmnImgSvcs} pma,dev))
dev/%: tagSuffix = :${env}

%/php/image: ${DF}
%/php/image: contextDir := .

%/nginx/image: DF := ## None, just use the one in the context dir.
%/nginx/image: contextDir := ${NGINX_DIR}

%/pma/image: DF_NAME   := ${DF_NAME}.pma
%/pma/image: tagSuffix := ## No tag suffix, Docker falls back to ":latest".
%/pma/image: target    := ## None, it is a single stage Dockerfile.
%/pma/image: contextDir := ${DEVOPS_DIR}

## Recipes common to all services in all environments
##
commonSubCmds := build up pause unpause restart stop logs
$(eval $(foreach subCmd,${commonSubCmds},$(call addComposeSub,${subCmd},,${envs})${EOL}))
$(eval $(foreach subCmd,${commonSubCmds},$(call addComposeSub,${subCmd},${svcs})${EOL}))
%/up:   subParams := -d
%/logs: subParams := -f

## Environment recipes
##
## <env>/up
$(addsuffix /up,${envs}): subParams := -d --remove-orphans
## <env>/down
$(eval $(call addComposeSub,down,,${envs}))
## <env>/unvolume
$(eval $(call addRecipe,unvolume,,${envs},vols=$$(docker volume ls -q -f label=com.docker.compose.project=\$${project});\
	[ "$$vols" ] && docker volume rm $$vols))
## <env>/migrate
$(eval $(call addComposeExec,migrate,,${envs},,-uwww-data:\$${group},php,bin/app migrate --interactive 0))
prod/%: group := www-data
dev/%: group := devel

## Service recipes
##
## <env>/<svc>/rm
$(eval $(call addComposeSub,rm,${svcs},,,-f))
## <env>/<svc>/shell
$(eval $(call addComposeExec,shell,${svcs},,,,,sh))
## <env>/mysql/shell
$(call addWcSpecificVar,shell,mysql,${envs},params,bash)
## <env>/mysql/client
$(eval $(call addComposeExec,client,mysql,,,,,sh -c 'exec mysql -u$$MYSQL_USER -p$$MYSQL_PASSWORD $$MYSQL_DATABASE'))
## dev/php/userShell
$(eval $(call addComposeExec,userShell,php,dev,,-u$$(id -u):devel,,docker-php-entrypoint sh))

## Prerequisite recipes
##
## <env>/<svc>/down
$(eval $(call addPrereq,${envSvcs},down,stop rm))
## <env>/reset
$(eval $(call addPrereq,${envs},reset,down unvolume up))
## <env>/recreate & <env>/<svc>/recreate
$(eval $(call addPrereq,${envsAndEnvSvcs},recreate,down up))
## <env>/compose & <env>/<svc>/compose
$(eval $(call addPrereq,${envsAndEnvSvcs},compose,build recreate))

###

## Organize recipes.
##
envWcs := $(call prefixOrder,${envs},${envWcs})
envSvcWcs := $(call wcOrder,${envWcs},${envSvcWcs})
envRecipes := $(call wcOrder,${envWcs},${envRecipes})
svcRecipes := $(call wcOrder,${svcWcs},${svcRecipes})

cmnSvcRecipes := $(filter-out ${devSvcWcs},${svcRecipes})
devSvcRecipes := $(filter ${devSvcWcs},${svcRecipes})

envSvcRecipes := $(call wcOrder,${envSvcWcs}, \
	$(call combine,${envs},${cmnSvcRecipes}) \
	$(addprefix dev/,${devSvcRecipes}) \
	${envSvcRecipes} \
)
envPrereqRecipes := $(call wcOrder,${envWcs},$(filter-out ${envSvcWcs},${prereqRecipes}))
svcPrereqRecipes := $(call wcOrder,${envSvcWcs},$(filter ${envSvcWcs},${prereqRecipes}))

allRecipes := $(strip ${recipes} $(call wcOrder,${envWcs},${envRecipes} ${envPrereqRecipes} \
	$(call wcOrder,${envSvcWcs},${envSvcRecipes} ${svcPrereqRecipes})))
nonPrereqRecipes := $(filter-out ${prereqRecipes},${allRecipes})

###

$(filter-out image/%,${nonPrereqRecipes}): init ## Images have dependencies of their own defined.

##
## Recipe command execution
##
${nonPrereqRecipes}:
	$(strip ${cmd} ${cmdParams} ${subCmd} ${subParams} ${svc} ${params})

.PHONY: ${allRecipes}


##
## Aliases
##
prod/shell: prod/php/shell
dev/shell: dev/php/shell
shell: dev/php/userShell

.PHONY: prod/shell dev/shell shell

## Alias generation
##
${MF_ALIASES}: ${MF}
	EOL=$$(printf '\n.'); EOL=$${EOL%?}
	## Printf to STDERR.
	eprintf() { printf "$$@" >&2; } ## (...output_to_stderr)
	# Return zero exit status if variable (by name) is set.
	is_set() { [ $$(eval echo \$${$${2}$${1}$${3}+.}) ]; } ## (name [prefix [suffix]])
	## Return zero exit status if item is in set separated by sep.
	is_in() {
		local set="$$1" item="$$2" sep="$${3:-"$$EOL"}"
		[ "$$set" ] && (
			[ "$$set" = "$$item" ] \
			|| [ ! "$${set##"$${item}$${sep}"*}" ] \
			|| [ ! "$${set%%*"$${sep}$${item}"}" ] \
			|| [ ! "$${set%%*"$${sep}$${item}$${sep}"*}" ]
		)
	}
	## Append item to set, separated by sep if set has items.
	append() {
		local set="$$1" item="$$2" sep="$${3:-"$$EOL"}"
		if [ "$$set" ]; then
			printf '%s' "$$set"
			[ "$$item" ] && printf '%s' "$$sep"
		fi
		printf '%s\n' "$$item"
	}
	## Output all (except known) permutations of words separated by sep.
	permutate() {
		local words="$$1" sep="$${2:-' '}" known="$$3" beginning ending prefix suffix
		beginning=$${words%"$${sep}"*}
		if [ "$$beginning" = "$$words" ]; then
			! is_in "$$known" "$$words" && printf '%s\n' "$$words"
			return
		fi
		ending=$${words#*"$${sep}"}
		prefix=$${words%%"$${sep}$${ending}"}
		suffix=$${words##"$${beginning}$${sep}"}
		permutate__recurse "$$prefix" "$$sep" "$$ending"
		permutate__recurse "$$suffix" "$$sep" "$$beginning"
	}
	permutate__recurse() {
		## non-local known
		local part="$$1" sep="$$2" rest="$$3" IFS permutation ordering
		while IFS= read -r permutation; do
			for ordering in \
				"$${part}$${sep}$${permutation}" \
				"$${permutation}$${sep}$${part}" \
			; do
				is_in "$$known" "$$ordering" && continue
				printf '%s\n' "$$ordering"
				known=$$(append "$$known" "$$ordering")
			done
		done <<- EOD
			$$(permutate "$$rest" "$$sep")
		EOD
	}
	## Output lines, starting each with the prefix.
	prefixLines() {
		local lines="$$1" prefix="$${2:-'# '}" IFS line
		while IFS= read -r line; do
			printf '%s%s\n' "$$prefix" "$$line"
		done <<- EOD
			$$lines
		EOD
	}
	## Output (multi-line) title wrapped in blank lines, starting each with prefix.
	printTitle() {
		local title="$$1" prefix="$${2:-##}" lines
		printf '%s\n%s\n' "$$prefix" "$$(printSubTitle "$$title" "$$prefix")"
	}
	## Output (multi-line) subtitle suceeded by a blank line, starting each with prefix.
	printSubTitle() {
		local title="$$1" prefix="$${2:-##}" lines
		printf '%s\n%s\n' "$$(prefixLines "$$title" "$${prefix} ")" "$$prefix"
	}
	## Output a block followed by an extra newline.
	printBlock() { printf '%s\n\n' "$$1"; } ## (contents)
	## Output aliases (and collect in shared variable) as permutations of targets.
	genPermutationAliases() {
		## non-local aliases [targetsProcessed]
		local target="$$1" permutations
		if is_set targetsProcessed; then
			# eprintf 'Checking whether "%s" has already been processed\n' "$$target"
			is_in "$$targetsProcessed" "$$target" && continue
			targetsProcessed=$$(append "$$targetsProcessed" "$$target")
		fi
		permutations=$$(permutate "$$target" / "$$target")
		for alias in $$permutations; do
			aliases=$$(append "$$aliases" "$$alias" ' ')
			printf '%s: %s\n' "$$alias" "$$target"
		done
	}
	## Print argument as .PHONY target prerequisites.
	printPhonies() { printf '.PHONY: %s\n' "$$1"; } ## (phonies)
	## Output aliases succeeded by phonies after whole group.
	genAliasesGrouped() {
		local targets="$$1" aliases
		for target in $$targets; do
			genPermutationAliases "$$target"
		done
		printPhonies "$$aliases"
	}
	## Output aliases succeeded by phonies after each target.
	genAliasesSeparate() {
		local targets="$$1" aliases
		for target in $$targets; do
			genPermutationAliases "$$target"
			printPhonies "$$aliases"
			aliases=
		done
	}
	## Output targets matching the regex.
	matchTargets() {
		local regex="$$1" targets="$$2"
		printf '%s\n' "$$targets"|grep -Eo "$$regex"
	}
	## Output title before a group of aliases for targets matched by regex.
	genGroupedTitled() {
		local title="$$1" regex="$$2" targets="$$3"
		printTitle "$${title} aliases"
		# eprintf 'Generating aliases for %s targets\n' "$$title"
		genAliasesGrouped "$$(matchTargets "$$regex" "$$targets")"
	}
	## Output title of env and svc before aliases for env and svc specific targets.
	genServiceAliases() {
		local envTitle="$$1" env="$$2" svc="$$3" svcTitle="$$4" targets="$$5"
		printSubTitle "$${envTitle} $${svcTitle} aliases"
		# eprintf 'Generating aliases for %s environment %s service targets\n' "$$envTitle" "$$svcTitle"
		genAliasesSeparate "$$(matchTargets "^$${env}/$${svc}/$${rxcName}+\$$" "$$targets")"
	}
	## Output env group of target aliases succeeded by environment service target aliases.
	genEnvironmentAliases() {
		local envTitle="$$1" env="$$2" targets="$$3"
		printBlock "$$(genGroupedTitled $$envTitle "^$${env}/$${rxcName}+\$$" "$$targets")"
		printBlock "$$(genServiceAliases $$envTitle $$env php PHP-FPM "$$targets")"
		printBlock "$$(genServiceAliases $$envTitle $$env mysql MySQL "$$targets")"
		printBlock "$$(genServiceAliases $$envTitle $$env nginx NGINX "$$targets")"
		printBlock "$$(genServiceAliases $$envTitle $$env pma "PHPMyAdmin PHP-FPM" "$$targets")"
	}
	genAliases() {
		local targets="$$1" targetsProcessed=
		printBlock "$$(genEnvironmentAliases Production prod "$$targets")"
		printBlock "$$(genEnvironmentAliases Development dev "$$targets")"
	}
	## Extract targets by regex from file.
	extractTargets() {
		local regex="$$1" file="$${2:-${MF}}" IFS targets target
		targets=$$(grep -Eo "$$regex" "$$file")
		while IFS= read -r target; do
			printf '%s\n' "$${target%%:}"
		done <<- EOD
			$$targets
		EOD
	}
	main() {
		local targetRegex="^$(subst ${SP},|,${envs})(/($(subst ${SP},|,${svcs})))?/$${rxcName}+$$"
		local targets="$$(printf '%s\n' '$(subst ','\'',${allRecipes})'|sed -E 's/ /\n/g')"
		targets=$$(append "$$targets" "$$(extractTargets "$${targetRegex}:")")

		printf '%s\n' "$$(genAliases "$$targets")" > $@
	}
	## Name (service or other) regex character class
	rxcName='[-0-9_a-z]'
	main

.SILENT: ${MF_ALIASES}
sinclude ${MF_ALIASES}

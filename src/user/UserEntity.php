<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

/**
 * Represents a user of the application.
 */
class UserEntity extends Identity
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password_hash;
    /**
     * @var string
     */
    public $first_name;
    /**
     * @var string
     */
    public $last_name;
    /**
     * @var string
     */
    public $email_token;
    /**
     * @var int
     */
    public $status;
    /**
     * @var int
     */
    public $created_at;
    /**
     * @var int
     */
    public $updated_at;
}

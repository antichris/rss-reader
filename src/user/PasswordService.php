<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

use yii\base\Security;

/**
 * Handles the assignment and validation of user entity password hashes.
 */
class PasswordService
{
    public function __construct(
        private Security $security,
    ) {
    }

    /**
     * Generate a secure hash from a password and a random salt.
     */
    public function generatePasswordHash(string $password): string
    {
        return $this->security->generatePasswordHash($password);
    }

    /**
     * Return whether user's password matches the given one.
     */
    public function validatePassword(UserEntity $user, string $password): bool
    {
        return $this->security->validatePassword($password, $user->password_hash);
    }

    /**
     * Generate and set the password hash from the given raw password string.
     */
    public function setPassword(UserEntity $user, string $password): UserEntity
    {
        $user->password_hash = $this->generatePasswordHash($password);

        return $user;
    }
}

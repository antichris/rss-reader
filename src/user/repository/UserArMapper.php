<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\repository;

use antichris\rssReader\misc\activeRepository\ActiveRecordMapper;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserEntityProvider;

/**
 * Maps data between user ActiveRecord and UserEntity instances.
 *
 * @method UserAr     mapToAr(UserEntity $entity, UserAr $activeRecord)     Maps data from a user entity to an existing ActiveRecord instance.
 * @method UserEntity mapToEntity(UserAr $activeRecord, UserEntity $entity) Maps data from an ActiveRecord instance to an existing user entity.
 * @method UserAr     mapToNewAr(UserEntity $entity)                        Maps data from a user entity to a new ActiveRecord instance.
 * @method UserEntity mapToNewEntity(UserAr $activeRecord)                  Maps data from a user ActiveRecord to a new entity instance.
 *
 * @extends ActiveRecordMapper<UserEntity,UserAr>
 */
class UserArMapper extends ActiveRecordMapper
{
    public function __construct(UserEntityProvider $entityProvider, UserArProvider $arProvider)
    {
        parent::__construct($entityProvider, $arProvider);
    }
}

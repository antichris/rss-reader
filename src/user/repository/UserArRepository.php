<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\repository;

use antichris\rssReader\misc\activeRepository\ActiveRecordCache;
use antichris\rssReader\misc\activeRepository\ActiveRepository;
use antichris\rssReader\misc\activeRepository\ActiveRepositoryProvider;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserRepositoryInterface;
use antichris\rssReader\user\UserStatusService;
use RangeException;

/**
 * Implements user entity retrieval and storage via ActiveRecord.
 */
class UserArRepository implements UserRepositoryInterface
{
    /**
     * @var ActiveRepository<UserEntity,UserAr>
     */
    private $repo;

    /**
     * @param ActiveRepositoryProvider<UserEntity,UserAr> $repoProvider
     * @param ActiveRecordCache<UserAr>                   $cache
     */
    public function __construct(
        ActiveRepositoryProvider $repoProvider,
        UserArProvider $provider,
        UserArMapper $mapper,
        ActiveRecordCache $cache,
        private UserStatusService $statusService,
    ) {
        $this->repo = $repoProvider->provide([$provider, $mapper, $cache]);
    }

    /**
     * {@inheritdoc}
     */
    public function findOne($condition): ?UserEntity
    {
        return $this->getRepo()->findOne($condition);
    }

    /**
     * {@inheritdoc}
     */
    public function save(UserEntity $entity, bool $runValidation = true, ?array $attributeNames = null): bool
    {
        return $this->getRepo()->save($entity, $runValidation, $attributeNames);
    }

    /**
     * {@inheritdoc}
     */
    public function findIdentity(int $id): ?UserEntity
    {
        return $this->findOne([
            'id' => $id,
            'status' => $this->statusService->getActive(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmailToken(string $token): ?UserEntity
    {
        return $this->findOne([
            'email_token' => $token,
            'status' => $this->statusService->getInactive(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail(string $email, ?int $status = null): ?UserEntity
    {
        $condition = ['email' => $email];

        if ($status !== null) {
            if (!$this->statusService->isValidValue($status)) {
                throw new RangeException(
                    "Not a valid user status: {$status}",
                );
            }
            $condition['status'] = $status;
        }

        return $this->findOne($condition);
    }

    /**
     * Returns the name of the ActiveRecord class managed by this repository.
     */
    public function getModelClass(): string
    {
        return $this->getRepo()->getModelClass();
    }

    /**
     * Returns the table name of the ActiveRecord managed by this repository.
     */
    public function getTableName(): string
    {
        return $this->getRepo()->getTableName();
    }

    /**
     * Returns the ActiveRepository instance that this repository is using.
     *
     * @return ActiveRepository<UserEntity,UserAr>
     */
    protected function getRepo(): ActiveRepository
    {
        return $this->repo;
    }
}

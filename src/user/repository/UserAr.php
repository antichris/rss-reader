<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\repository;

use antichris\rssReader\user\UserStatusService;
use Yii;
use yii\base\Security;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property int    $id
 * @property string $email
 * @property string $password_hash
 * @property string $first_name
 * @property string $last_name
 * @property string $auth_key
 * @property string $email_token
 * @property int    $status
 * @property int    $created_at
 * @property int    $updated_at
 */
class UserAr extends ActiveRecord
{
    /**
     * {@inheritdoc}
     *
     * Sets up the private $statusService reference to UserStatusService.
     */
    public function __construct(
        private Security $security,
        private UserStatusService $statusService,
        $config = [],
    ) {
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     *
     * @param mixed[] $row row data to be populated into the record
     *
     * @return self the newly created active record
     */
    public static function instantiate($row = null): self
    {
        /** @var UserArProvider $provider */
        $provider = Yii::createObject(UserArProvider::class);

        return $provider->provide();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [TimestampBehavior::class];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password_hash', 'first_name', 'last_name'], 'required'],
            [['id', 'status'], 'number'],
            [['email', 'password_hash', 'first_name', 'last_name', 'email_token'], 'string'],
            ['status', 'default', 'value' => $this->statusService->getDefault()],
            ['status', 'in', 'range' => $this->statusService->getAll()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->auth_key = $this->security->generateRandomString();
        }

        return true;
    }
}

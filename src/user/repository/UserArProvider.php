<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\repository;

use antichris\rssReader\misc\activeRepository\AbstractActiveRecordProvider;

/**
 * @method UserAr provide() Provides a new UserAr instance.
 *
 * @extends AbstractActiveRecordProvider<UserAr>
 */
class UserArProvider extends AbstractActiveRecordProvider
{
    /**
     * {@inheritdoc}
     */
    protected function getClassName(): string
    {
        return UserAr::class;
    }
}

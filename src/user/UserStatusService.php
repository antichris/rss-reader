<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

use BadMethodCallException;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Encapsulates and facilitates interaction with user status values.
 *
 * @method bool isDeleted(int $status)  Returns whether the given matches the DELETED status value.
 * @method bool isInactive(int $status) Returns whether the given matches the INACTIVE status value.
 * @method bool isActive(int $status)   Returns whether the given matches the ACTIVE status value.
 * @method bool isDefault(int $status)  Returns whether the given matches the DEFAULT status value.
 * @method int  getDeleted()            Returns the DELETED status value.
 * @method int  getInactive()           Returns the INACTIVE status value.
 * @method int  getActive()             Returns the ACTIVE status value.
 * @method int  getDefault()            Returns the DEFAULT status value.
 */
class UserStatusService
{
    public const DELETED = 0;
    public const INACTIVE = 1;
    public const ACTIVE = 2;
    public const DEFAULT = self::INACTIVE;

    /**
     * @var null|ReflectionClass<static>
     */
    private $reflection;

    /**
     * Handles the magic get[Status]() and is[Status]() calls.
     *
     * @param array<int> $arguments
     *
     * @throws BadMethodCallException
     *
     * @return bool|int
     */
    public function __call(string $name, array $arguments)
    {
        if ('get' === substr($name, 0, 3)) {
            $ucName = strtoupper($name);

            return $this->get(substr($ucName, 3));
        }
        if ('is' === substr($name, 0, 2)) {
            if (count($arguments) < 1) {
                throw new BadMethodCallException('Missing required value argument');
            }
            $ucName = strtoupper($name);

            return $this->is(substr($ucName, 2), $arguments[0]);
        }
        $class = get_class($this);

        throw new BadMethodCallException("Call to undefined method {$class}::{$name}())");
    }

    /**
     * Returns whether there is a status named as given.
     */
    public function has(string $name): bool
    {
        return $this->getReflection()->hasConstant($name);
    }

    /**
     * Returns the value of the given status.
     *
     * @throws InvalidArgumentException when the given status is not defined
     */
    public function get(string $name): int
    {
        if (!$this->has($name)) {
            throw new InvalidArgumentException("Status '{$name}' is not defined");
        }

        return $this->getReflection()->getConstant($name);
    }

    /**
     * Returns whether the status given by name matches the given value.
     */
    public function is(string $name, int $value): bool
    {
        return $this->get($name) === $value;
    }

    /**
     * Returns an array of all status values, keyed by names.
     *
     * @return int[]
     */
    public function getAll(): array
    {
        return $this->getReflection()->getConstants();
    }

    /**
     * Returns whether the given is a valid status value.
     */
    public function isValidValue(int $status): bool
    {
        return in_array($status, $this->getAll());
    }

    /**
     * A very introspective method.
     *
     * @return ReflectionClass<static>
     */
    private function getReflection(): ReflectionClass
    {
        if (empty($this->reflection)) {
            $this->reflection = new ReflectionClass($this);
        }

        return $this->reflection;
    }
}

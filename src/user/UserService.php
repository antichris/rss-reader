<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

/**
 * Provides user retrieval and storage interface.
 */
class UserService
{
    public function __construct(
        private UserRepositoryInterface $repository,
    ) {
    }

    /**
     * Finds an identity (user entity) by its ID.
     */
    public function findIdentity(int $id): ?UserEntity
    {
        return $this->repository->findIdentity($id);
    }

    /**
     * Finds a user entity by email (activation) token.
     */
    public function findByEmailToken(string $token): ?UserEntity
    {
        return $this->repository->findByEmailToken($token);
    }

    /**
     * Finds a user entity by email address.
     *
     * @param null|int $status when given, only matching entities are returned
     */
    public function findByEmail(string $email, ?int $status = null): ?UserEntity
    {
        return $this->repository->findByEmail($email, $status);
    }

    /**
     * Finds a user entity with the status ACTIVE by email address.
     */
    public function findActiveByEmail(string $email): ?UserEntity
    {
        return $this->repository->findByEmail($email, UserStatusService::ACTIVE);
    }

    /**
     * Saves a user entity in the database.
     *
     * @param bool          $runValidation  whether to run validation on the data
     * @param null|string[] $attributeNames list of select attribute names to be saved
     */
    public function save(UserEntity $user, bool $runValidation = true, ?array $attributeNames = null): bool
    {
        return $this->repository->save($user, $runValidation, $attributeNames);
    }
}

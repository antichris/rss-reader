<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * A bare-bones implementation of the IdentityInterface.
 */
class Identity implements IdentityInterface
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $auth_key;

    /**
     * {@inheritdoc}
     *
     * @param int $id
     */
    public static function findIdentity($id)
    {
        return Yii::createObject(UserService::class)->findIdentity($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented');
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}

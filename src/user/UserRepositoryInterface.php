<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

/**
 * Implements user entity retrieval and storage.
 */
interface UserRepositoryInterface
{
    /**
     * Saves a user entity in the database.
     *
     * @param bool          $runValidation  whether to run validation on the data
     * @param null|string[] $attributeNames list of select attribute names to be saved
     */
    public function save(UserEntity $entity, bool $runValidation = true, ?array $attributeNames = null): bool;

    /**
     * Finds a single user entity by the given condition.
     *
     * @param tActiveRCondition $condition primary key value or a set of column values
     */
    public function findOne($condition): ?UserEntity;

    /**
     * Finds an identity (user entity) by its ID.
     */
    public function findIdentity(int $id): ?UserEntity;

    /**
     * Finds a user entity by email (activation) token.
     */
    public function findByEmailToken(string $token): ?UserEntity;

    /**
     * Finds a user entity by email address.
     *
     * @param null|int $status when given, only matching entities are returned
     */
    public function findByEmail(string $email, ?int $status = null): ?UserEntity;
}

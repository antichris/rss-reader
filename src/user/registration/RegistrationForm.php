<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\registration;

use antichris\rssReader\user\repository\UserArProvider;
use yii\base\Model;

class RegistrationForm extends Model implements RegistrationFormInterface
{
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var string
     */
    public $lastName;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $repeatPassword;

    public function __construct(
        private UserArProvider $userArProvider,
        $config = [],
    ) {
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['firstName', 'lastName', 'email'], 'trim'],
            [['firstName', 'lastName', 'email', 'password', 'repeatPassword'], 'required'],
            [['firstName', 'lastName', 'email'], 'string', 'max' => 255],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => $this->userArProvider->getModelClass(),
                'message' => 'This email address is not available.',
            ],
            [['password', 'repeatPassword'], 'string', 'min' => 12],
            ['password', 'compare', 'compareAttribute' => 'repeatPassword'],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}

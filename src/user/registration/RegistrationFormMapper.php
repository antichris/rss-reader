<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\registration;

use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserEntityProvider;

/**
 * Maps data from registration forms to user entities.
 */
class RegistrationFormMapper
{
    public function __construct(
        private UserEntityProvider $userProvider,
        private PasswordService $passwordService,
    ) {
    }

    /**
     * Maps data from a form to a new user entity.
     */
    public function map(RegistrationFormInterface $form): UserEntity
    {
        return $this->fill($form, $this->userProvider->provide());
    }

    /**
     * Fills a user entity with data from a form.
     */
    public function fill(RegistrationFormInterface $form, UserEntity $user): UserEntity
    {
        $user->first_name = $form->getFirstName();
        $user->last_name = $form->getLastName();
        $user->email = $form->getEmail();
        $this->passwordService->setPassword($user, $form->getPassword());

        return $user;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\registration;

use antichris\rssReader\misc\AbstractProvider;

/**
 * @method RegistrationForm provide() Provides a new RegistrationForm instance.
 *
 * @extends AbstractProvider<RegistrationForm>
 */
class RegistrationFormProvider extends AbstractProvider
{
    /**
     * {@inheritdoc}
     */
    protected function getClassName(): string
    {
        return RegistrationForm::class;
    }
}

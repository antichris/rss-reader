<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\registration;

use antichris\rssReader\user\UserEntity;
use yii\mail\MailerInterface;
use yii\mail\MessageInterface;
use yii\web\UrlManager;

/**
 * Composes and sends registration confirmation emails.
 */
class RegistrationMailer
{
    /**
     * @param array<string,string>|string $sender
     */
    public function __construct(
        private MailerInterface $mailer,
        private UrlManager $urlManager,
        private string $appName,
        private $sender,
    ) {
    }

    /**
     * Composes and sends a registration confirmation email to the given user.
     */
    public function sendEmail(UserEntity $user): bool
    {
        $message = $this->composeEmail($user);

        return $this->mailer->send($message);
    }

    /**
     * Returns a registration confirmation email message for the given user.
     */
    protected function composeEmail(UserEntity $user): MessageInterface
    {
        $confirmLink = $this->urlManager->createAbsoluteUrl([
            '/registration/confirm',
            'token' => $user->email_token,
        ]);

        return $this->mailer->compose(
            ['html' => 'registration-html', 'text' => 'registration-text'],
            ['name' => $user->first_name, 'confirmLink' => $confirmLink],
        )
            ->setFrom($this->sender)
            ->setTo([$user->email => "{$user->first_name} {$user->last_name}"])
            ->setSubject("Account registration at {$this->appName}");
    }
}

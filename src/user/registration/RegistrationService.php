<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\registration;

use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserService;
use antichris\rssReader\user\UserStatusService;
use yii\base\InvalidArgumentException;
use yii\base\Security;

/**
 * Handles the registration of users.
 */
class RegistrationService
{
    public function __construct(
        private UserService $userService,
        private RegistrationFormMapper $mapper,
        private UserStatusService $userStatusService,
        private RegistrationMailer $mailer,
        private Security $security,
    ) {
    }

    /**
     * Generates an email confirmation token.
     *
     * The token consists of current UNIX time converted to base 36, left-padded with a random string to the given
     * length.
     */
    public function generateEmailToken(int $length = 32): string
    {
        $timeBase36 = base_convert((string) time(), 10, 36);

        $randomPrefix = $length - strlen($timeBase36) > 0
            ? $this->generateRandomString($length - strlen($timeBase36))
            : '';

        return "{$randomPrefix}{$timeBase36}";
    }

    /**
     * Registers a user.
     */
    public function register(RegistrationFormInterface $form): bool
    {
        if (!$form->validate()) {
            return false;
        }

        $user = $this->mapToUser($form);
        $user->email_token = $this->generateEmailToken();

        if (!$this->sendEmail($user)) {
            return false;
        }

        return $this->userService->save($user);
    }

    /**
     * Maps data from a form to a new user entity.
     */
    public function mapToUser(RegistrationFormInterface $form): UserEntity
    {
        return $this->mapper->map($form);
    }

    /**
     * Finds a user by the given email confirmation token.
     *
     * @throws InvalidArgumentException
     */
    public function getUserByToken(?string $token): UserEntity
    {
        if (empty($token)) {
            throw new InvalidArgumentException('Email confirmation token cannot be blank');
        }
        $user = $this->userService->findByEmailToken($token);
        if (!$user) {
            throw new InvalidArgumentException('Invalid email confirmation token');
        }

        return $user;
    }

    /**
     * Activates a user account.
     */
    public function activate(UserEntity $user): ?UserEntity
    {
        $user->status = $this->userStatusService->getActive();

        return $this->userService->save($user, false)
            ? $user
            : null;
    }

    /**
     * Returns a random string (URL-safe Base64 alphabet) of the given length.
     */
    protected function generateRandomString(int $length = 32): string
    {
        return $this->security->generateRandomString($length);
    }

    /**
     * Uses mailer dependency to compose and send a registration confirmation email to the given user.
     */
    protected function sendEmail(UserEntity $user): bool
    {
        // TODO Push into some kind of asynchronous mail queue service.
        return $this->mailer->sendEmail($user);
    }
}

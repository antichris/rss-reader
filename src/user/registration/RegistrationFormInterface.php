<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\registration;

use yii\base\InvalidArgumentException;

/**
 * A sloppy interface for a registration form model.
 */
interface RegistrationFormInterface
{
    /**
     * Performs the data validation.
     *
     * @param string|string[] $attributeNames attribute name or list of attribute names that should be validated
     * @param bool            $clearErrors    whether to call [[clearErrors()]] before performing validation
     *
     * @throws InvalidArgumentException if the current scenario is unknown
     *
     * @return bool whether the validation is successful without any error
     */
    public function validate($attributeNames = null, $clearErrors = true);

    public function getFirstName(): string;

    public function getLastName(): string;

    public function getEmail(): string;

    public function getPassword(): string;
}

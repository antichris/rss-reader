<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\authentication;

use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\UserEntity;
use antichris\rssReader\user\UserService;
use antichris\rssReader\user\UserStatusService;
use yii\web\IdentityInterface;
use yii\web\User;

/**
 * Handles the authentication of users.
 */
class AuthenticationService
{
    public function __construct(
        private UserService $userService,
        private UserStatusService $userStatusService,
        private PasswordService $passwordService,
        private User $userComponent,
    ) {
    }

    /**
     * Validates the login form and if password is valid, logs in the user.
     */
    public function login(LoginFormInterface $form): bool
    {
        if (!$form->validate()) {
            return false;
        }
        /** @var IdentityInterface $user */
        $user = $form->getUser();

        return $this->userComponent->login($user);
    }

    /**
     * Returns whether user's password matches the given one.
     */
    public function validatePassword(UserEntity $user, string $password): bool
    {
        return $this->passwordService->validatePassword($user, $password);
    }

    /**
     * Returns whether a user can be logged in with the given password.
     */
    public function canLogIn(UserEntity $user, string $password): bool
    {
        return $this->isStatusLoggableIn($user->status) && $this->validatePassword($user, $password);
    }

    /**
     * Retrieves user by the given email.
     */
    public function getUser(string $email): ?UserEntity
    {
        return $this->userService->findActiveByEmail($email);
    }

    /**
     * Returns whether someone having the given status can be logged in.
     */
    public function isStatusLoggableIn(int $status): bool
    {
        return $this->userStatusService->isActive($status);
    }
}

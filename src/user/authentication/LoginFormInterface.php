<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\authentication;

use antichris\rssReader\user\UserEntity;
use yii\base\InvalidArgumentException;

/**
 * A sloppy interface for a login form model.
 */
interface LoginFormInterface
{
    /**
     * Performs the data validation.
     *
     * @param string|string[] $attributeNames attribute name or list of attribute names that should be validated
     * @param bool            $clearErrors    whether to call [[clearErrors()]] before performing validation
     *
     * @throws InvalidArgumentException if the current scenario is unknown
     *
     * @return bool whether the validation is successful without any error
     */
    public function validate($attributeNames = null, $clearErrors = true);

    /**
     * Returns a value indicating whether there is any validation error.
     *
     * @param null|string $attribute attribute name. Pass null to check all attributes.
     *
     * @return bool
     */
    public function hasErrors($attribute = null);

    /**
     * Retrieves the user for this form (by email).
     *
     * @return null|UserEntity
     */
    public function getUser();
}

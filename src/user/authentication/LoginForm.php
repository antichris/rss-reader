<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user\authentication;

use antichris\rssReader\user\UserEntity;
use yii\base\Model;

/**
 * Collects and validates login credentials.
 */
class LoginForm extends Model implements LoginFormInterface
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var null|false|UserEntity
     */
    private $user = false;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        private AuthenticationService $service,
        $config = [],
    ) {
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password attribute.
     *
     * Also used as a validator in rules, therefore takes $attribute.
     */
    public function validatePassword(string $attribute): void
    {
        if ($this->hasErrors()) {
            return;
        }

        $user = $this->getUser();
        if ($user && $this->service->canLogIn($user, $this->{$attribute})) {
            return;
        }

        $this->addError($attribute, 'Incorrect email or password.');
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        if (false === $this->user) {
            $this->user = $this->service->getUser($this->email);
        }

        return $this->user;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\user;

use antichris\rssReader\misc\AbstractProvider;

/**
 * @method UserEntity provide() Provides a new UserEntity instance.
 *
 * @extends AbstractProvider<UserEntity>
 */
class UserEntityProvider extends AbstractProvider
{
    /**
     * {@inheritdoc}
     */
    protected function getClassName(): string
    {
        return UserEntity::class;
    }
}

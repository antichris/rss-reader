<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed;

/**
 * Returns top most common keyword occurrence counts among the given feed items.
 */
interface FeedTopKeywordExtractorInterface
{
    /**
     * Returns top most common keyword occurrence counts among the given feed items.
     *
     * @param Item[] $items
     *
     * @return int[]
     */
    public function extractTop(int $count, array $items): array;
}

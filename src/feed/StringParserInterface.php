<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed;

/**
 * Parses a XML feed data string and returns a bunch of Item objects.
 */
interface StringParserInterface
{
    /**
     * Parses a XML feed data string and returns a bunch of Item objects.
     *
     * @param int $limit maximum number of items to return, 0 means no limit
     *
     * @return Item[]
     */
    public function parse(string $feedXML, int $limit = 0): array;
}

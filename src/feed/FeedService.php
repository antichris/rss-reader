<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed;

use antichris\rssReader\misc\profiling\ProfilerInterface;
use Exception;

/**
 * Retrieves items from an RSS feed.
 */
class FeedService
{
    /**
     * @var string
     */
    public $feedURL = 'https://www.theregister.co.uk/software/headlines.atom';
    /**
     * @var int Maximum number of items to return when retrieving feed. 0 means no limit.
     */
    public $limit = 0;

    public function __construct(
        private StringFetcherInterface $fetcher,
        private StringParserInterface $parser,
        private ProfilerInterface $profiler,
    ) {
    }

    /**
     * Retrieves items from the feed.
     *
     * @param null|int $limit maximum number of items to return, 0 means no limit, null — use the value of `$this->limit`
     *
     * @throws FeedServiceException
     *
     * @return Item[]
     */
    public function retrieve(?string $feedURL = null, ?int $limit = null): array
    {
        if (empty($feedURL)) {
            $feedURL = $this->feedURL;
        }
        if (null === $limit) {
            $limit = (int) $this->limit;
        }

        try {
            $sample = $this->profiler->start("fetch {$feedURL}", __METHOD__);
            $xml = $this->fetcher->fetch($feedURL);
            $sample->next('parse');
            $items = $this->parser->parse($xml, $limit);
            $sample->end();
        } catch (Exception $exc) {
            throw new FeedServiceException("Could not retrieve the feed items: {$exc->getMessage()}", 0, $exc);
        }

        return $items;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use antichris\rssReader\feed\Item;
use antichris\rssReader\feed\ItemProvider;
use DateTimeImmutable;
use SimpleXMLElement;

/**
 * Maps a single SimpleXMLElement feed item to an Item object.
 */
abstract class AbstractSxItemMapper implements SxItemMapperInterface
{
    public function __construct(
        private ItemProvider $provider,
        private DateTimeParserInterface $dateTimeParser,
    ) {
    }

    /**
     * Creates a new Item instance and fills from the given $itemElement.
     */
    public function map(SimpleXMLElement $itemElement): Item
    {
        return $this->fill($itemElement, $this->provider->provide());
    }

    /**
     * Fills an instance of Item with values from the given $itemElement.
     */
    public function fill(SimpleXMLElement $itemElement, Item $item): Item
    {
        $item->title = $this->getTitle($itemElement);
        $item->description = $this->getDescription($itemElement);
        $item->link = $this->getLink($itemElement);
        $item->pubDate = $this->getPubDate($itemElement);
        $item->updated = $this->getUpdated($itemElement);
        $item->image = $this->getImageURL($itemElement);

        return $item;
    }

    /**
     * Extracts the publication time from the given feed item element.
     */
    abstract public function getPubDate(SimpleXMLElement $item): ?DateTimeImmutable;

    /**
     * Extracts the last modification time from the given feed item element.
     */
    abstract public function getUpdated(SimpleXMLElement $item): ?DateTimeImmutable;

    /**
     * Extracts the image URL from the given feed item element.
     */
    abstract public function getImageURL(SimpleXMLElement $item): ?string;

    /**
     * Extracts the title from the given feed item element.
     */
    public function getTitle(SimpleXMLElement $item): string
    {
        return (string) $item->title;
    }

    /**
     * Extracts the description from the given feed item element.
     */
    public function getDescription(SimpleXMLElement $item): string
    {
        return (string) $item->description;
    }

    /**
     * Extracts the link from the given feed item element.
     */
    public function getLink(SimpleXMLElement $item): string
    {
        return (string) $item->link;
    }

    /**
     * Converts a date and time string representation into a DateTimeImmutable instance.
     */
    protected function parseDateTime(string $dateTime): ?DateTimeImmutable
    {
        return $this->dateTimeParser->parse($dateTime);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use SimpleXMLElement;

/**
 * Returns whether XML tree looks like a valid feed.
 */
abstract class AbstractSxFeedValidator implements SxFeedValidatorInterface
{
    public function __construct(
        private SxFeedElementLocatorInterface $feedLocator,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function validate(SimpleXMLElement $root): bool
    {
        if (!$this->validateRoot($root)) {
            return false;
        }

        $feed = $this->getFeed($root);
        if (null === $feed) {
            return false;
        }

        return $this->validateFeed($feed);
    }

    abstract protected function validateRoot(SimpleXMLElement $root): bool;

    abstract protected function validateFeed(SimpleXMLElement $feed): bool;

    /**
     * Locates and returns the feed element.
     */
    protected function getFeed(SimpleXMLElement $root): ?SimpleXMLElement
    {
        return $this->feedLocator->getFeed($root);
    }
}

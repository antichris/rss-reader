<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use SimpleXMLElement;

/**
 * Returns the feed element from the XML tree.
 */
interface SxFeedElementLocatorInterface
{
    /**
     * Returns the feed element from the XML tree.
     */
    public function getFeed(SimpleXMLElement $root): ?SimpleXMLElement;
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use antichris\rssReader\feed\StringParserInterface;
use SimpleXMLElement;

/**
 * Fetches and parses a feed using SimpleXML.
 */
class SimpleXMLParser implements StringParserInterface
{
    public function __construct(
        private FormatDetectorInterface $formatDetector,
    ) {
    }

    /**
     * {@inheritdoc}
     *
     * @throws ParserException when the feed could not be parsed
     */
    public function parse(string $xml, int $limit = 0): array
    {
        $rootElement = $this->createSimpleXMLElement($xml);
        if (false === $rootElement) {
            throw new ParserException('Could not parse feed data as XML');
        }

        $format = $this->formatDetector->detect($rootElement);
        if (null === $format) {
            throw new ParserException('Could not detect feed format of the XML data');
        }

        return $format->getMapper()->map($rootElement, $limit);
    }

    /**
     * Creates a new SimpleXMLElement from the given XML.
     *
     * @return false|SimpleXMLElement
     */
    protected function createSimpleXMLElement(string $xml)
    {
        return new SimpleXMLElement($xml);
    }
}

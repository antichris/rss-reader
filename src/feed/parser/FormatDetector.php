<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use InvalidArgumentException;
use SimpleXMLElement;

/**
 * {@inheritdoc}
 */
class FormatDetector implements FormatDetectorInterface
{
    /**
     * @var SxFeedFormatInterface[]
     */
    private $formats;

    /**
     * @throws InvalidArgumentException when no formats given or a format is not a valid instance
     */
    public function __construct(SxFeedFormatInterface ...$formats)
    {
        if (empty($formats)) {
            throw new InvalidArgumentException('At least one feed format required');
        }

        $this->formats = $formats;
    }

    /**
     * {@inheritdoc}
     */
    public function detect(SimpleXMLElement $root): ?SxFeedFormatInterface
    {
        foreach ($this->formats as $format) {
            if ($format->validate($root)) {
                return $format;
            }
        }

        return null;
    }
}

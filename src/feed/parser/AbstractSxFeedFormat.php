<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use SimpleXMLElement;

/**
 * Binds a feed validator with a corresponding mapper.
 */
abstract class AbstractSxFeedFormat implements SxFeedFormatInterface
{
    public function __construct(
        private SxFeedValidatorInterface $validator,
        private SxFeedMapperInterface $mapper,
    ) {
    }

    /**
     * Returns the name of this feed format.
     */
    abstract public function getName(): string;

    /**
     * Returns whether the XML tree looks like a valid feed of this format.
     */
    public function validate(SimpleXMLElement $root): bool
    {
        return $this->validator->validate($root);
    }

    /**
     * Returns the feed mapper for this format.
     */
    public function getMapper(): SxFeedMapperInterface
    {
        return $this->mapper;
    }
}

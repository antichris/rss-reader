<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use DateTimeImmutable;

/**
 * Parses a string representation of date and time into a DateTimeImmutable instance.
 */
abstract class AbstractDateTimeParser implements DateTimeParserInterface
{
    /**
     * Parses a date and time string representation into a DateTimeImmutable instance.
     */
    public function parse(string $dateTime): ?DateTimeImmutable
    {
        $result = $this->createFromFormat($this->getDateFormat(), $dateTime);

        return $result
            ?: null;
    }

    /**
     * Returns the date format used by this parser.
     */
    abstract protected function getDateFormat(): string;

    /**
     * Creates a DateTimeImmutable instance from the given format.
     *
     * @return DateTimeImmutable|false
     */
    protected function createFromFormat(string $format, string $dateTime)
    {
        return DateTimeImmutable::createFromFormat($format, $dateTime);
    }
}

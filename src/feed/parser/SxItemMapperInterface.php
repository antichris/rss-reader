<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use antichris\rssReader\feed\Item;
use SimpleXMLElement;

/**
 * Maps a single SimpleXMLElement feed item to an Item object.
 */
interface SxItemMapperInterface
{
    /**
     * Creates a new Item instance and fills from the given $itemElement.
     */
    public function map(SimpleXMLElement $itemElement): Item;

    /**
     * Fills an instance of Item with values from the given $itemElement.
     */
    public function fill(SimpleXMLElement $itemElement, Item $item): Item;
}

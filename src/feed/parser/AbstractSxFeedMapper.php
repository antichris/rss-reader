<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use antichris\rssReader\feed\Item;
use SimpleXMLElement;

/**
 * Maps a SimpleXML root element to an array of feed items.
 */
abstract class AbstractSxFeedMapper implements SxFeedMapperInterface
{
    public function __construct(
        private SxFeedElementLocatorInterface $feedLocator,
        private SxItemMapperInterface $itemMapper,
    ) {
    }

    /**
     * {@inheritdoc}
     */
    public function map(SimpleXMLElement $root, int $limit = 0): array
    {
        $mapper = $this->getItemMapper();
        $itemTag = $this->getItemTag();
        $feed = $this->getFeed($root);

        return $this->mapFeedItems($mapper, $itemTag, $feed, $limit);
    }

    abstract protected function getItemTag(): string;

    /**
     * Locates and returns the feed element.
     *
     * @throws MapperException when feed element could not be found
     *
     * @todo Fix handling feed with no items.
     */
    protected function getFeed(SimpleXMLElement $root): SimpleXMLElement
    {
        $feed = $this->feedLocator->getFeed($root);
        if (empty($feed)) {
            throw new MapperException('Feed element not found');
        }

        return $feed;
    }

    protected function getItemMapper(): SxItemMapperInterface
    {
        return $this->itemMapper;
    }

    /**
     * Maps the feed element to an array of items, limited to the given length.
     *
     * @return Item[]
     */
    protected function mapFeedItems(
        SxItemMapperInterface $mapper,
        string $itemTag,
        SimpleXMLElement $feed,
        int $limit,
    ): array {
        $count = 0;
        $items = [];
        foreach ($feed->{$itemTag} as $item) {
            if ($limit && ++$count > $limit) {
                break;
            }
            $items[] = $mapper->map($item);
        }

        return $items;
    }
}

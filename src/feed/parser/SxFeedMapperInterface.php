<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use antichris\rssReader\feed\Item;
use SimpleXMLElement;

/**
 * Maps a SimpleXML root element to an array of feed items.
 */
interface SxFeedMapperInterface
{
    /**
     * Maps a SimpleXML root element to an array of feed items.
     *
     * @param int $limit maximum number of items to return, 0 means no limit
     *
     * @return Item[]
     */
    public function map(SimpleXMLElement $rootElement, int $limit = 0): array;
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser\rss;

use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\feed\parser\AbstractSxItemMapper;
use DateTimeImmutable;
use SimpleXMLElement;

/**
 * Maps a single SimpleXMLElement feed item to an Item object.
 *
 * @method ?DateTimeImmutable parseDateTime(string $dateTime) Converts an RSS date and time string representation into a DateTimeImmutable instance.
 */
class SxRssItemMapper extends AbstractSxItemMapper
{
    public function __construct(ItemProvider $provider, RssDateTimeParser $dateTimeParser)
    {
        parent::__construct($provider, $dateTimeParser);
    }

    /**
     * Extracts the publication time from the given RSS feed item element.
     */
    public function getPubDate(SimpleXMLElement $item): ?DateTimeImmutable
    {
        return $this->parseDateTime((string) $item->pubDate);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdated(SimpleXMLElement $item): ?DateTimeImmutable
    {
        return null;
    }

    /**
     * Extracts the image URL from the given RSS feed item element.
     */
    public function getImageURL(SimpleXMLElement $item): ?string
    {
        $enclosure = $item->enclosure;
        if (!empty($enclosure) && $this->isImageTypeSupported((string) $enclosure['type'])) {
            return (string) $enclosure['url'];
        }

        return null;
    }

    /**
     * Returns whether the given MIME type is considered a supported image type.
     */
    private function isImageTypeSupported(string $type): bool
    {
        return 0 === strpos($type, 'image/');
    }
}

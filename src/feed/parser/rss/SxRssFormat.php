<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser\rss;

use antichris\rssReader\feed\parser\AbstractSxFeedFormat;

/**
 * Binds an RSS feed validator with the corresponding mapper.
 */
class SxRssFormat extends AbstractSxFeedFormat
{
    public function __construct(SxRssValidator $validator, SxRssMapper $mapper)
    {
        parent::__construct($validator, $mapper);
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'RSS';
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser\rss;

use antichris\rssReader\feed\parser\AbstractSxFeedMapper;

/**
 * Maps an RSS feed SimpleXML root element to an array of feed items.
 */
class SxRssMapper extends AbstractSxFeedMapper
{
    public function __construct(SxRssFeedLocator $feedLocator, SxRssItemMapper $itemMapper)
    {
        parent::__construct($feedLocator, $itemMapper);
    }

    protected function getItemTag(): string
    {
        return 'item';
    }
}

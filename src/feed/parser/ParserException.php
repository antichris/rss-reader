<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use antichris\rssReader\feed\FeedException;

/**
 * Thrown when parsing the feed fails.
 */
class ParserException extends FeedException
{
}

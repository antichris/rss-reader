<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser;

use DateTimeImmutable;

/**
 * Parses a string representation of date and time to a DateTimeImmutable instance.
 */
interface DateTimeParserInterface
{
    /**
     * Parses a date and time string representation to a DateTimeImmutable instance.
     */
    public function parse(string $dateTime): ?DateTimeImmutable;
}

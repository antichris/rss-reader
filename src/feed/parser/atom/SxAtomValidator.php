<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser\atom;

use antichris\rssReader\feed\parser\AbstractSxFeedValidator;
use SimpleXMLElement;

/**
 * Returns whether XML tree looks like a valid Atom feed.
 */
class SxAtomValidator extends AbstractSxFeedValidator
{
    public function __construct(SxAtomFeedLocator $feedLocator)
    {
        parent::__construct($feedLocator);
    }

    protected function validateRoot(SimpleXMLElement $root): bool
    {
        return 'feed' === $root->getName();
    }

    protected function getFeed(SimpleXMLElement $root): ?SimpleXMLElement
    {
        return $root;
    }

    protected function validateFeed(SimpleXMLElement $feed): bool
    {
        return !(empty($feed->id) || empty($feed->title) || empty($feed->updated));
    }
}

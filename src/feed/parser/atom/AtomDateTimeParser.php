<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser\atom;

use antichris\rssReader\feed\parser\AbstractDateTimeParser;
use DateTimeImmutable;

/**
 * Parses an Atom date and time string representation into a DateTimeImmutable instance.
 */
class AtomDateTimeParser extends AbstractDateTimeParser
{
    /**
     * {@inheritdoc}
     */
    protected function getDateFormat(): string
    {
        return DateTimeImmutable::ATOM;
    }
}

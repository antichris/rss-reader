<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\parser\atom;

use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\feed\parser\AbstractSxItemMapper;
use antichris\rssReader\feed\parser\MapperException;
use DateTimeImmutable;
use SimpleXMLElement;

/**
 * Maps a single SimpleXMLElement Atom feed item to an Item object.
 *
 * @method ?DateTimeImmutable parseDateTime(string $dateTime) Converts an RSS date and time string representation into a DateTimeImmutable instance.
 */
class SxAtomItemMapper extends AbstractSxItemMapper
{
    public function __construct(ItemProvider $provider, AtomDateTimeParser $dateTimeParser)
    {
        parent::__construct($provider, $dateTimeParser);
    }

    /**
     * Extracts the alternate link from the given Atom feed entry.
     *
     * @throws MapperException if link URL could not be extracted
     *
     * @todo Implement full functionality.
     */
    public function getLink(SimpleXMLElement $entry): string
    {
        /** @var SimpleXMLElement $link */
        foreach ($entry->link as $link) {
            if ('alternate' === (string) $link['rel']) {
                $href = (string) $link['href'];
                if ($href) {
                    return $href;
                }
            }
        }

        throw new MapperException('Could not extract link URL from entry');
    }

    /**
     * Extracts the summary from the given Atom feed entry.
     */
    public function getDescription(SimpleXMLElement $entry): string
    {
        return (string) $entry->summary;
    }

    /**
     * Extracts the publication date from the given Atom feed entry.
     */
    public function getPubDate(SimpleXMLElement $entry): ?DateTimeImmutable
    {
        return $this->parseDateTime((string) $entry->published);
    }

    /**
     * Extracts the last update date from the given Atom feed entry.
     */
    public function getUpdated(SimpleXMLElement $entry): ?DateTimeImmutable
    {
        return $this->parseDateTime((string) $entry->updated);
    }

    /**
     * {@inheritdoc}
     */
    public function getImageURL(SimpleXMLElement $entry): ?string
    {
        return null;
    }
}

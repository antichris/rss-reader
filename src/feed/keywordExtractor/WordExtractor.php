<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\keywordExtractor;

/**
 * Extracts words from a string.
 */
class WordExtractor
{
    /**
     * @var string
     */
    private $wordRegex = "/([-\\p{L}0-9']+)/u";
    /**
     * @var string
     */
    private $nonWordFilterRegex = '/(\p{L}+)/u';

    /**
     * Extracts words from the string.
     *
     * @return string[]
     */
    public function extract(string $string): array
    {
        $words = $this->extractWords($string);
        if (empty($words)) {
            return [];
        }

        return $this->filterNonWords($words);
    }

    /**
     * Extracts an array of word-like strings from the string.
     *
     * @return string[]
     */
    protected function extractWords(string $string): array
    {
        $matches = null;
        if (!preg_match_all($this->wordRegex, $string, $matches)) {
            return [];
        }

        return $matches[0];
    }

    /**
     * Removes non-word strings from the array.
     *
     * @param string[] $words
     *
     * @return string[]
     */
    protected function filterNonWords(array $words): array
    {
        $i = 0;
        foreach ($words as $word) {
            if (preg_match($this->nonWordFilterRegex, $word)) {
                $words[$i++] = $word;
            }
        }

        return array_slice($words, 0, $i);
    }
}

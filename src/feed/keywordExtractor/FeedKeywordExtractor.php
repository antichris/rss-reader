<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\keywordExtractor;

use antichris\rssReader\feed\FeedTopKeywordExtractorInterface;
use antichris\rssReader\feed\Item;

/**
 * Returns top most common keyword occurrence counts among the given feed items.
 */
class FeedKeywordExtractor implements FeedTopKeywordExtractorInterface
{
    public function __construct(
        private KeywordExtractor $extractor,
    ) {
    }

    /**
     * Returns top most common keyword occurrence counts among the given feed items.
     */
    public function extractTop(int $count, array $items): array
    {
        return $this->extractor->extractTop(
            $count,
            $this->aggregateText($items),
        );
    }

    /**
     * @param Item[] $items
     */
    protected function aggregateText(array $items): string
    {
        $itemStrings = [];
        foreach ($items as $item) {
            $itemStrings[] = "{$item->title}\n{$item->description}";
        }

        return $this->strip(implode("\n", $itemStrings));
    }

    /**
     * Strip HTML tags from the text.
     */
    protected function strip(string $text): string
    {
        return strip_tags($text);
    }
}

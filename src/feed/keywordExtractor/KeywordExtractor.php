<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\keywordExtractor;

/**
 * Extracts keyword statistics from a string.
 */
class KeywordExtractor
{
    public const ORDER_DESC = SORT_DESC;
    public const ORDER_ASC = SORT_ASC;
    /**
     * Default sort order = descending.
     */
    public const ORDER_DEFAULT = self::ORDER_DESC;

    /**
     * @var string[]
     */
    public $blacklist = [
        'the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'I', 'it', 'for', 'not', 'on', 'with', 'he', 'as',
        'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'say', 'her', 'she', 'or', 'an', 'will',
        'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get', 'which',
        'go', 'me',
        // Top 50 ends here
        'when', 'make', 'can', 'like', 'time', 'no', 'just', 'him', 'know', 'take', 'people', 'into', 'year', 'your',
        'good', 'some', 'could', 'them', 'see', 'other', 'than', 'then', 'now', 'look', 'only', 'come', 'its', 'over',
        'think', 'also', 'back', 'after', 'use', 'two', 'how', 'our', 'work', 'first', 'well', 'way', 'even', 'new',
        'want', 'because', 'any', 'these', 'give', 'day', 'most', 'us',
        // Top 100 ends here
        'is', 'was', 'are', 'has', 'been', 'says', 'more', 's',
    ];

    public function __construct(
        private WordExtractor $extractor,
    ) {
    }

    /**
     * Counts keyword occurrences in the given string.
     *
     * @return array<string,int>
     */
    public function extract(string $string): array
    {
        $words = $this->extractor->extract($string);
        $filtered = $this->filter($words, $this->blacklist);

        return $this->count($filtered);
    }

    /**
     * Returns counts of keyword occurrences in the given string, ordered as given.
     *
     * @return array<string,int>
     */
    public function extractSorted(string $string, int $order = self::ORDER_DEFAULT): array
    {
        $words = $this->extract($string);

        return $this->sort($words, $order);
    }

    /**
     * Returns top most common keyword occurrence counts in the given string.
     *
     * @return array<string,int>
     */
    public function extractTop(int $count, string $string): array
    {
        $words = $this->extractSorted($string, self::ORDER_DEFAULT);

        return array_slice($words, 0, $count);
    }

    /**
     * Returns the array of words after having removed the blacklisted ones, compared as lowercase.
     *
     * @param string[] $words
     * @param string[] $blacklist
     *
     * @return string[]
     */
    protected function filter(array $words, array $blacklist): array
    {
        $flippedBl = array_flip(array_map('mb_strtolower', $blacklist));

        $i = 0;
        foreach ($words as $word) {
            if (isset($flippedBl[mb_strtolower($word)])) {
                continue;
            }
            $words[$i++] = $word;
        }

        return array_slice($words, 0, $i);
    }

    /**
     * Returns counts of word occurrences in the array, counting all in lower case.
     *
     * @param string[] $words
     *
     * @return array<string,int>
     */
    protected function count(array $words): array
    {
        return array_count_values(array_map('mb_strtolower', $words));
    }

    /**
     * Sorts word counts in the given order.
     *
     * @param array<string,int> $wordCounts
     *
     * @return array<string,int>
     */
    protected function sort(array $wordCounts, int $order = self::ORDER_DEFAULT): array
    {
        array_multisort($wordCounts, $order, SORT_NUMERIC);

        return $wordCounts;
    }
}

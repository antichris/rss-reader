<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed\fetcher;

use antichris\rssReader\feed\StringFetcherInterface;

/**
 * {@inheritdoc}
 */
class BasicStringFetcher implements StringFetcherInterface
{
    /**
     * {@inheritdoc}
     */
    public function fetch(string $url): string
    {
        $xml = $this->getContents($url);
        if (false === $xml) {
            throw new FetcherException('Failed fetching the feed data');
        }

        return $xml;
    }

    /**
     * Returns what file_get_contents() returns when passed the given URL.
     *
     * @return false|string
     */
    protected function getContents(string $url)
    {
        return file_get_contents($url);
    }
}

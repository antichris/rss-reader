<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\feed;

use DateTimeImmutable;

/**
 * Represents an item of an RSS feed.
 */
class Item
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $link;

    /**
     * @var null|DateTimeImmutable publication date
     */
    public $pubDate;

    /**
     * @var null|DateTimeImmutable indicates the last time the entry was modified in a significant way
     */
    public $updated;

    /**
     * @var null|string image URL
     */
    public $image;
}

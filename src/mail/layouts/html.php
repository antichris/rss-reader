<?php declare(strict_types=1);

use antichris\rssReader\web\View;
use yii\helpers\Html;
use yii\mail\MessageInterface;

/** @var View $this view component instance */
/** @var MessageInterface $message the message being composed */
/** @var string $content main view render result */
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= $this->language; ?>" lang="<?= $this->language; ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= $message->getCharset(); ?>" />
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body>
    <?php $this->beginBody(); ?>
    <?= $content; ?>
    <?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

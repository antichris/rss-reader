<?php declare(strict_types=1);
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var string $name */
/** @var string $confirmLink */
?>
<div class="verify-email">
    <p>Hello <?= Html::encode($name); ?>,</p>

    <p>Follow the link below to confirm your registration:</p>

    <p><?= Html::a(Html::encode($confirmLink), $confirmLink); ?></p>
</div>

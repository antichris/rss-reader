<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\assets;

use RuntimeException;

/**
 * Represents a web application manifest file.
 *
 * @phpstan-type tManifest array<string,mixed>
 */
class ManifestFile
{
    private bool $permsChecked = false;

    public function __construct(
        private string $path,
    ) {
    }

    /**
     * Read manifest data from the file.
     *
     * @return tManifest
     */
    public function read(): array
    {
        $jsonString = @file_get_contents($this->path);
        if ($jsonString === false) {
            throw new RuntimeException(
                "Could not read manifest file: {$this->path}",
            );
        }

        return json_decode($jsonString, true, flags: JSON_THROW_ON_ERROR);
    }

    /**
     * Return whether the manifest file does not need updating.
     *
     * @todo Better API/documentation
     */
    public function isUpToDate(): bool
    {
        $ref = $this->referenceFile();

        return file_exists($this->path) && file_exists($ref)
            && filemtime($ref) >= filemtime($this->path);
    }

    /**
     * Write given manifest data to the file.
     *
     * @param tManifest $manifest
     */
    public function write(array $manifest): void
    {
        $this->checkPerms();

        $jsonString = json_encode(
            $manifest,
            JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES,
        );

        if (@file_put_contents($this->path, $jsonString) === false) {
            throw new RuntimeException(
                "Could not write manifest file: {$this->path}",
            );
        }
    }

    /**
     * Mark the manifest file as updated.
     *
     * @todo Better API/documentation
     */
    public function markUpToDate(): void
    {
        $this->checkPerms();

        $ref = $this->referenceFile();
        if (@touch($ref) === false) {
            throw new RuntimeException("Could not touch: {$ref}");
        }
    }

    protected function checkPerms(): void
    {
        if ($this->permsChecked) {
            return;
        }

        $dir = dirname($this->path);
        if (!is_dir($dir)) {
            throw new RuntimeException("Directory does not exist: {$dir}");
        }
        if (!is_writable($dir)) {
            throw new RuntimeException("Directory not writeable: {$dir}");
        }

        $this->permsChecked = true;
    }

    protected function referenceFile(): string
    {
        return dirname($this->path) . DIRECTORY_SEPARATOR
            . '~' . basename($this->path);
    }
}

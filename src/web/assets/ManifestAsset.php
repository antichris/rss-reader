<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\assets;

use DomainException;
use yii\web\AssetBundle;
use yii\web\AssetManager;

/**
 * Bundles a web application manifest asset, updating the paths of its icons
 * with URLs after the publication.
 *
 * @phpstan-type tManifest array<string,mixed>
 */
class ManifestAsset extends AssetBundle
{
    public const KEY_THEME_COLOR = 'theme_color';
    public const KEY_ICONS = 'icons';

    /**
     * {@inheritDoc}
     */
    public $sourcePath = '@assets/webmanifest';

    /**
     * Name of the web application manifest file.
     */
    public string $manifestFile = 'manifest.json';

    /**
     * {@inheritDoc}
     */
    public function registerAssetFiles($view): void
    {
        $manager = $view->getAssetManager();
        $view->registerLinkTag([
            'href' => $manager->getAssetUrl($this, $this->manifestFile),
            'rel' => 'manifest',
        ]);

        $manifest = $this->getFile($manager)->read();
        if (!empty($manifest[self::KEY_THEME_COLOR])) {
            $view->registerMetaTag([
                'name' => 'theme-color',
                'content' => $manifest[self::KEY_THEME_COLOR],
            ]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function publish($manager): void
    {
        if (
            $this->sourcePath !== null
            && !isset($this->basePath, $this->baseUrl)
        ) {
            [$this->basePath, $this->baseUrl] = $manager
                ->publish($this->sourcePath, $this->publishOptions);
        }

        $this->publishManifest($manager);
    }

    protected function getFile(AssetManager $manager): ManifestFile
    {
        $path = $manager->getAssetPath($this, $this->manifestFile);
        if (!$path) {
            throw new DomainException('Could not resolve manifest file path');
        }

        return new ManifestFile($path);
    }

    protected function publishManifest(AssetManager $manager): void
    {
        $file = $this->getFile($manager);
        if ($file->isUpToDate()) {
            return;
        }
        $this->updateManifest($manager, $file);
        $file->markUpToDate();
    }

    protected function updateManifest(
        AssetManager $manager,
        ManifestFile $file,
    ): void {
        $manifest = $file->read();
        if (empty($manifest[self::KEY_ICONS])) {
            return;
        }
        foreach ($manifest[self::KEY_ICONS] as &$icon) {
            $icon['src'] = $manager->getAssetUrl($this, $icon['src']);
        }
        $file->write($manifest);
    }
}

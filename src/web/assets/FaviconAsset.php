<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\assets;

use yii\web\AssetBundle;
use yii\web\AssetManager;
use yii\web\View;

/**
 * Bundles favicon asset(s).
 *
 * @phpstan-type tSize int|array{int,int}
 * @phpstan-type tSizes tSize[]|int|self::ANY
 * @phpstan-type tIcon array{file:string,sizes:tSizes,rel?:string,type?:string}
 */
class FaviconAsset extends AssetBundle
{
    public const ANY = 'any';
    public const DEFAULT_REL = 'icon';

    public $sourcePath = '@assets/favicon';

    /**
     * Favicon assets.
     *
     * Each asset is an associative array, consisting of:
     *
     * - file: favicon asset file name
     * - sizes: icon sizes contained in the asset file, an array of ints if
     *      there is more than one size, an int otherwise; use the constant ANY
     *      if the asset is in a scalable vector format (such as SVG)
     * - rel (optional, defaults to 'icon'): the rel attribute for the link tag
     * - type (optional): MIME type of the asset
     *
     * @phpstan-var tIcon[]
     *
     * @var array[]
     */
    public array $icons = [[
        'file' => 'favicon.ico',
        'sizes' => [16, 32, 48],
        'type' => 'image/vnd.microsoft.icon',
    ], [
        'file' => 'favicon.svg',
        'sizes' => self::ANY,
        'type' => 'image/svg+xml',
    ], [
        'file' => '180.png',
        'sizes' => 180,
        'rel' => 'apple-touch-icon',
        'type' => 'image/png',
    ]];

    public function registerAssetFiles($view): void
    {
        foreach ($this->icons as $icon) {
            $this->registerIcon($view, $icon);
        }
    }

    /**
     * @param tIcon $icon
     */
    protected function registerIcon(
        View $view,
        array $icon,
    ): void {
        $manager = $view->getAssetManager();
        $link = $this->prepLink($manager, $icon);
        $view->registerLinkTag($link);
    }

    /**
     * @param tIcon $icon
     *
     * @return array{href:string,rel:string,sizes:string,type?:string}
     */
    protected function prepLink(
        AssetManager $manager,
        array $icon,
    ): array {
        $link = [
            'href' => $manager->getAssetUrl($this, $icon['file']),
            'rel' => self::DEFAULT_REL,
            'sizes' => $this->sizesString($icon['sizes']),
        ];

        if ($type = $icon['type'] ?? null) {
            $link['type'] = $type;
        }
        if ($rel = $icon['rel'] ?? null) {
            $link['rel'] = $rel;
        }

        return $link;
    }

    /**
     * @param tSizes $sizes
     */
    protected function sizesString(array|int|string $sizes): string
    {
        if ($sizes === self::ANY) {
            return $sizes;
        }
        if (is_int($sizes)) {
            return $this->sizeString($sizes);
        }

        return implode(' ', array_map([$this, 'sizeString'], $sizes));
    }

    /**
     * @param tSize $size
     */
    protected function sizeString(int|array $size): string
    {
        if (is_int($size)) {
            return "{$size}x{$size}";
        }

        return implode('x', $size);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\assets;

use yii\bootstrap5\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main application asset bundle.
 */
class AppAsset extends AssetBundle
{
    /**
     * {@inheritdoc}
     */
    public $basePath = '@webroot';
    /**
     * {@inheritdoc}
     */
    public $baseUrl = '@web';
    /**
     * {@inheritdoc}
     */
    public $css = [
        'css/site.css',
    ];
    /**
     * {@inheritdoc}
     */
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        FaviconAsset::class,
        ManifestAsset::class,
    ];
}

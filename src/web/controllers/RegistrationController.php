<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\controllers;

use antichris\rssReader\user\registration\RegistrationFormProvider;
use antichris\rssReader\user\registration\RegistrationService;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller as BaseController;
use yii\web\Response;
use yii\web\Session;
use yii\web\User;
use yii\widgets\ActiveForm;

/**
 * Provides the interface for account registration and activation.
 */
class RegistrationController extends BaseController
{
    /**
     * The key for the registration success session flash.
     */
    private const KEY_FLASH_REGISTRATION_SUCCESS = '__flash/user/registration/success';

    /**
     * {@inheritdoc}
     */
    public function __construct(
        $id,
        $module,
        private RegistrationService $service,
        private RegistrationFormProvider $formProvider,
        private User $userComponent,
        private Session $session,
        $config = [],
    ) {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => false, 'roles' => ['@'], 'class' => RedirectHomeAccessRule::class], // Deny authenticated users.
                    ['allow' => true, 'roles' => ['?']], // Allow unauthenticated ones.
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET', 'POST'],
                    '*' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Handles registration form display, validation and post-registration success message rendering.
     *
     * @return array<string,string[]>|tActionResult AJAX calls get array of validation results/errors
     */
    public function actionIndex()
    {
        if ($this->session->hasFlash(self::KEY_FLASH_REGISTRATION_SUCCESS)) {
            $user = $this->session->getFlash(self::KEY_FLASH_REGISTRATION_SUCCESS);

            return $this->render('success', [
                'user' => $user,
            ]);
        }

        $form = $this->formProvider->provide();

        if ($form->load($this->request->post())) {
            if ($this->request->isAjax) {
                $this->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($form);
            }
            if ($this->service->register($form)) {
                $this->session->setFlash(self::KEY_FLASH_REGISTRATION_SUCCESS, $this->service->mapToUser($form));

                return $this->redirect(['index']);
            }
        }

        $form->password = '';
        $form->repeatPassword = '';

        return $this->render('index', [
            'model' => $form,
        ]);
    }

    /**
     * Validates the email confirmation token and activates the account.
     *
     * @param string $token email confirmation token
     *
     * @throws BadRequestHttpException when the token could not be verified
     *
     * @return tActionResult
     */
    public function actionConfirm(string $token)
    {
        try {
            $user = $this->service->getUserByToken($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $activatedUser = $this->service->activate($user);

        if ($activatedUser && $this->userComponent->login($activatedUser)) {
            $this->session->setFlash('success', 'Your account has been activated. Welcome!');
        } else {
            $this->session->setFlash('error', 'Unable to activate your account with the provided token.');
        }

        return $this->goHome();
    }
}

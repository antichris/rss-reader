<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\controllers;

use antichris\rssReader\user\authentication\AuthenticationService;
use antichris\rssReader\user\authentication\LoginFormProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller as BaseController;
use yii\web\User;

/**
 * Provides user authentication (login/logout) interface.
 */
class UserController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        $id,
        $module,
        private AuthenticationService $loginService,
        private LoginFormProvider $formProvider,
        private User $userComponent,
        $config = [],
    ) {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'actions' => ['logout'], 'roles' => ['@']], // Allow authenticated users.
                    ['allow' => true, 'actions' => ['login'], 'roles' => ['?']], // Allow unauthenticated ones.
                    ['allow' => false, 'actions' => ['login'], 'roles' => ['@'], // Deny authenticated users.
                        'class' => RedirectHomeAccessRule::class, ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'login' => ['GET', 'POST'],
                    'logout' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders and validates the login form.
     *
     * @return tActionResult
     */
    public function actionLogin()
    {
        $form = $this->formProvider->provide([$this->loginService]);

        if ($form->load($this->request->post()) && $this->loginService->login($form)) {
            return $this->goBack();
        }

        $form->password = '';

        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * Logs out the user.
     *
     * @return tActionResult
     */
    public function actionLogout()
    {
        $this->userComponent->logout();

        return $this->goHome();
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\controllers;

use yii\base\Action;
use yii\filters\AccessRule;
use yii\web\Controller;

/**
 * Configures the default denyCallback to call `goHome()` of the controller housing the action, if it is a web controller.
 */
class RedirectHomeAccessRule extends AccessRule
{
    /**
     * Configures the default denyCallback to call `goHome()` of the controller housing the action, if it is a web controller.
     *
     * {@inheritdoc}
     */
    public function __construct($config = [])
    {
        $this->denyCallback = function (AccessRule $rule, Action $action) {
            if ($action->controller instanceof Controller) {
                $action->controller->goHome(); // Redirect back to home page.
            }
        };
        parent::__construct($config);
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\controllers;

use antichris\rssReader\feed\FeedService;
use antichris\rssReader\feed\FeedTopKeywordExtractorInterface;
use antichris\rssReader\misc\profiling\ProfilerInterface;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\PageCache;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Renders the RSS feed.
 */
class FeedController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        $id,
        $module,
        private FeedService $service,
        private FeedTopKeywordExtractorInterface $keywordExtractor,
        private ProfilerInterface $profiler,
        $config = [],
    ) {
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    ['allow' => true, 'roles' => ['@']],
                ],
            ],
            // XXX This is only okay if all users should get the same feed.
            'cache' => [
                'class' => PageCache::class,
                'duration' => 300,
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    '*' => ['GET'],
                ],
            ],
        ];
    }

    /**
     * Renders the RSS feed.
     *
     * @return tActionResult
     */
    public function actionIndex()
    {
        $_ = $this->profiler->start('feed index action', __METHOD__);

        $sample = $this->profiler->start('retrieve feed items', __METHOD__);
        $items = $this->service->retrieve();

        $sample->next('extract top keywords');
        $keywords = $this->keywordExtractor->extractTop(10, $items);

        $sample->next('create feed data provider');
        $provider = Yii::createObject([
            'class' => ArrayDataProvider::class,
            'allModels' => $items,
        ]);

        $sample->next('render index');
        $result = $this->render('index', [
            'keywords' => $keywords,
            'feedProvider' => $provider,
        ]);

        $sample->end();

        return $result;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web\widgets;

use yii\bootstrap5\Alert as BootstrapAlert;
use yii\bootstrap5\Widget;
use yii\web\Session;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:.
 *
 * ```php
 * \Yii::$app->session->setFlash('error', 'This is the message');
 * \Yii::$app->session->setFlash('success', 'This is the message');
 * \Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * \Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 *
 * @phpstan-type tCloseButtonOptions false|array{tag?:string,label?:string}
 */
class Alert extends Widget
{
    /**
     * @var array<string,string> the alert types configuration for the flash messages.
     *                           This array is setup as $key => $value, where:
     *                           - key: the name of the session flash variable
     *                           - value: the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'error' => 'alert-danger',
        'danger' => 'alert-danger',
        'success' => 'alert-success',
        'info' => 'alert-info',
        'warning' => 'alert-warning',
    ];
    /**
     * @var tCloseButtonOptions the options for rendering the close button tag.
     *                          Array will be passed to [[\yii\bootstrap\Alert::closeButton]].
     */
    public $closeButton = [];

    public function __construct(
        private Session $session,
        $config = [],
    ) {
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $flashes = $this->session->getAllFlashes();
        $appendClass = isset($this->options['class'])
            ? " {$this->options['class']}"
            : '';

        $results = [];
        foreach ($flashes as $type => $flash) {
            if (!isset($this->alertTypes[$type])) {
                continue;
            }

            foreach ((array) $flash as $i => $message) {
                $results[] = BootstrapAlert::widget([
                    'body' => $message,
                    'closeButton' => $this->closeButton,
                    'options' => array_merge($this->options, [
                        'id' => "{$this->getId()}-{$type}-{$i}",
                        'class' => $this->alertTypes[$type] . $appendClass,
                    ]),
                ]);
            }

            $this->session->removeFlash($type);
        }

        return implode('', $results);
    }
}

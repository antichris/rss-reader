<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\web;

use yii\base\Application as BaseApplication;
use yii\i18n\Formatter;
use yii\i18n\I18N;
use yii\web\Application as WebApplication;
use yii\web\User as User;
use yii\web\View as BaseView;

/**
 * @property-read string    $appName application name.
 * @property-read string    $charset charset currently used for the application.
 * @property-read string    $homeUrl homepage URL (when in a web application).
 * @property-read Formatter $formatter formatter component.
 * @property-read I18N      $i18n internationalization component.
 * @property-read string    $language current application language.
 * @property-read ?User     $userComponent user component in a web application.
 */
class View extends BaseView
{
    public function __construct(
        private BaseApplication|WebApplication $app,
        private Formatter $formatter,
        private I18N $i18n,
        private User $userComponent,
        $config = [],
    ) {
        parent::__construct($config);
    }

    /**
     * Returns the name of the Application instance associated with this view.
     */
    public function getAppName(): string
    {
        return $this->app->name;
    }

    /**
     * Returns the charset of the Application instance associated with this view.
     */
    public function getCharset(): string
    {
        return $this->app->charset;
    }

    /**
     * Returns the home URL of the web Application instance associated with this
     * view, or an empty string, if the application is not an instance of Yii
     * WebApplication.
     */
    public function getHomeUrl(): string
    {
        if (!$this->app instanceof WebApplication) {
            return '';
        }

        return $this->app->getHomeUrl();
    }

    /**
     * Returns the Formatter component instance associated with this view.
     */
    public function getFormatter(): Formatter
    {
        return $this->formatter;
    }

    /**
     * Returns the I18N component instance associated with this view.
     */
    public function getI18n(): I18N
    {
        return $this->i18n;
    }

    /**
     * Returns current language of the Application instance associated with this
     * view.
     */
    public function getLanguage(): string
    {
        return $this->app->language;
    }

    /**
     * Returns the User component instance associated with this view.
     */
    public function getUserComponent(): User
    {
        return $this->userComponent;
    }
}

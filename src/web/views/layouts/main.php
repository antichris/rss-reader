<?php declare(strict_types=1);

use antichris\rssReader\web\assets\AppAsset;
use antichris\rssReader\web\View;
use antichris\rssReader\web\widgets\Alert;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\helpers\Html;

/** @var View $this */
/** @var string $content */
AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= $this->language; ?>">
<head>
    <meta charset="<?= $this->charset; ?>">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags(); ?>
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>
<?php
NavBar::begin([
    'brandLabel' => $this->appName,
    'brandUrl' => $this->homeUrl,
    'options' => [
        'class' => 'navbar-expand-md navbar-dark bg-dark fixed-top',
    ],
    'innerContainerOptions' => ['class' => 'container-md'],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ms-auto'],
    'items' => [
        $this->userComponent?->isGuest ? (
            ['label' => 'Log In', 'url' => ['/user/login']]
        ) : (
            '<li class=nav-item>'
            . Html::beginForm(['/user/logout'], 'post')
            . Html::submitButton('Log Out', ['class' => 'btn nav-link'])
            . Html::endForm()
            . '</li>'
        ),
    ],
]);
NavBar::end();
?>
<main class=container-md>
    <?= Alert::widget(); ?>
    <?= $content; ?>
</main>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\web\View;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;

/** @var View $this */
/** @var ArrayDataProvider $feedProvider */
/** @var int[] $keywords */
$this->title = $this->appName;

echo $this->render('keywords', compact('keywords'));

echo ListView::widget([
    'dataProvider' => $feedProvider,
    'layout' => '{items}',
    'options' => ['class' => 'feed-items'],
    'itemOptions' => ['class' => 'card bg-light'],
    'itemView' => 'item/whole',
]);

<?php declare(strict_types=1);
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use yii\web\View;

/** @var View $this */
/** @var int[] $keywords */
/** @var ?string $color */ // TODO Come up with a more appropriate name for this.
$color ??= 'secondary';
?>
<details class="feed-keywords card border-<?= $color; ?>">
    <summary class="card-header bg-<?= $color; ?> text-white">Keywords</summary>
    <div class="card-body">
        <ul class="list-inline">
        <?php foreach ($keywords as $keyword => $count) { ?>
            <li class="list-inline-item"><?= $keyword; ?> <span class="badge bg-<?= $color; ?>"><?= $count; ?></span></li>
        <?php } ?>
        </ul>
    </div>
</details>

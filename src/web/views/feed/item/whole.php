<?php declare(strict_types=1);
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\feed\Item;
use yii\web\View;
use yii\widgets\ListView;

/** @var View $this */
/** @var Item $model */
/** @var mixed $key According to ListView documentation. */
/** @var int $index */
/** @var ListView $widget */
$contents = $widget->view->render('contents', ['model' => $model]);
?>
<div class="card-body">
    <div class="row">
        <?php if (!empty($model->image)) { ?>
            <div class="col-lg-4">
                <a href="<?= $model->link; ?>" target="_new">
                    <img src="<?= $model->image; ?>" alt="<?= $model->title; ?>">
                </a>
            </div>
        <?php } ?>
        <div class="col">
            <?= $contents; ?>
        </div>
    </div>
</div>

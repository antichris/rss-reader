<?php declare(strict_types=1);
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\feed\Item;
use antichris\rssReader\web\View;

/** @var View $this */
/** @var Item $model */
?>
<h3>
    <a href="<?= $model->link; ?>" target="_new"><?= $model->title; ?></a>
</h3>
<p class="text-muted">
    <?php if ($model->pubDate) { ?>
        <?= $this->formatter->asDatetime($model->pubDate); ?>
    <?php } ?>
    <?php if ($model->updated) { ?>
        <?= $this->i18n->format('Updated at {updated}', [
            'updated' => $this->formatter->asDatetime($model->updated),
        ], 'en-US'); ?>
    <?php } ?>
</p>
<p>
<?= $model->description; ?>

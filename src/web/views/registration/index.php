<?php declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\user\registration\RegistrationForm;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var RegistrationForm $model */
$this->title = 'Registration';
?>
<div class="row">
<div class="user-registration">
    <h1><?= Html::encode($this->title); ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'registration-form']); ?>

        <?= $form->field($model, 'firstName')->textInput(['autofocus' => true]); ?>
        <?= $form->field($model, 'lastName'); ?>
        <?= $form->field($model, 'email', ['enableAjaxValidation' => true, 'validateOnType' => true]); ?>
        <?= $form->field($model, 'password')->passwordInput(); ?>
        <?= $form->field($model, 'repeatPassword')->passwordInput(); ?>

        <div class=d-grid>
            <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'register-button']); ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
</div>

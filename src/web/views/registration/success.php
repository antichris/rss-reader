<?php declare(strict_types=1);
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\user\UserEntity;
use antichris\rssReader\web\View;
use yii\helpers\Html;

/** @var View $this */
/** @var UserEntity $user */
$this->title = 'Registration Successful';
?>
<div class="row">
    <div class="col col-md-auto registration-success">
    <div class="card">
        <h1 class="card-header text-success">Registration successful</h1>
        <div class="card-body">
            <p>An email with an activation link has been sent to <?= Html::tag('code', $user->email); ?>.
            <p>Check your inbox and follow that link to complete the registration process.
        </div>
        <div class="card-footer d-grid">
            <?= Html::a('Return to the home page', [$this->homeUrl], ['class' => 'btn btn-success']); ?>
        </div>
    </div>
    </div>
</div>

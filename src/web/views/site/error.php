<?php declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var string $name */
/** @var string $message */
/** @var Exception $exception */
$this->title = $name;
?>
<div class="site-error">
    <h1><?= Html::encode($this->title); ?></h1>
    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)); ?>
    </div>
    <p>The above error occurred while the Web server was processing your request.
</div>

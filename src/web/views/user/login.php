<?php declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\user\authentication\LoginForm;
use yii\bootstrap5\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/** @var View $this */
/** @var LoginForm $model */
$this->title = 'Log In';
?>
<div class="row">
<div class="user-login">
    <h1><?= Html::encode($this->title); ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]); ?>
        <?= $form->field($model, 'password')->passwordInput(); ?>

        <div class="d-grid gap-3">
            <?= Html::submitButton('Log In', ['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
            <?= Html::a('Register an account', ['/registration/index'], ['class' => 'btn btn-link']); ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
</div>

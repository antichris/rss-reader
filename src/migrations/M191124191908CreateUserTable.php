<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\migrations;

use antichris\rssReader\user\repository\UserArRepository;
use antichris\rssReader\user\UserStatusService;
use yii\db\Migration;

/**
 * Creates the user table.
 */
class M191124191908CreateUserTable extends Migration
{
    public function __construct(
        private UserArRepository $userRepository,
        private UserStatusService $userStatusService,
        $config = [],
    ) {
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $table = $this->getTableName();
        $defaultStatus = $this->userStatusService->getDefault();
        $tableOptions = 'mysql' === $this->getDb()->driverName
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'email_token' => $this->string()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue($defaultStatus),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createNamedIndex($table, ['status', 'id']);
        $this->createNamedIndex($table, ['status', 'email']);
        $this->createNamedIndex($table, ['status', 'email_token']);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable($this->getTableName());
    }

    /**
     * Returns the name of the table to be migrated.
     */
    public function getTableName(): string
    {
        return $this->userRepository->getTableName();
    }

    /**
     * Creates a named index.
     *
     * @param string[] $columns
     */
    private function createNamedIndex(string $table, array $columns, string $prefix = 'idx'): void
    {
        $name = $this->nameIndex($table, $columns, $prefix);
        $this->createIndex($name, $table, $columns);
    }

    /**
     * Generates and returns a name for an index, based off the given values.
     *
     * @param string[] $columns
     */
    private function nameIndex(string $table, array $columns, string $prefix = 'idx'): string
    {
        $rawTableName = $this->getDb()->schema->getRawTableName($table);

        return "{$prefix}__{$rawTableName}__" . implode(':', $columns);
    }
}

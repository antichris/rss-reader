<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc;

use yii\di\Container;

/**
 * Provides dependency-injected instances.
 *
 * @template T
 */
abstract class AbstractProvider
{
    /**
     * @param Container $container an instance of the Yii DI Container
     */
    public function __construct(
        private Container $container,
    ) {
    }

    /**
     * Provides an instance of the class that this provider is set up to provide.
     *
     * @param mixed[] $params a list of constructor parameter values. The parameters
     *                        should be provided in the order they appear in the
     *                        constructor declaration. If you want to skip some
     *                        parameters, you should index the remaining ones with
     *                        the integers that represent their positions in the
     *                        constructor parameter list.
     *
     * @return T
     */
    public function provide(array $params = [])
    {
        return $this->container->get($this->getClassName(), $params);
    }

    /**
     * Returns the name of the class that this provider is set up to instantiate.
     *
     * @return class-string<T>
     */
    abstract protected function getClassName(): string;
}

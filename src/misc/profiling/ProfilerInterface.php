<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\profiling;

interface ProfilerInterface
{
    public const DEFAULT_CATEGORY = 'application';

    /**
     * Start a new Sample with the given token and (optional) category.
     *
     * Contrary to what the Yii documentation {@link \yii\BaseYii::beginProfile}
     * inaccurately states, samples with the same token *cannot* be nested,
     * regardless of category. In fact, it seems that reusing
     */
    public function start(
        string $token,
        string $category = self::DEFAULT_CATEGORY,
    ): SampleInterface;

    /**
     * Wrap a closure between profiling begin and end calls.
     *
     * @template T
     * @phpstan-param callable():T $fn
     * @phpstan-return T
     */
    public function wrap(
        string $token,
        callable $fn,
        string $category = self::DEFAULT_CATEGORY,
    ): mixed;
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\profiling\concrete;

use antichris\rssReader\misc\profiling\ProfilerInterface;
use RuntimeException;
use Throwable;

/**
 * Profiler implements a handy interface to create and run Sample objects.
 */
class Profiler implements ProfilerInterface
{
    public function __construct(
        private SampleProvider $provider,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function start(
        string $token,
        string $category = self::DEFAULT_CATEGORY,
    ): Sample {
        return $this->provider->create($token, $category)->begin();
    }

    /**
     * {@inheritDoc}
     */
    public function wrap(
        string $token,
        callable $fn,
        string $category = self::DEFAULT_CATEGORY,
    ): mixed {
        $sample = $this->start($token, $category);

        try {
            return $fn();
        } catch (Throwable $ex) {
            throw new RuntimeException(
                "Exception during profiling: {$ex->getMessage()}",
                previous: $ex,
            );
        } finally {
            $sample->end();
        }
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\profiling\concrete;

use antichris\rssReader\misc\profiling\ReadySampleInterface;
use antichris\rssReader\misc\profiling\SampleInterface;
use LogicException;
use yii\log\Logger;

/**
 * Holds onto the bits of data relevant to the Logger for profiling and tells it
 * to start or stop doing so.
 *
 * Also implements a destructor, that automatically stops profiling as soon as
 * the instance object goes out of scope.
 */
class Sample implements SampleInterface, ReadySampleInterface
{
    private bool $active = false;

    public function __construct(
        public Logger $logger,
        public string $token,
        public string $category = 'application',
    ) {
    }

    public function __destruct()
    {
        if ($this->active) {
            $this->end();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function begin(): self
    {
        if ($this->active) {
            throw new LogicException('Sample already active');
        }
        $this->logger->log(
            $this->token,
            Logger::LEVEL_PROFILE_BEGIN,
            $this->category,
        );
        $this->active = true;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function next(string $token, ?string $category = null): static
    {
        $this->end();
        $this->token = $token;
        if ($category !== null) {
            $this->category = $category;
        }
        $this->begin();

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function end(): void
    {
        if (!$this->active) {
            throw new LogicException('Sample not active');
        }
        $this->logger->log(
            $this->token,
            Logger::LEVEL_PROFILE_END,
            $this->category,
        );
        $this->active = false;
    }
}

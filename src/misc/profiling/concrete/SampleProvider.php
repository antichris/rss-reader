<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\profiling\concrete;

use antichris\rssReader\misc\AbstractProvider;
use yii\di\Container;
use yii\log\Logger;

/**
 * Provides Sample instances.
 *
 * @method Sample provide(mixed[] $params = [])
 *
 * @internal
 * @extends AbstractProvider<Sample>
 */
class SampleProvider extends AbstractProvider
{
    /**
     * {@inheritDoc}
     */
    public function __construct(
        Container $container,
        private Logger $logger,
    ) {
        parent::__construct($container);
    }

    /**
     * Create a new Sample with the given token and (optional) category.
     */
    public function create(string $token, string $category = 'application'): Sample
    {
        return $this->provide([$this->logger, $token, $category]);
    }

    /**
     * {@inheritDoc}
     */
    protected function getClassName(): string
    {
        return Sample::class;
    }
}

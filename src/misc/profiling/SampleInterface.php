<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\profiling;

/** Represents a profiling sample. */
interface SampleInterface
{
    /**
     * End this sample and start a new one, reusing current category,
     * if a new one is not specified.
     */
    public function next(string $token, ?string $category = null): static;

    /**
     * End the profiling sample.
     */
    public function end(): void;
}

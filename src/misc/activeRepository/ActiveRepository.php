<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use yii\db\ActiveRecord;

/**
 * Implements entity retrieval and storage via ActiveRecord.
 *
 * @template TEntity of object
 * @template TActiveRecord of ActiveRecord
 * @extends BaseActiveRepository<TActiveRecord>
 */
class ActiveRepository extends BaseActiveRepository
{
    /**
     * @var CacheRepositoryAdapter<TActiveRecord>
     */
    private $cacheAdapter;

    /**
     * @param AbstractActiveRecordProvider<TActiveRecord>   $provider
     * @param ActiveRecordMapper<TEntity,TActiveRecord>     $mapper
     * @param ActiveRecordCache<TActiveRecord>              $cache
     * @param CacheRepositoryAdapterProvider<TActiveRecord> $cacheAdapterProvider
     */
    public function __construct(
        AbstractActiveRecordProvider $provider,
        private ActiveRecordMapper $mapper,
        ActiveRecordCache $cache,
        CacheRepositoryAdapterProvider $cacheAdapterProvider,
    ) {
        parent::__construct($provider);
        $this->cacheAdapter = $cacheAdapterProvider->provide([$cache]);
    }

    /**
     * {@inheritdoc}
     */
    public function save($entity, bool $runValidation = true, ?array $attributeNames = null): bool
    {
        $model = $this->cacheAdapter->fetch($entity->id);
        if (empty($model)) {
            $model = $this->mapper->mapToNewAr($entity);
        } else {
            $model = $this->mapper->mapToAr($entity, $model);
        }

        $result = $this->saveRecord($model, $runValidation, $attributeNames);
        if ($result) {
            $this->cacheAdapter->store($model);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function findOne($condition)
    {
        $model = $this->cacheAdapter->fetchByCondition($condition);
        if (empty($model)) {
            $model = $this->findOneRecord($condition);
        }
        if (empty($model)) {
            return null;
        }
        $this->cacheAdapter->store($model);

        return $this->mapper->mapToNewEntity($model);
    }
}

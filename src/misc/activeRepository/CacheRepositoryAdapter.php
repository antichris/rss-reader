<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use yii\db\ActiveRecord;

/**
 * Implements entity retrieval and storage via ActiveRecord.
 *
 * @template T of ActiveRecord
 */
class CacheRepositoryAdapter
{
    /**
     * @param ActiveRecordCache<T> $cache
     */
    public function __construct(
        private ActiveRecordCache $cache,
    ) {
    }

    /**
     * @param T $activeRecord
     */
    public function store(ActiveRecord $activeRecord): void
    {
        $this->cache->store($activeRecord);
    }

    /**
     * Fetches a cached ActiveRecord by ID.
     *
     * @return ?T
     */
    public function fetch(?int $id): ?ActiveRecord
    {
        if (null === $id) {
            return null;
        }

        return $this->cache->fetch($id);
    }

    /**
     * Fetches a cached ActiveRecord by extracting its ID from the search condition.
     *
     * @param tActiveRCondition $condition primary key value or a set of column values
     *
     * @return ?T
     */
    public function fetchByCondition($condition): ?ActiveRecord
    {
        return $this->fetch($this->getIdFromCondition($condition));
    }

    /**
     * Extracts active record ID from the search condition, if possible.
     *
     * @param tActiveRCondition $condition primary key value or a set of column values
     */
    protected function getIdFromCondition($condition): ?int
    {
        if (is_numeric($condition)) {
            return (int) $condition;
        }
        if (is_array($condition) && isset($condition['id'])) {
            return (int) $condition['id'];
        }

        return null;
    }
}

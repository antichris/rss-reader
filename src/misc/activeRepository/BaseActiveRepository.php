<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use yii\db\ActiveRecord;

/**
 * Base class for ActiveRecord repositories.
 *
 * @template T of ActiveRecord
 * @implements ActiveRepositoryInterface<T>
 */
abstract class BaseActiveRepository implements ActiveRepositoryInterface
{
    /**
     * @param AbstractActiveRecordProvider<T> $provider
     */
    public function __construct(
        private AbstractActiveRecordProvider $provider,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getModelClass(): string
    {
        return $this->provider->getModelClass();
    }

    /**
     * {@inheritDoc}
     */
    public function getTableName(): string
    {
        /** @var ActiveRecord $modelClass Not really, but can be used statically as such. */
        $modelClass = $this->getModelClass();

        return $modelClass::tableName();
    }

    /**
     * Saves an ActiveRecord instance in the database.
     *
     * @param T             $model
     * @param bool          $runValidation  whether to run validation on the data
     * @param null|string[] $attributeNames list of select attribute names to be saved
     */
    protected function saveRecord(ActiveRecord $model, bool $runValidation = true, ?array $attributeNames = null): bool
    {
        return $model->save($runValidation, $attributeNames);
    }

    /**
     * Finds a single ActiveRecord instance by the given condition.
     *
     * @param tActiveRCondition $condition primary key value or a set of column values
     *
     * @return T
     */
    protected function findOneRecord($condition): ?ActiveRecord
    {
        /** @var T $modelClass Not really, but can be used statically as such. */
        $modelClass = $this->getModelClass();

        return $modelClass::findOne($condition);
    }
}

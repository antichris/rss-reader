<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use yii\db\ActiveRecord;

/**
 * Implements entity retrieval and storage via ActiveRecord.
 *
 * @template T of ActiveRecord
 */
interface ActiveRepositoryInterface
{
    /**
     * Saves an entity in the database.
     *
     * @param mixed         $entity
     * @param bool          $runValidation  whether to run validation on the data
     * @param null|string[] $attributeNames list of select attribute names to be saved
     */
    public function save($entity, bool $runValidation = true, ?array $attributeNames = null): bool;

    /**
     * Finds a single entity by the given condition.
     *
     * @param tActiveRCondition $condition primary key value or a set of column values
     *
     * @return mixed
     */
    public function findOne($condition);

    /**
     * Returns the name of the ActiveRecord class managed by this repository.
     *
     * @return class-string<T>
     */
    public function getModelClass(): string;

    /**
     * Returns the table name of the ActiveRecord managed by this repository.
     */
    public function getTableName(): string;
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use antichris\rssReader\misc\AbstractProvider;
use yii\db\ActiveRecord;

/**
 * Maps data between ActiveRecord and entity instances.
 *
 * @template TEntity of object
 * @template TActiveRecord of ActiveRecord
 */
class ActiveRecordMapper
{
    /**
     * @param AbstractProvider<TEntity>                   $entityProvider
     * @param AbstractActiveRecordProvider<TActiveRecord> $arProvider
     */
    public function __construct(
        private AbstractProvider $entityProvider,
        private AbstractActiveRecordProvider $arProvider,
    ) {
    }

    /**
     * Maps data from an entity to an existing ActiveRecord instance.
     *
     * @param TEntity       $entity
     * @param TActiveRecord $activeRecord
     *
     * @return TActiveRecord
     */
    public function mapToAr(object $entity, ActiveRecord $activeRecord): ActiveRecord
    {
        $activeRecord->attributes = (array) $entity;

        return $activeRecord;
    }

    /**
     * Maps data from an ActiveRecord instance to an existing entity.
     *
     * @param TActiveRecord $activeRecord
     * @param TEntity       $entity
     *
     * @return TEntity
     */
    public function mapToEntity(ActiveRecord $activeRecord, object $entity): object
    {
        foreach ($activeRecord->attributes as $name => $value) {
            $entity->{$name} = $value;
        }

        return $entity;
    }

    /**
     * Maps data from an entity to a new ActiveRecord instance.
     *
     * @param TEntity $entity
     *
     * @return TActiveRecord
     */
    public function mapToNewAr(object $entity): ActiveRecord
    {
        return $this->mapToAr($entity, $this->arProvider->provide());
    }

    /**
     * Maps data from an ActiveRecord to a new entity instance.
     *
     * @param TActiveRecord $activeRecord
     *
     * @return TEntity
     */
    public function mapToNewEntity(ActiveRecord $activeRecord): object
    {
        return $this->mapToEntity($activeRecord, $this->entityProvider->provide());
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use antichris\rssReader\misc\AbstractProvider;
use yii\db\ActiveRecord;

/**
 * Provides instances of ActiveRepository.
 *
 * @method ActiveRepository<TEntity,TActiveRecord> provide(array $params=[]) Provides an instance of ActiveRepository.
 *
 * @template TEntity of object
 * @template TActiveRecord of ActiveRecord
 * @extends AbstractProvider<ActiveRepository<TEntity,TActiveRecord>>
 */
class ActiveRepositoryProvider extends AbstractProvider
{
    protected function getClassName(): string
    {
        return ActiveRepository::class;
    }
}

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use antichris\rssReader\misc\AbstractProvider;
use yii\db\ActiveRecord;

/**
 * @method T provide() Provides an instance of the ActiveRecord class that this provider is set up to provide.
 *
 * @template T of ActiveRecord
 * @extends AbstractProvider<T>
 */
abstract class AbstractActiveRecordProvider extends AbstractProvider
{
    /**
     * Returns the name of the ActiveRecord class that this provider is set up to instantiate.
     *
     * @return class-string<T>
     */
    public function getModelClass(): string
    {
        return $this->getClassName();
    }
}

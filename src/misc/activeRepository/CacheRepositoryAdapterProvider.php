<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use antichris\rssReader\misc\AbstractProvider;
use yii\db\ActiveRecord;

/**
 * Provides instances of CacheRepositoryAdapter.
 *
 * @method CacheRepositoryAdapter provide(array $params=[]) Provides an instance of CacheRepositoryAdapter.
 *
 * @template T of ActiveRecord
 * @extends AbstractProvider<CacheRepositoryAdapter<T>>
 */
class CacheRepositoryAdapterProvider extends AbstractProvider
{
    protected function getClassName(): string
    {
        return CacheRepositoryAdapter::class;
    }
}

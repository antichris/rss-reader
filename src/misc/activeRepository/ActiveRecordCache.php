<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader\misc\activeRepository;

use yii\db\ActiveRecord;

/**
 * Caches ActiveRecord instances.
 *
 * @template T of ActiveRecord
 */
class ActiveRecordCache
{
    /**
     * @var T[]
     */
    private $cachedRecords;

    /**
     * Returns whether an ActiveRecord instance with the given ID is cached.
     */
    public function has(int $id): bool
    {
        return isset($this->cachedRecords[$id]);
    }

    /**
     * Caches an ActiveRecord instance.
     *
     * @param T $activeRecord
     *
     * @todo Needs a more robust solution — not all tables in the world have single int ID column as the primary key.
     */
    public function store(ActiveRecord $activeRecord): void
    {
        $this->cachedRecords[$activeRecord->getAttribute('id')] = $activeRecord;
    }

    /**
     * Returns an ActiveRecord instance by the given ID, if there is one cached.
     *
     * @return ?T
     */
    public function fetch(int $id): ?ActiveRecord
    {
        if ($this->has($id)) {
            return $this->get($id);
        }

        return null;
    }

    /**
     * Returns an ActiveRecord instance by the given ID.
     *
     * @return T
     */
    protected function get(int $id): ActiveRecord
    {
        return $this->cachedRecords[$id];
    }
}

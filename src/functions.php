<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace antichris\rssReader;

use Closure;

/**
 * 22-character URL-safe Base64 encoded MD5 hash of the given string without the
 * trailing `=` padding.
 */
function base64md5(string $string): string
{
    return sodium_bin2base64(
        md5($string, true),
        SODIUM_BASE64_VARIANT_URLSAFE_NO_PADDING,
    );
}

/**
 * Caller method name, same value that the caller woud have in `__METHOD__`.
 *
 * If this is called by `MyClass::a()` when it is called by `MyClass::b()`,
 * this will return "MyClass::b".
 *
 * @param int  $frameOffset increase to retrieve higher up the stack
 * @param bool $useType     from the stack frame data to format result
 */
function callerName(
    int $frameOffset = 0,
    bool $useType = false,
): string {
    $frame = callerFrame($frameOffset + 1, DEBUG_BACKTRACE_IGNORE_ARGS);
    $op = $useType
        ? $frame['type']
        : '::';

    return $frame['class'] . $op . $frame['function'];
}

/**
 * Caller stack frame from debug_backtrace().
 *
 * If this is called by `a()` when that is called by `b()`, this will return the
 * stack frame of `b()`.
 *
 * @see \debug_backtrace() for details on return value structure.
 *
 * @param int $offset  of a frame to retrieve higher up the stack
 * @param int $options for `\debug_backtrace()`
 *
 * @return array<string,int|string>
 */
function callerFrame(
    int $offset = 0,
    int $options = DEBUG_BACKTRACE_PROVIDE_OBJECT,
): array {
    /** @var int
     * 1 - this function;
     * 2 - the method that called this looking to get its caller;
     * 3 - the caller that we were looking for
     */
    $targetFrame = 3;
    $trace = debug_backtrace($options, $targetFrame + $offset);

    return $trace[count($trace) - 1];
}

/**
 * Gets the value of an environment variable (set by the OS, putenv(), SAPI or
 * in $_ENV superglobal) or a default.
 *
 * @param string $varname      The variable name
 * @param mixed  $defaultValue The value to return of the variable is not set in the environment
 *
 * @return mixed|string the string value from the environment or the given default value
 *
 * @todo Consider a testable object-oriented solution.
 */
function getlenv(string $varname, $defaultValue = null)
{
    if (isset($_ENV[$varname])) {
        return $_ENV[$varname];
    }
    if (isset($_SERVER[$varname]) && !str_starts_with($varname, 'HTTP_')) {
        return $_SERVER[$varname];
    }
    $value = getenv($varname, true);
    if (false === $value) {
        return $defaultValue;
    }

    return $value;
}

/**
 * Returns a closure for calling a protected or private method of an object.
 *
 * When called, the closure will pass all of its arguments to the method and
 * pass back the return value.
 *
 * @return Closure&callable(...mixed):mixed
 */
function internalCall(object $instance, string $method): Closure
{
    return (fn (...$arguments) => $instance->{$method}(...$arguments))
        ->bindTo($instance, $instance);
}

# Changelog

This project adheres to [Semantic Versioning][semver2].


## 0.9.0

### New

- Bootstrap 5 GUI
- `View` component for web applications
- Password service for hashing and validation of user passwords
- `UserService::findActiveByEmail()` method
- PHPDoc annotations on generic types for static analysis
- Integration tests for
	- `Dotenv` and environment variable value retrieval
	- DI Container config of some of the more complex components
- DI Container configs for the components that now fully rely on DI
- Default values for SMTP host, user and password environment variables
- Image recipes for `nginx-dev` and `pma` in the `Makefile`
- Development-specific aggregate stage in the `Dockerfile.dist`
- Development notes and the original specification files in `docs/`
- `TODO` on mail queue in `RegistrationService`

### Changed

- Mailer to Symfony Mailer, since Swiftmailer is abandoned
- Environment variable support to look for values also in `$_ENV` and `$_SERVER`
- DI Container config to override the Yii web `View` component with our own
- `UserController` to use its own `Request` object instead of the global application one
- `AuthenticationService` to use `findActiveByEmail()` of `UserService` instead of plain `findByEmail()`
- `findByEmail()` in `UserRepositoryInterface`, `UserArRepository` and `UserService` to
	- take optional status
	- not to default to `ACTIVE` status
	- throw a `RangeException` if an invalid value is passed for status
- All test data providers to be as close as possible to the corresponding tests
- PHPStan analysis level after upgrade by reducing from max (9) to 8
- The feed keywords widget in the GUI to
	- render as a `<details>` element
	- have the same border, header and badge background
	- have its contents centered
- Tagging development Docker images: `<imagename>:dev` instead of `<imagename>-dev:latest`
- `composer.json` to keep Composer packages sorted via a config parameter

### Removed

- Support for a plain string value in `$class` parameter of `Container::get()` in the PHPStan stub
- Name and email minimum length validation from the registration form
- `repeatPassword` getter from the registration form
- Tests for `UserEntity`, since now it does not contain any testable logic
- NGINX logs volume from the Docker Compose deployment config
- Production specific volumes from the Docker Compose development deployment
- `FeedKeywordExtractorTest::provideGetCaseSensitive()` data provider (unused since the 0.8.0 release)

### Improved

- Dependency injection in
	- `Alert` widget
	- `AuthenticationService`
	- `RegistrationController`
	- `RegistrationMailer`
	- `RegistrationService`
	- `UserAr`
	- `UserController`
	- view templates
- Promotion of constructor arguments to class properties (where possible)
- HTML mail layout to use the message specific charset not the application one
- `findByEmail()` of `UserService` to mirror the signature of `UserRepositoryInterface`
- `UserRepositoryInterface::findByEmail()` `$email` parameter name
- Tests
	- by deduplication of the identical cases in `AbstractSxFeedValidatorTest::provideValidate()`
	- by extracting mock creation methods where that makes sense
	- by mocking `UserEntity`
		- in the setup of `ActiveRecordMapperTest`
		- instead of instantiating in `LoginFormTest`
	- for `RegistrationService`
		- by line wrapping `RegistrationService` constructor arguments
		- by annotating a form mock variable
	- for `AuthenticationService`
		- by wrapping `AuthenticationService` constructor arguments in a method
		- by annotating two form mock variables
- PHPStan config to include analysis level and target paths
- PHP-FPM and Composer base image version definition by moving that to the top of `Dockerfile.dist`
- PHP container entrypoint script by
	- introducing a main function
	- fixing the issues that `shellcheck` was warning about
- PHP CS Fixer config to prevent it from turning `@property-read/write` annotations into plain `@property`
- Composer caching in Bitbucket Pipelines config
- The process of adding Docker image recipes to the `Makefile`
- `vfsHelper::create()` PHPDoc comment
- `.env.example` comment format by
	- starting with a triple hash (`#`)
	- wrapping lines at 80 characters
- Spelling in the README and CHANGELOG

### Updated

- PHP to 8.0.14
- Composer to 2.2.4
- Composer dependencies:
	- `vlucas/phpdotenv` v5.4.1
	- `yiisoft/yii2` 2.0.44
	- `yiisoft/yii2-bootstrap` 2.0.11
	- `friendsofphp/php-cs-fixer` v3.4.0
	- `mikey179/vfsstream` v1.6.10
	- `phpbench/phpbench` 1.2.3
	- `phpstan/phpstan` 1.3.3
	- `phpstan/phpstan-phpunit` 1.0.0
	- `phpunit/phpunit` 9.5.11
	- `yiisoft/yii2-debug` 2.1.18


## 0.8.1

### New

- Asset bundle components
	- `FaviconAsset` for publishing a favicon
	- `ManifestAsset` for publishing a web application manifest file
- `profiling` package
- Profiling in `FeedController` and `FeedService`
- Benchmarks for (key)word extraction and filtering
- Feed index caching using unconditional `PageCache`
- Unit test coverage
	- `Alert` widget
	- `getlenv()` function
- Base unit `TestCase` methods that generate setter and getter closures for non-public fields
- PHPStan stubs for Yii DI `Container` and `BaseYii` classes
- Functions
	- `base64md5` returning 22 character URL-safe Base64 encoded (instead of 32 hexadecimal digit) MD5 hash
	- `callerName` and `callerFrame` returning data from a single frame up the call stack
	- `internalCall` extracted from base unit `TestCase`
- Mention of the web application test instance config (`etc/test-web.php`) in the README
- Bitbucket Pipelines configuration
- Composer development dependencies:
	- `mikey179/vfsstream` v1.6.8
	- `phpbench/phpbench` 1.0.3

### Changed

- Name of the file defining `getlenv()`

### Removed

- Empty javascript assets definition from the `AppAsset` bundle
- Unused `WordExtractor` instance property from its unit test
- `LoginFormTest::setInternalField()` method
- `instanceCtorArgs()` method from `DateTimeParserAbstractTest`, `AbstractDateTimeParserTest` and `UserArTest`, since that is now handled by the base unit `TestCase`
- Reference to `upstream.conf` from NGINX Docker run example in README

### Improved

- Performance of
	- `WordExtractor::filterNonWords()`
	- `KeywordExtractor::filter()`
- PHP CS Fixer configuration to enforce trailing comma in multiline function parameters and arguments
- Brevity of `FeedService` and `FeedControler` by promoting the matching constructor parameters to properties
- Documentation of `AssetBundle` properties in its PHPStan stub
- `KeywordExtractor` blacklist by adding another common word
- Simplicity of array type annotation of `$params` for `AbstractProvider::provide()`
- Visibility modifiers of test double creation methods in PHPUnit's `TestCase` PHPStan stub
- Base unit `TestCase`
	- `internalCall()` method to use arrow function
	- `mockInstance()` method return type annotation as intersection
	- doccomments wrapped to ~80 chars
	- subclass `mockInstance` @method annotations by specifying the array type of `$mockedMethods` parameter
	- by adding a default `instanceCtorArgs` method
- Unit test for `WordExtractor::extractWords()` by
	- actually setting expectations on data passed to the mocked `filterNonWords` method
	- adding an expected word that was missing in a case of the data provider for the test
- `ProviderAbstractTest::$instance` type annotation as intersection
- `LoginFormTest` to use `TestCase::internalSetter()`
- Test classes that should have been `final` by declaring them as such
- PHP CS Fixer config file name in README by updating to the current one
- Some minor spelling, wording and formatting in README


## 0.8.0

### New

- Field `auth_key` in `user\Identity`
- Method on `RegistrationService` for mapping the form to user entity
- Separate configuration for web application test instances
- `RegistrationMailer` test coverage
- Build stage in `Dockerfile.dist` that aggregates files for the production image in a single layer
- Global Composer dependency installation for developers in development containers
	- and global vendor binaries directory in the `PATH` in dev containers
- Support for adding services specific to the development environment in `Makefile`
- phpMyAdmin service in the development environment
- Variable in `Makefile` alias generation with regex to match the characters used in environment, service and command names

### Changed

- Registration confirmation emails to be sent before persisting the new user
- Registration to pass user entity instead of the entire form model to the success message view
- Feed and Registration controllers to have hardcoded action IDs
- Yii application version to match the actual project version number
- Communication between PHP FPM and NGINX services to utilize a Unix socket
- The Docker Compose PHP FPM service to take the DB host from the environment
- `Dockerfile.dist` to copy the `entrypoint` script after the base stage

### Removed

- The option for case sensitive keyword extraction
- The PHP upstream definition for NGINX and corresponding configuration
- Garbage in `srv/assets` in a production image by adding it to `.dockerignore`
- Explicit line number reference from README on Composer cache reuse
- Extra whitespace in base unit `TestCase` class

### Improved

- The design of `RegistrationMailer`
- Variable, parameter and return type documentation that PHPStan pointed out about after the upgrade
- `KeywordExtractor` blacklist to encompass the 100 most common English words, plus a few more that commonly appear in the feed.
- `RegistrationService::Register` test to
	- test for registration failure when confirmation email cannot be sent
	- use a data provider
- `RegistrationService` testing to use the form interface not the concrete model
- Tests to log web app messages to a file in `var/` instead of `stdout`/`stderr`
- `Dockerfile.dist`
	- to use explicit versions when installing `apk` packages
	- to use an explicit version of Composer image
	- `base` stage to have a reduced layer count
	- to prep `srv` and `srv/assets` for `prod` stage during the `build` stage
	- not to switch `USER`s to install Composer dependencies when building `dev` image
	- by resolving `hadolint` issues
- `Dockerfile.dist` change detection and warning in `Makefile`
	- not to appear when the only file the timestamp has changed, not content
	- to explicitly print "Abort" if/when user decides to do so
- After a major version upgrade, the configuration for
	- PHP CS Fixer
	- PHPStan
	- PHPUnit
- PHP CS Fixer configuration to prefer short-echo tags
- NGINX service `build` directive verbosity in `docker-compose.yml` to be reduced

### Updated

- PHP to 8.0.6
- NGINX to 1.21
- Composer to 2.0.14
- Composer dependencies:
	- `vlucas/phpdotenv` v5.3.0
	- `yiisoft/yii2` 2.0.42.1
	- `friendsofphp/php-cs-fixer` v3.0.0
	- `phpstan/extension-installer` 1.1.0
	- `phpstan/phpstan` 0.12.88
	- `phpstan/phpstan-phpunit` 0.12.19
	- `phpunit/phpunit` 9.5.4
	- `yiisoft/yii2-debug` 2.1.17
	- `yiisoft/yii2-shell` 2.0.4


## 0.7.0

### New

- Procedural generation of most Makefile recipes and their aliases
- Default Make recipe `init`, that is a dependency for all the generated recipes and initializes (if not present)
	- `.env` and sets the `COOKIE_VALIDATION_KEY`
	- `devops/Dockerfile` and offers updating it whenever `devops/Dockerfile.dist` changes
- `MAKEFLAGS` in Makefile to disable `make`s built-in rules and variables
- A bunch of useful functions in Makefile

### Changed

- Name of the `devops/Dockerfile` to `devops/Dockerfile.dist`
- Name of the `make` recipe for the PHP development user shell to CamelCase — `dev/php/userShell`
- Dockerfile name variable to `DF_NAME` in Makefile

### Improved

- Variables in Makefile to be simply expanded where recursively expanded ones make no sense
- Wording of the `is_set()` function comment in Docker entrypoint


## 0.6.0

### New

- Development image build stage in the Dockerfile to properly take advantage of multi-stage builds
- Separate `dev-base` build stage to prepare `dev` image PHP configs, extensions, etc.
- User and group for developers in the development Docker image
- Developer ownership and full write permissions on the working directory, its contents, Composer dependencies and cache in the development image
- Umask of `rwxrwxr-x` in Docker images via the entrypoint
- Vim in the development image
- README mention of a procedure for reducing the development image Composer cache size

### Changed

- PHP CS Fixer config to add the strict types declaration in all source files
- Composer cache reuse during Docker builds to be disabled by default
- The `base` build stage of the Dockerfile to (and `prod` not to)
	- symlink the webroot and init the `var/` directory
	- add the Docker entrypoint
- The `dev` Docker image build to be based on the `base` stage instead of `prod`
- README's Docker instructions to generate NGINX `upstream.conf` in `var`

### Removed

- The development Dockerfile — `devops/Dockerfile.dev`
- The `src/` directory from the `build` stage
- Autoloader optimization from the development Docker image
- Composer cache volumes from the `build` stage and the development image
- PHPStan excludes directive (after moving PHPUnit output to `var/` in the previous release)

### Improved

- Composer cache reuse to be able to take effect during all Docker image builds
- Docker build cache reuse during `dev` builds by installing Composer dependencies sooner
- Docker `build` stage name (previously `composed`)
- Docker development image to include PHP CS Fixer, PHPStan and PHPUnit configs
- Feed item date mapping after adding strict types declaration
- The environment-dependent `YII_*` constant handling in PHPStan config
- Minor formatting issues in the Dockerfile


## 0.5.0

### New

- Docker and Docker Compose deployment support
- `Makefile` to aid in building and controlling Docker images and Docker Compose deployments
- Censoring of the database and SMTP passwords in `web` application logs
- Semantic Versioning
- `CHANGELOG.md`

### Changed

- Environment variable for SMTP password to `SMPT_PASS`
- PHP CS Fixer config not to apply `yoda_style` fix to identical (`===`/`!==`) comparison statements

### Improved

- Composer `install` command in README.md
- Some wording and formatting in README.md


## 0.4.0

### New

- Separate environment variables for components of the data source name (DSN) — see `.env`
- Environment variable for customizing which IP addresses are allowed to use the Yii Debug module
- Use of `stdout` and `stderr` streams for logging from `web` application
- Composer dependency:
	- `codemix/yii2-streamlog` 1.3.1

### Changed

- The default database password
- Names of environment variables for database user name and password, `DB_USER` and `DB_PASS` respectively
- Load and bootstrap the Yii Debug module only if the `YII_DEBUG` environment variable is `true`
- PHPUnit result cache and test coverage output to a sub-directory in `var/`
- User registration controller success flash key to be more descriptive

### Removed

- The `log` component from `common` configuration; logs now come only from `web`

### Updated

- Composer dependencies:
	- `vlucas/phpdotenv` v4.1.3
	- `yiisoft/yii2` 2.0.34
	- `phpstan/extension-installer` 1.0.4
	- `phpunit/phpunit` 8.5.3
	- `yiisoft/yii2-debug` 2.1.13
	- `yiisoft/yii2-shell` 2.0.3


## 0.3.0

### New

- `FormatDetector` extracted from `SimpleXMLParser`


## 0.2.0

### New

- Keyword extractor
- Feed format detection
- Atom feed support

### Improved

- Feed parser design
- `ActiveRepository` design
- Test coverage
- Documentation

### Changed

- Feed URL to <https://www.theregister.co.uk/software/headlines.atom>
- Feed item limit to unlimited

### Updated

- Composer dependencies:
	- `vlucas/phpdotenv` v4.0.1
	- `phpunit/phpunit` 8.5.0


## 0.1.0

Initial rushed release


[semver2]: https://semver.org/spec/v2.0.0.html

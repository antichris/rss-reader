<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\feed\FeedService;
use antichris\rssReader\feed\FeedTopKeywordExtractorInterface;
use antichris\rssReader\feed\fetcher\BasicStringFetcher;
use antichris\rssReader\feed\ItemProvider;
use antichris\rssReader\feed\keywordExtractor\FeedKeywordExtractor;
use antichris\rssReader\feed\parser\atom\AtomDateTimeParser;
use antichris\rssReader\feed\parser\atom\SxAtomFeedLocator;
use antichris\rssReader\feed\parser\atom\SxAtomFormat;
use antichris\rssReader\feed\parser\atom\SxAtomItemMapper;
use antichris\rssReader\feed\parser\atom\SxAtomMapper;
use antichris\rssReader\feed\parser\atom\SxAtomValidator;
use antichris\rssReader\feed\parser\FormatDetector;
use antichris\rssReader\feed\parser\FormatDetectorInterface;
use antichris\rssReader\feed\parser\rss\RssDateTimeParser;
use antichris\rssReader\feed\parser\rss\SxRssFeedLocator;
use antichris\rssReader\feed\parser\rss\SxRssFormat;
use antichris\rssReader\feed\parser\rss\SxRssItemMapper;
use antichris\rssReader\feed\parser\rss\SxRssMapper;
use antichris\rssReader\feed\parser\rss\SxRssValidator;
use antichris\rssReader\feed\parser\SimpleXMLParser;
use antichris\rssReader\feed\StringFetcherInterface;
use antichris\rssReader\feed\StringParserInterface;
use antichris\rssReader\misc\profiling\concrete\Profiler;
use antichris\rssReader\misc\profiling\concrete\SampleProvider;
use antichris\rssReader\misc\profiling\ProfilerInterface;
use antichris\rssReader\user\authentication\AuthenticationService;
use antichris\rssReader\user\authentication\LoginForm;
use antichris\rssReader\user\authentication\LoginFormInterface;
use antichris\rssReader\user\PasswordService;
use antichris\rssReader\user\registration\RegistrationForm;
use antichris\rssReader\user\registration\RegistrationFormInterface;
use antichris\rssReader\user\registration\RegistrationFormMapper;
use antichris\rssReader\user\registration\RegistrationMailer;
use antichris\rssReader\user\registration\RegistrationService;
use antichris\rssReader\user\repository\UserArMapper;
use antichris\rssReader\user\repository\UserArProvider;
use antichris\rssReader\user\repository\UserArRepository;
use antichris\rssReader\user\UserEntityProvider;
use antichris\rssReader\user\UserRepositoryInterface;
use antichris\rssReader\user\UserService;
use antichris\rssReader\user\UserStatusService;
use antichris\rssReader\web\controllers\RegistrationController;
use antichris\rssReader\web\controllers\UserController;
use antichris\rssReader\web\View;
use antichris\rssReader\web\widgets\Alert;
use yii\di\Instance;
use yii\web\View as WebView;

return [
    'definitions' => [
        'app.formatter' => fn () => Yii::$app->get('formatter'),
        'app.i18n' => fn () => Yii::$app->get('i18n'),
        'app.mailer' => fn () => Yii::$app->get('mailer'),
        'app.name' => fn () => Yii::$app->name,
        'app.security' => fn () => Yii::$app->get('security'),
        'app.session' => fn () => Yii::$app->get('session'),
        'app.urlManager' => fn () => Yii::$app->get('urlManager'),
        'app.userComponent' => fn () => Yii::$app->get('user'),
        'app' => fn () => Yii::$app,
        'param.senderAddress' => fn () => [
            Yii::$app->params['senderEmail'] => Yii::$app->params['senderName'],
        ],
        Alert::class => ['__construct()' => [
            'session' => Instance::of('app.session'),
        ]],
        View::class => ['__construct()' => [
            'app' => Instance::of('app'),
            'formatter' => Instance::of('app.formatter'),
            'i18n' => Instance::of('app.i18n'),
            'userComponent' => Instance::of('app.userComponent'),
        ]],
        WebView::class => View::class,
        LoginFormInterface::class => LoginForm::class,
        RegistrationFormInterface::class => RegistrationForm::class,
        RegistrationController::class => ['__construct()' => [
            4 => Instance::of('app.userComponent'),
            5 => Instance::of('app.session'),
        ]],
        UserController::class => ['__construct()' => [
            4 => Instance::of('app.userComponent'),
        ]],
    ],
    'singletons' => [
        FeedService::class => [
            //'feedURL' => 'http://www.tvnet.lv/rss/',
            //'feedURL' => 'https://www.delfi.lv/rss?channel=bizness',
            //'limit' => 10,
        ],
        StringFetcherInterface::class => BasicStringFetcher::class,
        StringParserInterface::class => SimpleXMLParser::class,
        BasicStringFetcher::class,
        SimpleXMLParser::class,
        FormatDetectorInterface::class => FormatDetector::class,
        FormatDetector::class => ['__construct()' => [
            'Atom' => Instance::of(SxAtomFormat::class),
            'RSS' => Instance::of(SxRssFormat::class),
        ]],
        SxAtomFormat::class,
        SxAtomValidator::class,
        SxAtomMapper::class,
        SxAtomFeedLocator::class,
        SxAtomItemMapper::class,
        AtomDateTimeParser::class,
        SxRssFormat::class,
        SxRssValidator::class,
        SxRssMapper::class,
        SxRssFeedLocator::class,
        SxRssItemMapper::class,
        RssDateTimeParser::class,
        ItemProvider::class,
        FeedKeywordExtractor::class,
        FeedTopKeywordExtractorInterface::class => FeedKeywordExtractor::class,
        UserRepositoryInterface::class => UserArRepository::class,
        UserService::class,
        UserArMapper::class,
        UserArProvider::class,
        UserEntityProvider::class,
        UserStatusService::class,
        AuthenticationService::class => ['__construct()' => [
            'userComponent' => Instance::of('app.userComponent'),
        ]],
        PasswordService::class => ['__construct()' => [
            'security' => Instance::of('app.security'),
        ]],
        RegistrationService::class,
        RegistrationFormMapper::class,
        RegistrationMailer::class => ['__construct()' => [
            'mailer' => Instance::of('app.mailer'),
            'urlManager' => Instance::of('app.urlManager'),
            'appName' => Instance::of('app.name'),
            'sender' => Instance::of('param.senderAddress'),
        ]],
        SampleProvider::class => fn () => new SampleProvider(Yii::$container, Yii::getLogger()),
        ProfilerInterface::class => Profiler::class,
    ],
];

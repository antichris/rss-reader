<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use antichris\rssReader\user\Identity;
use codemix\streamlog\Target;
use yii\debug\Module;
use yii\helpers\ArrayHelper;
use yii\symfonymailer\Mailer;
use function antichris\rssReader\getlenv;

$common = require __DIR__ . '/common.php';
$container = require __DIR__ . '/container.php';

$smtpHost = getlenv('SMTP_HOST', 'localhost');
$smtpUser = getlenv('SMTP_USER', 'rss-reader');
$smtpPass = getlenv('SMTP_PASS', 'top-secret');
$smtpPort = getlenv('SMTP_PORT', 587);

$logMaskVars = [
    '_SERVER.DB_PASS',
    '_SERVER.SMTP_PASS',
    '_SERVER.MAILER_DSN',
    // ---
    '_SERVER.HTTP_AUTHORIZATION',
    '_SERVER.PHP_AUTH_USER',
    '_SERVER.PHP_AUTH_PW',
    // ---
    '_POST.LoginForm.password',
    '_POST.RegistrationForm.password',
    '_POST.RegistrationForm.repeatPassword',
];

$config = [
    'id' => 'reader',
    'controllerNamespace' => 'antichris\\rssReader\\web\\controllers',
    'defaultRoute' => 'feed',
    'viewPath' => '@src/web/views',
    'components' => [
        'request' => [
            'cookieValidationKey' => getlenv('COOKIE_VALIDATION_KEY'),
        ],
        'user' => [
            'identityClass' => Identity::class,
            'loginUrl' => ['/user/login'],
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'mailer' => [
            'class' => Mailer::class,
            'useFileTransport' => !getlenv('MAILER_ENABLED', false),
            'transport' => [
                'dsn' => getlenv(
                    'MAILER_DSN',
                    "smtp://{$smtpUser}:{$smtpPass}@{$smtpHost}:{$smtpPort}",
                ),
            ],
        ],
        'log' => [
            'traceLevel' => (int) getlenv('YII_TRACELEVEL'),
            'targets' => [
                'stdout' => [
                    'class' => Target::class,
                    'url' => 'php://stdout',
                    'levels' => ['info', 'trace'],
                    'maskVars' => $logMaskVars,
                ],
                'stderr' => [
                    'class' => Target::class,
                    'url' => 'php://stderr',
                    'levels' => ['error', 'warning'],
                    'maskVars' => $logMaskVars,
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'registration' => '/registration/index',
                'activate/<token:[\w-]+>' => '/registration/confirm',
                'login' => '/user/login',
                'logout' => '/user/logout',
            ],
        ],
    ],
    'container' => $container,
];

if (YII_ENV_DEV && YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => Module::class,
        'allowedIPs' => explode(',', getlenv('YII_DEBUG_ALLOWED_IPS', '127.0.0.1,::1')),
    ];
}

return ArrayHelper::merge($common, $config);

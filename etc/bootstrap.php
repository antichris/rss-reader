<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use Dotenv\Dotenv;
use function antichris\rssReader\getlenv;

defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', __DIR__ . '/..');

// Composer autoload.
require_once YII_APP_BASE_PATH . '/vendor/autoload.php';

// Load local .env, if present.
Dotenv::createImmutable(YII_APP_BASE_PATH)->safeLoad();

// Set Yii constants from the environment.
defined('YII_DEBUG') or define('YII_DEBUG', (bool) getlenv('YII_DEBUG'));
defined('YII_ENV') or define('YII_ENV', getlenv('YII_ENV', 'prod'));

// Load Yii main class.
require_once YII_APP_BASE_PATH . '/vendor/yiisoft/yii2/Yii.php';

<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use yii\caching\FileCache;
use yii\db\Connection;
use function antichris\rssReader\getlenv;

$params = require __DIR__ . '/params.php';

$dbHost = getlenv('DB_HOST', 'localhost');
$dbPort = (int) getlenv('DB_PORT', 3306);
$dbName = getlenv('DB_NAME', 'rss-reader');

$config = [
    'name' => 'RSS Reader',
    'version' => '0.9.0', // TODO Automate updating this.
    'timeZone' => 'Europe/Riga',
    'basePath' => dirname(__DIR__),
    'runtimePath' => '@app/var/yii', // The file taget of log component uses this instead of the @runtime alias.
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        // ---
        '@src' => '@app/src',
        '@antichris/rssReader' => '@src',
        '@app/mail' => '@src/mail',
        '@assets' => '@app/assets',
    ],
    'bootstrap' => ['log'],
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'db' => [
            'class' => Connection::class,
            'dsn' => getlenv('DB_DSN', "mysql:host={$dbHost};port={$dbPort};dbname={$dbName}"),
            'username' => getlenv('DB_USER', 'rss-reader'),
            'password' => getlenv('DB_PASS', 'top-secret'),
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
    ],
    'params' => $params,
];

return $config;

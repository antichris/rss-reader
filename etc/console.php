<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use yii\console\controllers\MigrateController;
use yii\console\controllers\ServeController;
use yii\helpers\ArrayHelper;

$common = require __DIR__ . '/common.php';

$config = [
    'id' => 'basic-console',
    'controllerNamespace' => 'antichris\rssReader\console',
    'aliases' => [
        '@tests' => '@app/tests',
    ],
    'controllerMap' => [
        'serve' => [
            'class' => ServeController::class,
            'docroot' => '@app/srv',
        ],
        'migrate' => [
            'class' => MigrateController::class,
            'migrationNamespaces' => [
                'antichris\rssReader\migrations',
            ],
            'migrationPath' => null, // Allow only namespaced migrations.
        ],
        //'fixture' => [
        //    'class' => yii\faker\FixtureController::class,
        //],
    ],
];

return ArrayHelper::merge($common, $config);

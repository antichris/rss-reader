<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use yii\log\FileTarget as Target;

$config = require __DIR__ . '/web.php';

$config['id'] = 'reader-test';

$config['components']['log']['targets'] = [
    'catchAll' => [
        'class' => Target::class,
        // TODO Truncate the log between test sessions.
        'logFile' => '@runtime/logs/test-web.log',
    ],
];

return $config;

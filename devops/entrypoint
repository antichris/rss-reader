#!/bin/sh -e

## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at http://mozilla.org/MPL/2.0/.

main() {
	file_var COOKIE_VALIDATION_KEY
	file_var DB_PASS
	file_var SMTP_PASS
	file_var MAILER_DSN

	if [ "${1#-}" != "$1" ]; then
		set -- php-fpm "$@"
	fi

	if [ "$1" = php-fpm ] && ! is_set COOKIE_VALIDATION_KEY; then
		eprintf "WARNING: App will not process requests without a COOKIE_VALIDATION_KEY; expect 500's!\n"
	fi

	umask 0002

	exec "$@"
}

# Export env <VARIABLE> from a default value or the contents of <VARIABLE>_FILE.
file_var() { # (variable_name [default_value])
	var="$1"
	if is_set 2; then
		default="$2"
	fi
	var_file="${var}_FILE"

	if is_set "$var" && is_set "$var_file"; then
		die 'ERROR: Both %s and %s are set\n' "$var" "$var_file"
	fi

	if is_set "$var_file"; then
		value="$(cat "$(ref "$var_file")")" \
			|| die 'ERROR: Failed setting %s from %s\n' "$var" "$var_file"
		# Separate, because `export` masks error status.
		export "$var"="$value"
		unset "$var_file"
	elif is_set default; then
		export "$var"="$default"
	fi
}

# Output params to STDERR.
# shellcheck disable=SC2059
eprintf() { printf "$@" >&2; } # (...output_to_stderr)

# Return zero exit status if variable (by name) is set.
is_set() { [ "$(eval echo "\${${2}${1}${3}+.}")" ]; } # (name [prefix [suffix]])

# Output params to STDERR and exit with an non-zero (error) exit status.
die() { eprintf "$@"; exit 1; }  # (...output_to_stderr)

# Output (to STDOUT) value of variable by name.
ref() { eval echo "\$${2}${1}${3}"; } # (name [prefix [suffix]])

# Normalized echo.
echo() { env echo "$@"; } # (...params)

main "$@"
